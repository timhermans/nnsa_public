<!-- ABOUT THE PROJECT -->
# Quick start
## About
The NeoNatal Signal Analysis (*nnsa*) package is a software toolbox that provides a high-level interface for preprocessing, analyzing and postprocessing medical neonatal data, especially EEG.

**Open-source repository**: [https://gitlab.com/timhermans/nnsa_public](https://gitlab.com/timhermans/nnsa_public)

**Documentation**: [nnsa.readthedocs.io](https://nnsa.readthedocs.io)


## Dependencies
* The package was developed using python version 3.8. 
* Required packages and their exact versions (as used when developing this package) can be found in [requirements.txt](https://gitlab.com/timhermans/nnsa_public/-/blob/main/requirements.txt).
* The required packages are also listed in the `install_requires` list in the [setup.py](https://gitlab.com/timhermans/nnsa_public/-/blob/main/setup.py) file (without strict version assignments), which automatically installs any missing dependencies when installing the `nnsa` package (but without pinned versions).

## Installation
This quick installation uses Git and Anaconda to create a new environment and installs the `nnsa` package and its dependencies.

1) Download the repository. Open the Git bash prompt, cd to a location of your preference and clone the repository to get a copy of the code:

        cd <clone_directory>
        git clone https://gitlab.com/timhermans/nnsa_public.git

   >Alternatively, you can create a fork of the project and clone the forked repository (if you want to make and maintain changes to the code). 

2) Activate an environment. Open the Anaconda prompt, optionally create a new environment (use python version 3.8 to avoid potential version conflicts) and activate the environment:

        conda create -n my_env python=3.8
        conda activate my_env

3) Install dependencies. cd to the cloned repository in the Anaconda prompt and install the pinned dependencies (this can take a few minutes): 
      
        cd <clone_directory>\nnsa_public
        pip install -r requirements.txt
   
   >This installs the dependencies with the exact versions used during development (to reduce the risk of dependency/version issues). 
   > If want to test/use the `nnsa` package with other versions of the dependencies (e.g., if you want to install it in an existing environment), then you can skip the second command which installs the version-specific requirements. 
   > Instead, move on to the next step in which also checks and installs missing dependencies, but without caring about the exact version. 
   > However with this latter approach you could potentially run into some version/dependency conflicts.  

4) Install the `nnsa` package (note the dot at the end, specifying the current directory): 

        pip install .

	> If you are planning on making your own changes to (a fork of) the `nnsa` code, you could install with the editable flag -e:  
    `pip install -e .`  
    This way, you can change the source code without needing to reinstall the `nnsa` package. 

5) To check the installation, exit the current directory, start python and try to import `nnsa`. E.g., still in the Anaconda prompt:

        cd ..
        python
        >>> import nnsa
        >>> print(nnsa)

    If no error message is displayed, installation is successful. 
   >The printed path shows where the code is located. Note that this depends on whether you added the `-e` flag to the pip install command or not.
   > If you did, then the location of the `nnsa` package is the same as where you cloned the repository in step 1 (e.g. it is simply pointing to the files that you cloned). Therefore, changes to the cloned files will change your installation of `nnsa`.
   > In contrast, if you did not include the `-e` flag, then an independent copy of the `nnsa` package is installed elsewhere on your machine (where all other packages in the environment are installed).
   > In that case, any changes to the cloned files in `nnsa` won't affect the installed version of `nnsa` unless you install it again (step 4). 

## Usage
The [Documentation](https://nnsa.readthedocs.io) includes multiple [Examples](https://nnsa.readthedocs.io/en/latest/examples.html) demonstrating how to use the code.
In addition, it includes an overview and descriptions of all functions and classes in the [nnsa](https://nnsa.readthedocs.io/en/latest/nnsa.html) package. 

Note: some of the example scripts may have become deprecated due to continuous updates to the code.

<!-- 
LICENSE 
## License
-->

## Model data
Some models included in this package are trained on (labelled) medical data. Therefore, some of these models are confidential and not included in this online public repository. 
This may cause errors when trying to apply these models (e.g. sleep stage CNN, brain age estimation). 
In case you want to have access to these models, please send a request to <tim-hermans@hotmail.com>.

## Links
Open-source repository: [https://gitlab.com/timhermans/nnsa_public](https://gitlab.com/timhermans/nnsa_public)

Documentation: [nnsa.readthedocs.io](https://nnsa.readthedocs.io)

<!-- CONTACT -->
## Contact

Tim Hermans (developer) - <tim-hermans@hotmail.com>

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
The analysis algorithms in this package incoporate work from various researchers from the 
[BioMed](https://www.esat.kuleuven.be/stadius/research.php?view=BioMed) group at KU Leuven in the department 
[ESAT - STADIUS, Stadius Centre for Dynamical Systems, Signal Processing and Data Analytics](https://www.esat.kuleuven.be/stadius/).

This toolbox was developed as part of the [INFANS](https://www.tu-ilmenau.de/universitaet/fakultaeten/fakultaet-informatik-und-automatisierung/profil/institute-und-fachgebiete/institut-fuer-biomedizinische-technik-und-informatik/infans-project) 
project, which received funding from the European Union’s Horizon 2020 Framework Programme under grant agreement No.813843.
