# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options.
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
import os
import sys
sys.path.insert(0, os.path.abspath('..'))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'NeoNatal Signal Analysis'
copyright = '2023, Tim Hermans'
author = 'Tim Hermans'

# The full version, including alpha/beta/rc tags
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # 'sphinx.ext.autodoc',  # To use autodoc.
    'autodocsumm',  # autodoc-like documentation + summary of classes & methods (pip install autodocsumm).
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',  # For Google style docstrings.
    'sphinx.ext.todo',
    'myst_parser',  # Markdown support (pip install --upgrade myst-parser)
    # 'sphinx.ext.linkcode',
]

# def linkcode_resolve(domain, info):
#     # For sphinx.ext.linkcode, see https://www.sphinx-doc.org/en/master/usage/extensions/linkcode.html#confval-linkcode_resolve
#     if domain != 'py':
#         return None
#     if not info['module']:
#         return None
#     filename = info['module'].replace('.', '/')
#     return "https://gitlab.com/timhermans/nnsa_public/-/blob/main/%s.py" % filename

# Options for autodocsumm.
autodoc_default_options = {"autosummary": True}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# -- autodoc settings --------------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
# If importing unrelevant modules breaks the build, add them to list list to ignore imports.
autodoc_mock_imports = ['seaborn', 'matplotlib', 'tensorflow', 'pyprind', 'pycwt', 'scipy',
                        'matplotlib', 'mpl_toolkits', 'tensorflow', 'termcolor', 'seaborn',
                        'scikit-learn', 'sklearn', 'statsmodels', 'joblib', 'h5py', 'numba',
                        'tqdm', 'networkx', 'lifelines', 'hmmlearn', 'xlrd', 'openpyxl', 'psutil',
                        'nnsa.feature_extraction.feature_sets_old', 'nnsa.matlab']

# -- Napoleon settings -------------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
# napoleon_use_rtype = False  # Separate line for type of return value.
napoleon_custom_sections = [('Returns', 'params_style')]  # Configure the Returns section to behave like Args.

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
