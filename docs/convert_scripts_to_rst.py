"""
Converts a python script to .rst with alternating code blocks and paragraphs.

Author: Tim Hermans (tim-hermans@hotmail.com).
"""
import os.path
import warnings
from glob import glob

from nnsa.utils.paths import split_path

# Path to examples directory (directory with example scripts).
EXAMPLES_DIR = '..\\examples'

# Path to examples directory on the Gitlab page.
EXAMPLES_DIR_GITLAB = 'https://gitlab.com/timhermans/nnsa_public/-/blob/main/examples'

# Path (relative or absolute) to the docs directory for saving .rst files.
DOCS_DIR = '.'

# Patterns in filepaths to skip.
IGNORE_PATTERNS = [
    'wavelet_artefacts',
    'test_all.py',
]


def py_2_rst(script_fp):
    """
    Convert a python script to an rst file.

     A triple quote comment at the top is used as first paragraph of the .rst file.
     Section comments are also displayed as text. The rest is displayed as a block of code.

    Args:
        script_fp (str): path to the python script to convert.

    Returns:
        rst_lines (list): list of rst lines to write (join with a newline "\n").
    """
    # Initialize list for the contents of the .rst file.
    rst_lines = []

    # Get the directory of the script.
    script_dir = os.path.dirname(script_fp)

    # Get the path relative to the examples directory.
    rel_path = os.path.relpath(script_fp, start=EXAMPLES_DIR)

    # To web link at GitLab.
    rel_path_parts = split_path(rel_path)
    link_gitlab = EXAMPLES_DIR_GITLAB + '/' + '/'.join(rel_path_parts)

    # Use the name of the script to create a title for the page.
    filename = os.path.splitext(os.path.basename(script_fp))[0]
    title = ' '.join(filename.split('_')).capitalize()
    title_underline = '='*len(title)

    # Add title to rst lines.
    rst_lines.append(title)
    rst_lines.append(title_underline)
    rst_lines.append('')

    # Read script.
    with open(script_fp, "r", encoding="utf8") as f:
        # Reading from a file
        all_lines = f.readlines()

    # Find the lines of the code starting with “““.
    where_triple = [idx for idx, line in enumerate(all_lines) if line.startswith('"""')]

    # If there are no triple quotes, skip the file.
    if len(where_triple) == 0:
        msg = f'\nFile {script_fp} has no triple quotes. Skipping...'
        warnings.warn(msg)
        return None

    # Put the text enclosed by the triple quotes as the header/first section of the rst.
    if len(where_triple) > 1:
        header = ''.join(all_lines[where_triple[0]: where_triple[1]]).strip('"').strip()

        # Remove the header part.
        del all_lines[where_triple[0]: where_triple[1]+1]

        # Add to rst lines.
        rst_lines.append(header)
        rst_lines.append('')

    # Add GitLab link to script file.
    rel_path_ = rel_path.replace("\\", "/")
    rst_lines.append(f'Link to script: `{rel_path_} <{link_gitlab}>`_')
    rst_lines.append('')

    # Find the sections (starting with #%%).
    while len(all_lines) > 0:
        # Find where the code stops (untill first occurrence of a section comment #%%)
        # and where the section comment stops.
        idx_code_stop = len(all_lines)
        idx_comment_stop = len(all_lines)
        for idx, line in enumerate(all_lines):
            if line[:5].replace(' ', '').startswith('#%%'):
                idx_code_stop = idx
                idx_comment_stop = len(all_lines)
                for idx2, line2 in enumerate(all_lines[idx_code_stop:]):
                    previous_line = all_lines[idx_code_stop + idx2 - 1].strip()
                    if not line2.strip().startswith('#'):
                        # No more comment.
                        idx_comment_stop = idx + idx2
                        break
                    elif len(previous_line) < 110 and previous_line.endswith('.'):
                        # Previous line was short and ended with a period. Assume the next line is not a continuation.
                        idx_comment_stop = idx + idx2
                        break
                break

        # Get the code.
        code_lines = all_lines[:idx_code_stop]

        # Get the section text.
        section_text = ''
        for line in all_lines[idx_code_stop: idx_comment_stop]:
            if line != '\n':
                # Strip comment marks and white white spaces and new lines.
                line = line.strip().strip('#').strip().strip('%%').strip()
            # Add a space to the end of the line (it has been stripped).
            section_text += line + ' '

        # Add to the rst lines.
        in_code_block = False
        code_rst = ''
        while len(''.join(code_lines).strip()) > 0:
            # Look at one line of code at a time.
            line = code_lines.pop(0)

            # If line of code saves a fig as figure, then display that saved figure instead of that line of code.
            if line.strip().startswith('save_fig_as'):
                # Get filepath to figure.
                line = line.replace("'", '"')  # Replace single quotes with double quotes.
                idx_quotes = [i for i, ch in enumerate(line) if ch == '"']
                err_msg = f'\nCould not insert figure on line {line}'

                if len(idx_quotes) != 2:
                    # We need a start and end.
                    warnings.warn(err_msg+f' {len(idx_quotes)} quotes.')
                    continue

                fig_fp = line[idx_quotes[0]+1: idx_quotes[1]]
                if not os.path.splitdrive(fig_fp)[0]:
                    # The filepath is relative to the script.
                    fig_fp = os.path.join(script_dir, fig_fp)

                if not os.path.exists(fig_fp):
                    warnings.warn(err_msg+f' {len(fig_fp)} not exist.')
                    continue

                # Get figure filepath relative to DOCS_DIR and with / slashes.
                fig_fp_reldocs = os.path.relpath(fig_fp, DOCS_DIR)
                fig_fp_reldocs = fig_fp_reldocs.replace(os.path.sep, '/')

                # Insert figure.
                code_rst += f'\n.. figure:: {fig_fp_reldocs}\n'

                # We left the code block.
                in_code_block = False
            elif line.strip() == 'from nnsa.utils.plotting import save_fig_as':
                # Skip this line (since we do not show the code line that saves the figure).
                continue
            else:
                # Code text.
                if not in_code_block:
                    # Start code block if needed.
                    code_rst += '\n.. code-block:: python\n\n'

                    # We entered a code block.
                    in_code_block = True
                # Add 4 spaces for correct indentation in rst file.
                code_rst += '    ' + line

        # Add text.
        rst_lines.append('')
        rst_lines.append(code_rst)
        rst_lines.append('')
        rst_lines.append(section_text)
        rst_lines.append('')

        # Remove the processed part.
        del all_lines[:idx_comment_stop]

    return rst_lines


#%% Settings.
# Whether to overwrite existing .rst files with scripts.
overwrite = True

# Whether make changes to the examples.rst file (maybe we want to use a custom order).
update_examples_rst = True

#%% Convert .py scripts to .rst files.
# Find all scripts in the examples directory.
all_example_fps = glob(os.path.join(EXAMPLES_DIR, '**/*.py'), recursive=True)

# Exclude any filepaths that match patterns in IGNORE_PATTERNS.
all_example_fps = [fp for fp in all_example_fps if not any(pat in os.path.abspath(fp) for pat in IGNORE_PATTERNS)]

print(f'Converting {len(all_example_fps)} python scripts to .rst files...')
for fp in all_example_fps:
    # Get the path relative to the examples directory.
    rel_path = os.path.relpath(fp, start=EXAMPLES_DIR)

    # Skip if its in a directory "old".
    if 'old' in split_path(rel_path):
        continue

    # Output .rst path.
    rst_fp = os.path.join(DOCS_DIR, 'examples.' + '.'.join(split_path(os.path.splitext(rel_path)[0])) + '.rst')

    # Check for overwriting.
    if not overwrite and os.path.exists(rst_fp):
        msg = f'\nFile {rst_fp} already exists. Overwriting disabled. Skipping.'
        warnings.warn(msg)
        raise FileExistsError(msg)

    # Convert the code to .rst.
    rst_lines = py_2_rst(script_fp=fp)

    if rst_lines is not None:
        # Write .rst text.
        with open(rst_fp, 'w') as f:
            f.write('\n'.join(rst_lines))
        print(f'Saved to {rst_fp}')

#%% Create hiarchical structure in .rst files.
print(f'Including all .rst files in the index...')
# Find all example*.rst files and create the example.rst and subsequent with the same folder structure as the code.
all_rst_fps = sorted(glob(os.path.join(DOCS_DIR, 'examples.*.rst')))
for rst_fp in all_rst_fps:
    rel_fp = os.path.relpath(path=rst_fp, start=DOCS_DIR)

    new_line = os.path.splitext(rel_fp)[0]

    while '.' in new_line:
        file_parts = os.path.basename(new_line).split('.')

        # We need to put the relative part in the file above.
        rst_above_fn = '.'.join(file_parts[:-1]) + '.rst'
        if not update_examples_rst and rst_above_fn == 'examples.rst':
            # Do not change this file.
            break

        # Add the current file to the file above.
        rst_above_fp = os.path.join(DOCS_DIR, rst_above_fn)

        # Create file if not exists.
        if not os.path.exists(rst_above_fp):
            # Title.
            title = file_parts[-2].replace('_', ' ').capitalize()
            # Init the toctree.
            toctree = """
.. toctree::
   :maxdepth: 4
"""
            with open(rst_above_fp, 'w') as f:
                f.write(title+'\n')
                f.write('='*len(title)+'\n')
                f.write(toctree+'\n')

        # Check if already referenced in rst file.
        with open(rst_above_fp, 'r') as f:
            rst_contents = f.read()
        if new_line + '\n' not in rst_contents:
            # Add.
            with open(rst_above_fp, 'a') as f:
                f.write('   '+new_line+'\n')

        # Go one level up.
        new_line = os.path.splitext(new_line)[0]
