Artefacts
=========

.. toctree::
   :maxdepth: 4

   examples.artefacts.apply_cnn_to_array
   examples.artefacts.apply_cnn_to_eegdataset
   examples.artefacts.extended_artefact_detection
