Cwt
===

.. toctree::
   :maxdepth: 4

   examples.cwt.basic_wavelet_transforms
   examples.cwt.partial_wavelet_coherence
