Feature extraction
==================

.. toctree::
   :maxdepth: 4

   examples.feature_extraction.amplitude_eeg
   examples.feature_extraction.burst_detection
   examples.feature_extraction.burst_detection_dibi
   examples.feature_extraction.coherence_graph
   examples.feature_extraction.envelope
   examples.feature_extraction.line_length
   examples.feature_extraction.multi_scale_entropy
   examples.feature_extraction.multifractal_analysis
   examples.feature_extraction.power_analysis
   examples.feature_extraction.psd
   examples.feature_extraction.read_edf_and_detect_sleep
   examples.feature_extraction.robust_fba
   examples.feature_extraction.sleep_stages
   examples.feature_extraction.sleep_stages_2class
   examples.feature_extraction.sleep_stages_4class
   examples.feature_extraction.sleep_stages_cnn
