Examples
========

.. toctree::
   :maxdepth: 4

   examples.a_quick_start
   examples.annotations
   examples.artefacts
   examples.containers
   examples.cwt
   examples.dynamic_coupling
   examples.feature_extraction
   examples.io
   examples.side_obsp
