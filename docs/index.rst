.. NeoNatal Signal Analysis documentation master file, created by
   sphinx-quickstart on Fri Aug 18 17:14:14 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NeoNatal Signal Analysis's documentation!
====================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme_link
   examples
   nnsa

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
