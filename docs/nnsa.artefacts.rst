nnsa.artefacts package
======================

Submodules
----------

nnsa.artefacts.artefact\_detection module
-----------------------------------------

.. automodule:: nnsa.artefacts.artefact_detection
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.artefacts.clean\_detector\_cnn module
------------------------------------------

.. automodule:: nnsa.artefacts.clean_detector_cnn
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nnsa.artefacts
   :members:
   :undoc-members:
   :show-inheritance:
