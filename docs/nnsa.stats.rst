nnsa.stats package
==================

Submodules
----------

nnsa.stats.intervals module
---------------------------

.. automodule:: nnsa.stats.intervals
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.stats.paired module
------------------------

.. automodule:: nnsa.stats.paired
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.stats.partially\_paired module
-----------------------------------

.. automodule:: nnsa.stats.partially_paired
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.stats.statistics module
----------------------------

.. automodule:: nnsa.stats.statistics
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.stats.surrogates module
----------------------------

.. automodule:: nnsa.stats.surrogates
   :members:
   :undoc-members:
   :show-inheritance:

nnsa.stats.utils module
-----------------------

.. automodule:: nnsa.stats.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: nnsa.stats
   :members:
   :undoc-members:
   :show-inheritance:
