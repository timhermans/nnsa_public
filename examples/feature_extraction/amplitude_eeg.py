"""
This script demonstrates how to emulate amplitude-integrated EEG with the nssa package.

Author: Tim Hermans (tim-hermans@hotmail.com).
"""

import numpy as np
import matplotlib.pyplot as plt

import os

from nnsa import AmplitudeEeg, TimeSeries, EdfReader

plt.close('all')

#%% First define a filepath to an edf file on you PC that you want to read (include the file extension).

# Specify path to an EDF(+) file.
filepath = 'C:/data_temp/test.edf'

#%% Load data or create mock-data is file cannot be found.
if os.path.exists(filepath):
    print('Reading EDF...')
    with EdfReader(filepath) as r:
        ds = r.read_eeg_dataset()
    eeg_ts = ds.create_bipolar_channel(channel_1='EEG C3', channel_2='EEG C4')

    # Get the eeg data as an array.
    eeg = eeg_ts.signal
    fs = eeg_ts.fs
    t = eeg_ts.time
else:
    # Create mock-data (not representative for any EEG).
    print('Generating mock data...')
    fs = 200
    n = fs*3600
    t = np.arange(n)/fs
    eeg = 300 * \
          np.sin(t*2*np.pi*3) * \
          np.sin(t*2*np.pi*8) * \
          np.sin(t*2*np.pi*13)

#%% Plot the signal.
fig, axes = plt.subplots(4, 1, tight_layout=True, sharex='all')
ax = axes[0]
ax.plot(t, eeg)
ax.set_ylabel('EEG')

#%% Various algorithms exists for emulating aEEG. Several of them are implemented in nnsa. You
# can choose the algorithm when initializing an AmplitdeEeg object. Choose from
# 'NEAT', 'TW', 'ZHANG'. See the functions in the nnsa.feature_extraction_aeeg module for
# each of these methods for more information (e.g. compute_aeeg_tw()).

#%% Let us compare several methods.

# Loop over methods, compute aEEG and plot.
for ax, method in zip(axes[1:], ['NEAT', 'TW', 'ZHANG']):
    # Create instance of AmplitudeEeg object and specify parameters.
    aeeg = AmplitudeEeg(method=method)
    aeeg_result = aeeg.process(eeg, fs, verbose=1)

    # Plot the envelope.
    aeeg_result.plot(ax=ax, time_scale='seconds')
    ax.set_title(method)
