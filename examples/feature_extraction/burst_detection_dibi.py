"""
This script illustrates how the dIBI detection can be used.
"""

from nnsa import EdfReader
from nnsa.feature_extraction.discontinuity import detect_dibi

# Select an EDF with discontinuous EEG.
filepath = r'C:\Users\thermans\OneDrive - KU Leuven\Documents\data\pt33.EDF'

# Read EEG data from EDF.
with EdfReader(filepath) as r:
    eeg_ds = r.read_eeg_dataset(begin=46*60, end=56*60).reference('Cz')

# Prepare inputs.
x = eeg_ds.asarray()  # Raw multichannel EEG signal (channels, time).
fs = eeg_ds.fs  # Sample frequency of the EEG.

# Select a part
start = 240
end = start + 100
x = x[:, int(start*fs): int(end*fs)]

# Run algorithm and show intermediate illustrative plots.
dIBI = detect_dibi(x, fs, show_plots=True)
