"""
This script demonstrates how to read an EDF file and predict sleep stages.

Author: Tim Hermans (tim-hermans@hotmail.com).
"""
import nnsa

#%% Settings.
# Path to the EDF file.
edf_filepath = r'C:\data_temp\test.edf'

# Whether to use 4-class sleep staging (True) or 2-class (False).
four_class = True

# Path to the output file (Excel).
output_filepath = r'C:\data_temp\test_sleep.xlsx'

#%% Read EEG data from EDF file.
with nnsa.EdfReader(edf_filepath) as r:
    # Read (see the EdfReader.read_eeg_dataset() function for explanation of the options).
    # Returns an EegDataset object, which has some useful methods for processing the EEG.
    eeg_ds = r.read_eeg_dataset(discontinuous_mode='fill')

#%% Sleep staging (for neonates >= 36 weeks PMA).
# Compute result.
if four_class:
    result = eeg_ds.sleep_stages_robust()
else:
    result = eeg_ds.sleep_stages_cnn(num_classes=2)
# The sleep stages are contained in the attribute `df` as a pandas dataframe:
df = result.df

# Save the results to Excel (or you could also save to csv. by using df.to_csv()).
df.to_excel(output_filepath, index=False)
print(f'Saved results to {output_filepath}')
