import numpy as np
import matplotlib.pyplot as plt
from nnsa.feature_extraction.wavelets import WaveletCoherence

plt.close('all')

#%% Generate random signals.
np.random.seed(43)
N = 1024
fs = 1
t = np.arange(0, N, 1/fs)
x = np.cos(2*np.pi*t*0.01) + np.random.rand(N)*2 + np.cos(2*np.pi*t*0.2)
y = np.sin(2*np.pi*t*0.01) + np.random.rand(N)*2
y[int(N/2):] += np.sin(2*np.pi*t[int(N/2):]*0.2)

#%% Compute coherence using default parameters.
# result = WaveletCoherence().wct(x=x, y=y, fs=fs)

#%% Compute coherence using non-default parameters.
# To specify non-default parameters, set the parameters when creating the WaveletCoherence object.

# Parameters for continuous wavelet transform (CWT) computation, see nnsa.compute_wavelet_coherence().
cwt_kwargs = {
    # 2log spacing between discrete scales. Smaller values will result in better scale (frequency) resolution,
    # but slower calculation. Defaults to 1/12.
    'ds': 1/6,
    # Smallest scale of the wavelet (~highest frequency). Freq ~ 1/scale. Defaults to 2/fs.
    'min_scale': 2/fs,
    # Number of scales. Scales range from min_scale up to min_scale * 2**((num_scales-1) * ds).
    # Defaults to (log2(N/min_scale/fs))/dj + 1.
    'num_scales': None,
    # Bool specifying whether to normalize (zero mean, unit variance) signals before computation or not.
    # Defaults to True.
    'normalize': True,
}

# Parameters for surrogate analysis. Set n_surrogates to 0 if no surrogate computations are desired.
surrogates = {
    # Number of surrogates to compute. Set to 0 to not compute any surrogate values:
    'n_surrogates': 100,
    # How to generate surrogates (see nnsa.stats.surrogates.compute_surrogate() for options):
    'how': 'AR',
    # Seed for the random generator:
    'seed': None,
}

# Create a WaveletCoherence object and wct the two signals.
result = WaveletCoherence(cwt_kwargs=cwt_kwargs, surrogates=surrogates).wct(x=x, y=y, fs=fs)

#%% Plot results.
# Plot most information. Note that parts where the significance threshold is exceeded are encircled with black lines.
result.plot()

# Plot the phase to find parts were phase locking occurs.
result.plot_phase()

# Plot the significance.
result.plot_significance()

# Plot percentage of significant coherence as function of frequency.
result.plot_freq_profile_significant()

# Plot percentage of significant coherence as function of time.
result.plot_time_profile_significant(freq_low=0.15, freq_high=0.25)

#%% Filter the result keeping only significant parts.
result_sig = result.filter_significant()

# Plot.
result_sig.plot()
result_sig.plot_phase()