"""
This script demonstrates how the nnsa.EdfReader can be used to easily read EDF(+) files.
More a more basic introduction on the EdfReader, see the edf_reader.py example script.
"""
from nnsa import EdfReader
import matplotlib.pyplot as plt

#%% First define a filepath to an edf file on you PC that you want to read (include the file extension).
# Specify path to an EDF(+) file.
filepath = 'C:/data_temp/test.edf'

#%% The nnsa.EdfReader() is a child of the nnsa.edfreadpy.EdfReader() class. The basic functionality is implemented
# in the edfreadpy class (see also the edf_reader.py example script). More high level interfaces are
# implemented in the nnsa class and aim to facilitate typical data reading for neonatal signal analysis
# purposes. E.g. we can load a signal as a nnsa.TimeSeries:

# Read a signal as a TimeSeries.
with EdfReader(filepath) as r:
    ts = r.read_time_series(channel=1)

# Plot the signal.
plt.figure()
ts.plot()

#%% Besides reading a signle signal as a TimeSeries, we can alos easily read multile related signals as an nnsa Dataset.
# For example, to read all EEG data, we can use the read_eeg_dataset() function. See the code and documentation of
# that function for more details.

# Read an entire EEG dataset.
with EdfReader(filepath) as r:
    eeg_ds = r.read_eeg_dataset()

# Plot the loaded dataset.
plt.figure()
eeg_ds.plot()

#%% There exists other similar functions for reading other sets of signals, such as read_spo2_dataset():

# Read all arterial oxygen-related signals in a dataset.
with EdfReader(filepath) as r:
    spo2_ds = r.read_spo2_dataset()

# Plot the loaded dataset.
plt.figure()
spo2_ds.plot()

#%% See https://nnsa.readthedocs.io/en/latest/nnsa.html#nnsa.EdfReader for a list of all functions. Of course, you
# can create your own functions depending on your needs.
