"""
This script finds all python files in the examples directory and runs them one at a time, catching any errors.
For each script, a message is printed indicating whether the script finished successfully or whether it failed.

Author: Tim Hermans (tim-hermans@hotmail.com).
"""

# Set the matplotlib backend to suppress any plot windows.
import matplotlib
matplotlib.use('Agg')

import sys
from glob import glob
import os
import subprocess
from time import sleep

from termcolor import cprint, colored

from nnsa.utils.paths import split_path

# Path to examples directory (directory with example scripts).
EXAMPLES_DIR = '.'

# Files to ignore (e.g. because they rely on reading an EDF, or they are too slow).
# Do not add the extension (.py).
ignore_files = [
    'config',
    'test_all',
]

# Find all scripts in the examples directory.
all_example_fps = glob(os.path.join(EXAMPLES_DIR, '**/*.py'), recursive=True)

# Remove any files contained in a directory named "old" and remove the current file.
all_example_fps = [fp for fp in all_example_fps if
                   'old' not in split_path(fp) and
                   not os.path.samefile(__file__, fp) and
                   os.path.splitext(os.path.basename(fp))[0] not in ignore_files]

print(f'Testing {len(all_example_fps)} python scripts...')
results = dict()
for ii, fp in enumerate(all_example_fps):
    # Get the path relative to the examples directory.
    rel_path = os.path.relpath(fp, start=EXAMPLES_DIR)

    # Initial status.
    status = 'Success'

    # Print the script name.
    text = f'{ii+1}/{len(all_example_fps)} {rel_path}...'
    sys.stdout.write(f'{text:55s}:')

    # Redirect output and errors to null devices
    with open(os.devnull, 'w') as null_device:
        try:
            # Run example script.
            subprocess.run(['python', fp], check=True, stdout=null_device, stderr=null_device)
        except subprocess.CalledProcessError as e:
            # Catch any errors.
            status = 'Failed'
            # raise e

    sys.stdout.write(colored(f' {status}\n', color='red' if status=='Failed' else 'green'))
    results[rel_path] = status

#%% Print summary.
success = [stat == 'Success' for stat in results.values()]
n_success = sum(success)
n_all = len(success)
n_fail = n_all - n_success
cprint(f"{n_fail}/{n_all} examples failed!", color='red')
cprint(f"{n_success}/{n_all} examples ran successfully!", color='green')
