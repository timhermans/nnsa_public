import os

# Suppress non-fatal messages from tensorflow.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # or any {'0', '1', '2'}

# Classes/functions.
from .annotations.annotation import Annotation
from .annotations.annotation_set import AnnotationSet
from .artefacts.clean_detector_cnn import CleanDetectorCnn
from .artefacts.artefact_detection import detect_anomalous_channels
from .models.evaluation import ConfusionMatrix
from .config import NNSA_DIR, PKG_DIR, MODEL_DATA_DIR
from .containers.datasets import BaseDataset, EegDataset
from .containers.time_series import TimeSeries
from .decompositions.side_obsp import SideObsp
from .feature_extraction.aeeg import AmplitudeEeg, AmplitudeEegResult
from .feature_extraction.brain_age_cnn import BrainAgeSinc, BrainAgeResult
from .feature_extraction.connectivity import CoherenceGraph, CoherenceGraphResult
from .feature_extraction.discontinuity import BurstDetection, BurstDetectionResult
from .feature_extraction.dynamic_coupling import DynamicCoupling, DynamicCouplingResult
from .feature_extraction.entropy import MultiScaleEntropy, MultiScaleEntropyResult
from .feature_extraction.envelope import Envelope, EnvelopeResult
from .feature_extraction.feature_sets.base import FeatureSetResult
from .feature_extraction.frequency_analysis import PowerAnalysis, PowerAnalysisResult, Psd, PsdResult
from .feature_extraction.fractality import LineLength, LineLengthResult, MultifractalAnalysis, \
    MultifractalAnalysisResult
from .feature_extraction.result import SUPPORTED_RESULT_FILE_TYPES, read_result_from_file, ResultBase
from .feature_extraction.sleep_stages import SleepStages, SleepStagesResult, SleepStagesCnn, SleepStagesCnnResult, \
    SleepStagesRobust, SleepStagesRobustResult
from .feature_extraction.statistics import SignalStats, SignalStatsResult
from .feature_extraction.wavelets import WaveletCoherence, WaveletCoherenceResult, WaveletResult
from .cwt.transforms import compute_wavelet_coherence
from .io.readers import EdfReader
from .io.routines import read_annotation_set_KL, read_annotation_set_preterm
from .parameters.parameters import ClassWithParameters, Parameters
from .preprocessing.filter import FilterBase, NotchIIR, MovingAverage, WinFIR, RemezFIR, Butterworth
from .preprocessing.combine_channels import combine_channels
from .utils.arrays import (clamp, moving_envelope, moving_line_length, moving_mad, moving_max, moving_mean,
                           moving_median, moving_std)
from .utils.objects import list_attrs, list_methods, print_object_summary
from .utils.other import print_exception_info
from .utils.paths import get_filepaths, get_output_dir
from .utils.plotting import stripboxplot, format_time_axis
from .utils.testing import assert_equal

# Only include functions and classes directly useful for users.
__all__ = [
    'feature_extraction',

    'CleanDetectorCnn',
    'detect_anomalous_channels',

    'Annotation',
    'AnnotationSet',

    'ConfusionMatrix',

    'NNSA_DIR',
    'PKG_DIR',
    'MODEL_DATA_DIR',

    'BaseDataset',
    'EegDataset',

    'TimeSeries',

    'SideObsp',

    'SUPPORTED_RESULT_FILE_TYPES',
    'read_result_from_file',
    'ResultBase',

    'AmplitudeEeg',
    'AmplitudeEegResult',
    'BrainAgeSinc',
    'BrainAgeResult',
    'CoherenceGraph',
    'CoherenceGraphResult',
    'BurstDetection',
    'BurstDetectionResult',
    'DynamicCoupling',
    'DynamicCouplingResult',
    'MultiScaleEntropy',
    'MultiScaleEntropyResult',
    'Envelope',
    'EnvelopeResult',
    'FeatureSetResult',
    'PowerAnalysis',
    'PowerAnalysisResult',
    'Psd',
    'PsdResult',
    'LineLength',
    'LineLengthResult',
    'MultifractalAnalysis',
    'MultifractalAnalysisResult',
    'SleepStages',
    'SleepStagesResult',
    'SleepStagesCnn',
    'SleepStagesCnnResult',
    'SleepStagesRobust',
    'SleepStagesRobustResult',
    'SignalStats',
    'SignalStatsResult',
    'WaveletCoherence',
    'WaveletCoherenceResult',
    'WaveletResult',
    'EdfReader',

    'read_annotation_set_KL',
    'read_annotation_set_preterm',

    'ClassWithParameters',
    'Parameters',

    'combine_channels',

    'clamp',
    'moving_envelope',
    'moving_line_length',
    'moving_mad',
    'moving_max',
    'moving_mean',
    'moving_median',
    'moving_std',

    'Butterworth',
    'FilterBase',
    'MovingAverage',
    'NotchIIR',
    'RemezFIR',
    'WinFIR',

    'list_attrs',
    'list_methods',
    'print_object_summary',

    'print_exception_info',

    'get_filepaths',
    'get_output_dir',

    'stripboxplot',
    'format_time_axis',

    'assert_equal',
]
