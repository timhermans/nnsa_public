"""
Code related to analyzing and working with text annotations that have a specific onset time and duration.
The classes in this package are for example used to read annotations from an EDF+ file and may be useful
for processing sleep stage annotations.
"""
from .annotation import Annotation
from .annotation_set import AnnotationSet


__all__ = [
    'Annotation',
    'AnnotationSet',
]
