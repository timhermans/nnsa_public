"""
Code related to a single annotation.
"""
import numpy as np

__all__ = [
    'Annotation'
]


class Annotation(object):
    """
    Universal annotation object.

    An Annotation object has four properties:
        Annotation.onset (int, float): the onset time of the annotation.
        Annotation.duration (int, float): the duration of the annotation (same unit as onset).
        Annotation.text (str): the annotation text.
        Annotation.offset (int, float): the end time of the annotation (onset + duration).

    Args:
        onset (int, float): the onset time of the annotation.
        duration (int, float): the duration of the annotation (same unit as onset).
        text (str): the annotation text.
    """
    def __init__(self, onset, duration, text):
        # Initialize protected variables.
        self._onset = None
        self._duration = None
        self._text = None

        # Set protected variables.
        self.onset = onset
        self.duration = duration
        self.text = text

    def __repr__(self):
        """
        Return a comprehensive info string about this object.

        Returns:
            (str): a comprehensive info string about this object.
        """
        return '{} with onset at {} s, duration of {} s and text "{}"'\
            .format(self.__class__.__name__, self.onset, self.duration, self.text)

    @property
    def duration(self):
        """
        Return the duration of the annotation in seconds.

        Returns:
            (float): the duration of the annotation in seconds.
        """
        return self._duration

    @duration.setter
    def duration(self, value):
        """
        Set the duration of the annotation in seconds, or np.nan if duration is not applicable.

        Args:
            value (float): the duration of the annotation in seconds, or np.nan if duration is not applicable.
        """
        # Type check.
        if not isinstance(value, (float, int, np.float, np.int, np.int32, np.int64)):
            raise ValueError('Invalid type "{}" for Annotation.duration. Type should be float or int.'
                             .format(type(value)))
        self._duration = float(value)

    @property
    def offset(self):
        """
        Return the end time (onset + duration).

        Clips duration such that it cannot be negative.
        (sometimes, duration could be set to -1 if there is no duration associated with the Annotation,
        in this case, make the offset the same as the onset by making the duration 0).
        """
        return self.onset + max([0, self.duration])

    @property
    def onset(self):
        """
        Return the onset of the annotation in seconds.

        Returns:
            (float): the onset of the annotation in seconds.
        """
        return self._onset

    @onset.setter
    def onset(self, value):
        """
        Set the onset of the annotation in seconds.

        Args:
            value (float): the onset of the annotation in seconds.
        """
        # Type check.
        if not isinstance(value, (float, int, np.float, np.int, np.int32, np.int64)):
            raise ValueError('Invalid type "{}" for Annotation.onset. Type should be float or int.'
                             .format(type(value)))
        self._onset = float(value)

    @property
    def text(self):
        """
        Return the annotation text.

        Returns:
            (str): the annotation text.
        """
        return self._text

    @text.setter
    def text(self, value):
        """
        Set the annotation text.

        Args:
            value (str): the annotation text.
        """
        # Type check.
        if type(value) is not str:
            try:
                value = str(value)
            except:
                raise ValueError('Invalid type "{}" for Annotation.text. Type should be str.'
                                 .format(type(value)))
        self._text = value
