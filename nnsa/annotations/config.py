"""
Constants related to sleep stages.
"""

__all__ = [
    'NO_LABEL',
    'STANDARD_ANNOTATIONS',
    'LABEL_ANNOTATIONS_MAPPING',
    'SLEEP_LABELS'
]

# Label for unlabeled segments.
NO_LABEL = 'NL'

# Dictionary with standard annotation texts that are used. This dictionary maps a description to a corresponding
# abbreviation/label/annotation. They values (on the right) may be changed here, and the idea is that the code
# will be compatible with the change since it always looks up the annotations in this dictionary from the description.
STANDARD_ANNOTATIONS = {
    'active_sleep': 'AS',
    'active_sleep_1': 'AS1',
    'active_sleep_2': 'AS2',
    'additional_artefact': ' + ARTEFACT',
    'artefact': 'ARTEFACT',
    'artefact_2': 'AF',
    'blinking': 'BLINKING',
    'dubious': 'DUBIOUS',
    'indeterminate_sleep': 'INDETERMINATE',
    'transitional_sleep': 'TS',
    'intermediate_sleep': 'IS',
    'movement': 'MOVEMENT',
    'no_label': NO_LABEL,
    'non_quiet_sleep': 'NQS',
    'quiet_sleep': 'QS',
    'quiet_sleep_hvs': 'QS_HVS',
    'quiet_sleep_hvs_ta': 'QS',
    'quiet_sleep_ta': 'QS_TA',
    'undetermined': 'UNDETERMINED',
    'wake': 'WAKE',
}

# Dictionary with sleep labels that are used. They are similar to the standard annotations, but sometimes as shorter
# abbreviation is used.  They may be changed here, and the idea is that the code will be compatible with the change.
# NOTE: use labels that are no normal words which can be expected in annotations (or those words may be interpreted as
# labels when reading annotations (case sensitive)).
SLEEP_LABELS = {
    'artefact': 'AF',
    'active_sleep': 'AS',
    'active_sleep_1': 'ASI',
    'active_sleep_2': 'LVI',
    'indeterminate_sleep': 'INDS',
    'transitional_sleep': 'TS',
    'no_label': NO_LABEL,
    'non_quiet_sleep': 'NQS',
    'quiet_sleep': 'QS',
    'quiet_sleep_hvs': 'QS HVS',
    'quiet_sleep_ta': 'QS TA',
    'wake': 'Aw',
}

# Specify which standard annotation texts belong to a standard sleep label.
LABEL_ANNOTATIONS_MAPPING = {
    # Artefacts:
    SLEEP_LABELS['artefact']: [STANDARD_ANNOTATIONS[key] for key in ['artefact', 'artefact_2', 'blinking', 'movement']],

    # All active sleep:
    SLEEP_LABELS['active_sleep']: [STANDARD_ANNOTATIONS[key] for key in ['active_sleep', 'active_sleep_1',
                                                                         'active_sleep_2']],

    # AS1:
    SLEEP_LABELS['active_sleep_1']: [STANDARD_ANNOTATIONS[key] for key in ['active_sleep_1']],

    # AS2:
    SLEEP_LABELS['active_sleep_2']: [STANDARD_ANNOTATIONS[key] for key in ['active_sleep_2']],

    # Indeterminate sleep:
    SLEEP_LABELS['indeterminate_sleep']: [STANDARD_ANNOTATIONS[key] for key in ['dubious', 'undetermined']],

    # Intermediate sleep:
    SLEEP_LABELS['transitional_sleep']: [STANDARD_ANNOTATIONS[key] for key in ['transitional_sleep']],

    # All non-quiet sleep:
    SLEEP_LABELS['non_quiet_sleep']: [STANDARD_ANNOTATIONS[key] for key in ['active_sleep', 'active_sleep_1',
                                                                            'active_sleep_2', 'wake',
                                                                            'non_quiet_sleep']],

    # All quiet sleep:
    SLEEP_LABELS['quiet_sleep']: [STANDARD_ANNOTATIONS[key] for key in ['quiet_sleep', 'quiet_sleep_hvs',
                                                                        'quiet_sleep_hvs_ta', 'quiet_sleep_ta']],

    # QS HVS:
    SLEEP_LABELS['quiet_sleep_hvs']: [STANDARD_ANNOTATIONS[key] for key in ['quiet_sleep_hvs']],

    # QS TA:
    SLEEP_LABELS['quiet_sleep_ta']: [STANDARD_ANNOTATIONS[key] for key in ['quiet_sleep_ta']],

    # Wakefulness:
    SLEEP_LABELS['wake']: [STANDARD_ANNOTATIONS[key] for key in ['wake']],
}
