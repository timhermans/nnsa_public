"""
This module contains constants and defaults related to the entire nnsa project.
"""
from os.path import abspath, join, dirname

__all__ = [
    'PKG_DIR',
    'MODEL_DATA_DIR',
    'NNSA_DIR',
]

pkg_dir = dirname(__file__)


def up(path, n=1):
    # Go n times up a directory in path.
    for _ in range(n):
        path = dirname(path)
    return path


# Specify some shortcuts to commonly used paths.
# Path to top-level nnsa directory.
nnsa_dir = up(pkg_dir, 1)

# Path to the directory with the model data (e.g. CNN weights, random forest, etc.).
model_data_dir = join(pkg_dir, 'model_data')

# Convert relative paths to absolute paths.
PKG_DIR = abspath(pkg_dir)
NNSA_DIR = abspath(nnsa_dir)
MODEL_DATA_DIR = abspath(model_data_dir)
