"""
This package defines objects that contain data.
"""
from .datasets import EegDataset
from .time_series import TimeSeries

__all__ = [
    'EegDataset',

    'TimeSeries'
]
