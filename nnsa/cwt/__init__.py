"""
This module contains code for analyses that make use of the continuous wavelet transform (CWT).
"""
from .mothers import Mother, Morlet, Paul, get_wavelet
from .plotting import plot_cwt_scalogram, plot_scalogram, plot_tf_map
from .transforms import compute_partial_coherence, compute_wavelet_coherence, cwt, cwt_2d, icwt, pct, wct, xwt_smooth
from .utils import *

__all__ = [
    'Mother',
    'Morlet',
    'Paul',
    'get_wavelet',

    'plot_cwt_scalogram',
    'plot_scalogram',
    'plot_tf_map',

    'compute_partial_coherence',
    'compute_wavelet_coherence',
    'cwt',
    'cwt_2d',
    'icwt',
    'pct',
    'wct',
    'xwt_smooth',

    'autoscale_rec',
    'compute_coi',
    'compute_moi',
    'compute_power_cwt',
    'compute_reconstruction',
    'cwt_power',
    'cwt_total_power',
    'reconstruct_x',
]
