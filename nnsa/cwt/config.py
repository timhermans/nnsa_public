from nnsa.cwt.mothers import Morlet

__all__ = [
    'DEFAULT_WAVELET',
]

DEFAULT_WAVELET = Morlet(6)
