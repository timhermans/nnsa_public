"""
Code related to signal decomposition.
"""

from .fourier import *
from .side_obsp import *

__all__ = [
    'fftdeconvolve',

    'SideObsp',
    'create_block_hankel',
]

