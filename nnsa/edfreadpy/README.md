<!-- ABOUT THE PROJECT -->
## About
Reader for EDF and EDF+ files in Python.
Includes functionality to read annotations and to anonymize the EDF header.

<!-- USAGE EXAMPLES -->
## Usage
```python
from edfreadpy import EdfReader

# Specify filepath to an EDF(+) file.
filepath = 'your_edf_file.EDF'

# Use a context manager, which automatically closes the file when leaving the context block.
with EdfReader(filepath) as r:
    # Read first signal in the file (first channel).
    signal = r.read_signal(channel=0)
    signal_label = r.signal_headers['label'][0]

print('Done reading signal "{}": {}'.format(signal_label, signal))
```

See the tutorial script [../../examples/io/edf_reader.py](../../examples/io/edf_reader.py) for more examples.
