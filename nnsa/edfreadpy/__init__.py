"""
Package for reading (and anonymizing) EDF and EDF+ files.
"""
from .io.reader import EdfReader

__all__ = [
    'EdfReader',
]
