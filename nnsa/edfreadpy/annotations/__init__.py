"""
Code related to analyzing and working with annotations. This exists for backwards compatibility reasons.
"""
import warnings

msg = '\nThis module is deprecated. Use nnsa.annotations instead.'
warnings.warn(msg)
