"""
Code related to a single annotation.
"""
from nnsa.annotations import Annotation

import warnings

__all__ = [
    'Annotation'
]

msg = '\nThis module is deprecated. Use nnsa.annotations.annotation instead.'
warnings.warn(msg)
