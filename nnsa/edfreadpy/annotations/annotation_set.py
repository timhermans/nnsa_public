"""
Code related to annotation sets.
"""
from nnsa.annotations.annotation_set import AnnotationSet

import warnings

__all__ = [
    'AnnotationSet',
]

msg = '\nThis module is deprecated. Use nnsa.annotations.annotation_set instead.'
warnings.warn(msg)
