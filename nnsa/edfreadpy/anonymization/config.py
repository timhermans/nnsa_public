"""
Module with general constants for anonymization.
"""

__all__ = [
    'MIN_DAYS_TO_SHIFT',
    'MAX_DAYS_TO_SHIFT'
]

# Constants for random date generator.
MIN_DAYS_TO_SHIFT = round(365.25*1)  # + 1 years
MAX_DAYS_TO_SHIFT = round(365.25*50)  # + 50 years
