"""
This module contains constants and defaults related to reading data files.
"""

__all__ = [
    'EEG_TYPE',
    'EEG_SENSORS',
    'EEG_LABELS',

    'BASIC_UNITS',
    'UNIT_PREFIXES',

    'EDF_EXTENSIONS',
    'EDFPLUS_TYPES',

    'INT_TO_MONTH',
    'MONTH_TO_INT',

    'default_edf_file_header',
    'default_edf_signal_header'
    ]

# Define standard signal labels.
# According to the standard text rules of EDF (https://www.edfplus.info/specs/edftexts.html) a signal label consists of:
# type of signal, a space, and a specification of the sensor. E.g. 'EEG Fpz-Cz'). Standard text should be used for the
# type of signal and for sensor specification.

# EEG:
EEG_TYPE = 'EEG'
EEG_SENSORS = [
                  'Fp1', 'Fp2',
          'F7', 'F3', 'Fz', 'F4', 'F8',
    'A1', 'T3', 'C3', 'Cz', 'C4', 'T4', 'A2',
          'T5', 'P3', 'Pz', 'P4', 'T6',
                  'O1',  'O2',
    'REF',
]
EEG_LABELS = [' '.join([EEG_TYPE, sensor]) for sensor in EEG_SENSORS]

# # Oxygen:
# SPO2_LABEL = 'SpO2'
# SAO2_LABEL = 'SaO2'
#

# ECG.
ECG_TYPE = 'ECG'
ECG_SENSORS = ['I',
               'II',
               'III',]
ECG_LABELS = [ECG_TYPE] + ['{} {}'.format(ECG_TYPE, sens) for sens in ECG_SENSORS]

# Define valid file extensions (without leading dot).
EDF_EXTENSIONS = ['edf', 'EDF']

# Define supported EDF+ file types.
EDFPLUS_TYPES = ['EDF+C', 'EDF+D']  # EDF+D is not supported until this is needed.

# Define standard units.
BASIC_UNITS = [
    '%',
    'A',
    'K',
    'V',
    'deg',
    'degC',
    'degF',
    'g',
    'Hz',
    'm',
    'mol',
    'rad',
    's'
]

# Define (expected) prefixes for the units (e.g. kilo, centi, etc.). Code assumes that prefixes have length 1.
UNIT_PREFIXES = [
    # 'Y',
    # 'Z',
    # 'E',
    # 'P',
    # 'T',
    'G',
    'M',
    'K', 'k',  # Accept both upper and lower case for kilo
    # 'H', 'h',
    # 'D',
    '',  # do not forget the 0 power
    'd',
    'c',
    'm',
    'u',  # use u instead of Greek mu
    'n',
    # 'f',
    # 'a',
    # 'z',
    # 'y',
]

# Create dicts to convert 3 letter month abbreviations to corresponding integers.
ints = range(1, 13)
months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
INT_TO_MONTH = dict(zip(ints, months))
MONTH_TO_INT = dict(zip(months, ints))


def default_edf_file_header():
    """
    Return the default file header for EDF file.

    Returns:
        (dict) dictionary populated with default file header keys with value None.
    """
    file_header_fields = [
        'version',
        'patient_id',
        'recording_id',
        'startdate',
        'starttime',
        'size_header',  # in bytes
        'reserved',
        'num_datarecords',
        'duration_datarecord',  # in seconds
        'num_signals',
    ]

    # Return a dictionary with the header fields as keys, each with default value None.
    return dict.fromkeys(file_header_fields, None)


def default_edf_signal_header():
    """
    Return the default signal header for EDF file.

    Returns:
        (dict) dictionary populated with default signal header keys with value None.
    """
    signal_header_fields = [
        'label',
        'transducer',
        'physical_dimension',
        'physical_min',
        'physical_max',
        'digital_min',
        'digital_max',
        'prefilter',
        'num_samples',
        'reserved'
    ]

    # Return a dictionary with the header fields as keys, each with default value None.
    return dict.fromkeys(signal_header_fields, None)
