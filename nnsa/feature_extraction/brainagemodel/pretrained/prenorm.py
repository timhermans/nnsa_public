import pickle
import os

def get_norm_pars():
    root = os.path.dirname(__file__)
    fname = f'{root}/norm.pkl'
    with open(fname, 'rb') as f:
        r = pickle.load(f)
    return r

def pre_normalize(eeg_in_volts):
    r = get_norm_pars()
    return (eeg_in_volts - r['sub']) / r['den']
