import numpy as np

__all__ = [
    'compute_rbf_1d',
]


def compute_rbf_1d(x, y, gamma):
    d = x - y
    return np.exp(-gamma * np.dot(d, d))
