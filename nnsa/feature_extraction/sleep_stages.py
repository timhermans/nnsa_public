"""
Code for sleep stage-related feature extraction (after classification/annotation wct)
"""
import copy
import csv
import datetime
import os
import pickle
import warnings
from collections import defaultdict
from functools import partial

import h5py
import joblib
import numpy as np
import scipy.signal
from lifelines import KaplanMeierFitter, ExponentialFitter
from matplotlib import pyplot as plt
from matplotlib.colors import to_rgba
from matplotlib.patches import Rectangle
from matplotlib.ticker import FuncFormatter
import matplotlib.patches as mpatches

from scipy import interpolate, signal
import pandas as pd
from scipy.signal import convolve

from nnsa.annotations.annotation import Annotation
from nnsa.annotations.annotation_set import AnnotationSet
from nnsa.annotations.config import STANDARD_ANNOTATIONS, SLEEP_LABELS
from nnsa.annotations.sleep_stages import annotation_to_label, standardize_annotation
from nnsa.artefacts.artefact_detection import detect_artefact_signals, detect_high_amplitudes, detect_anomalous_channels
from nnsa.artefacts.clean_detector_cnn import CleanDetectorCnn
from nnsa.preprocessing.data_cleaning import substitute_bad_channels
from nnsa.utils.arrays import check_eeg_is_long, stepwise_reduce, clamp, mirror_boundaries
from nnsa.feature_extraction.feature_sets.base import FeatureSetResult
from nnsa.feature_extraction.feature_sets.eeg import compute_features
from nnsa.models.evaluation import ConfusionMatrix
from nnsa.config import MODEL_DATA_DIR
from nnsa.preprocessing.resample import resample_by_filtering
from nnsa.preprocessing.saved_filters import filter_cnn_ansari_2019
from nnsa.feature_extraction.result import ResultBase
from nnsa.io.hdf5 import read_dict_from_hdf5, write_dict_to_hdf5
from nnsa.parameters.parameters import ClassWithParameters, Parameters
from nnsa.utils.config import HORIZONTAL_RULE
from nnsa.utils.event_detections import get_onsets_offsets
from nnsa.utils.mathematics import compute_entropy
from nnsa.utils.normalization import normalize_eeg_channels
from nnsa.utils.other import convert_string_auto, convert_string
from nnsa.utils.pkl import pickle_load
from nnsa.utils.plotting import subplot_rows_columns, maximize_figure, fillplot, shade_axis, pieplot, format_time_axis
from nnsa.utils.conversions import convert_time_scale
from nnsa.utils.segmentation import compute_n_segments, get_all_segments
from nnsa.utils.testing import assert_equal

__all__ = [
    'SleepStages',
    'SleepStagesResult',
    'SleepStagesCnn',
    'SleepStagesCnnResult',

    'add_sleep_features_2',
    'get_cnn_sleep_result',
]


LABEL_COLORS_OLD = {
    'QS TA': '#6199C8',
    'QS HVS': '#A5BDDF',
    'ASI': '#FFCCDF',
    'LVI': '#ED95BB',
    'TS': '#BD97D4',
    'Movement': '#F0CA86',
    'Novelty': '#D55656',
    'Artefact': '#838383',
    'Uncertain': '#c9c9c9',
}

LABEL_COLORS = {
    'LVI': '#4063A0',
    'ASI': '#6D8DC5',
    'AS': np.array([ 86.5, 120. , 178.5])/255,
    'TS': '#EB7D30',
    'QS HVS': '#CBCCCD',
    'HVS': '#CBCCCD',
    'QS TA': '#A1A3A5',
    'TA': '#A1A3A5',
    'QS': np.array([182. , 183.5, 185. ])/255,
    'Sleep': np.array([134.25, 151.75, 181.75])/255,
    'Movement': '#B58503',
    'Artefact': '#FBBD10',
    'Uncertain': '#CE3B3B',
    'Novelty': '#DA6C6C',
}


# Order of the sleep labels in the 4-stage hypnogram (top-to-bottom).
HYPNOGRAM_ORDER = ['LVI', 'ASI', 'QS HVS', 'QS TA']


class SleepStages(ClassWithParameters):
    """
    Sleep stage classification from text annotations.

    main method: sleep_stages()

    Args:
        see nnsa.ClassWithParameters.

    Examples:
        >>> ss = SleepStages()
        >>> assert_equal(ss.parameters, SleepStages.default_parameters())
        >>> print(type(ss.parameters).__name__)
        Parameters

        >>> annotation_set = AnnotationSet([Annotation(0, 100, 'QS TA'), Annotation(139.2, 60.8, 'AS1')])
        >>> ss_result = ss.sleep_stages(annotation_set)
        >>> print(type(ss_result).__name__)
        SleepStagesResult

        >>> print(ss_result.proportion('QS'))
        0.5
    """
    @staticmethod
    def default_parameters():
        """
        Return the default parameters as a dictionary.

        Returns:
            (nnsa.Parameters): a default set of parameters for the sleep stages.
        """
        # Define the classes to use, see nnsa.annotations.sleep_stages.annotation_to_label(). Note that the order of
        # the classes will determine their corresponding integer class number in SleepStagesResult:
        class_labels = [
            # Use class non quiet sleep, labeled as NQS, will be mapped to 0:
            'NQS',
            # Use class quiet sleep, labeled as QS, will be mapped to 1:
            'QS',
        ]

        # Keyword arguments for nnsa.annotations.sleep_stages.annotation_to_label()
        # (do not specify the `inplace` argument):
        annotation_to_label_kwargs = Parameters(**{
            # Specify whether to include sleep stage annotations/labels that also indicate the presence of artefacts:
            'include_labels_with_artefacts': False,
        })

        # Keyword arguments for AnnotationSet.interpolate_artefacts() (do not specify the `inplace` argument):
        interpolate_artefacts_kwargs = Parameters(**{
            # List of annotation texts indicating artefact segments. If None, a default list of artefact-related
            # annotation texts is used:
            'artefact_texts': None,
            # To disable interpolation, set max_duration to 0:
            'max_duration': 0,
        })

        # Keyword arguments for SleepStagesResult.remove_short_stages() (do not specify the `inplace` argument):
        remove_short_stages_kwargs = Parameters(**{
            # To remove nothing, set min_duration to 0:
            'min_duration': 0,
        })

        pars = {
            # The classes to use, see annotation_to_label():
            'class_labels': class_labels,
            # Keyword arguments for annotation_to_label() (do not specify the `inplace` argument):
            'annotation_to_label_kwargs': annotation_to_label_kwargs,
            # Keyword arguments for AnnotationSet.interpolate_artefacts():
            'interpolate_artefacts_kwargs': interpolate_artefacts_kwargs,
            # Keyword arguments for SleepStagesResult.remove_short_stages():
            'remove_short_stages_kwargs': remove_short_stages_kwargs,
        }

        return Parameters(**pars)

    def annotation_conversions(self, annotation_set):
        """
        Convert each unique annotation in the annotation set to a standard annotation text and a sleep label.

        Can be used to see how individual annotations are converted to sleep labels.

        Args:
            annotation_set (nnsa.AnnotationSet): text annotations that need to be converted to sleep stages.

        Returns:
            (pd.DataFrame): DataFrame with columns 'original_text', 'standard_text' and 'sleep_label'.
        """
        annotation_to_label_kwargs = self.parameters['annotation_to_label_kwargs']
        class_labels = self.parameters['class_labels']

        # Collect unique annotation texts in DataFrame.
        df = pd.DataFrame({'original_text': np.unique(annotation_set.texts())})

        # Convert to standard texts in a new column.
        df['standard_text'] = df['original_text'].apply(standardize_annotation)

        # Convert to sleep labels in a new column
        annot_2_lab = partial(annotation_to_label, class_labels=class_labels, **annotation_to_label_kwargs)
        df['sleep_label'] = df['standard_text'].apply(annot_2_lab)

        return df

    def sleep_stages(self, annotation_set, verbose=1):
        """
        Infer the course of the sleep stages from the annotations.

        1) Sort the annotations based on onset.
        2) Standardize the annotations, i.e., find a standard annotation text for each annotation.
        3) Convert the standard texts to sleep stage labels (controlled by class_mapping and
        include_labels_with_artefacts parameters).
        4) Fill unlabeled periods using no_label annotations.
        5) Merge successive annotations if their label is the same.
        6) Interpolate/replace artefact segments (controlled by interpolate_no_label_kwargs parameter).
        7) Remove short sleep stages (controlled by remove_short_stages_kwargs parameter).

        Args:
            annotation_set (nnsa.AnnotationSet): text annotations that need to be converted to sleep stages.
            verbose (int, optional): verbose level.
                Defaults to 1.

        Returns:
            (nnsa.SleepStagesResult): SleepStagesResult object containing the annotations related to sleep stages.
        """
        if verbose > 0:
            print('Extracting sleep stages with parameters:')
            print(self.parameters)

        annotation_to_label_kwargs = self.parameters['annotation_to_label_kwargs']
        class_labels = self.parameters['class_labels']
        interpolate_artefacts_kwargs = self.parameters['interpolate_artefacts_kwargs']
        remove_short_stages_kwargs = self.parameters['remove_short_stages_kwargs']

        # Input check.
        if any(np.isnan(annotation_set.durations())):
            raise NotImplementedError('Duration not specified in (some) annotations. '
                                      'Must be specified to determine (the course of) sleep stages. '
                                      'Tip: AnnotationSet.compute_nan_durations() could be used to infer durations '
                                      'from inter-annotation time intervals.')

        # Add artefacts to class labels if not already in there (for conversion of standard texts to labels and the
        # interpolation of artefacts).
        artefact_label = SLEEP_LABELS['artefact']
        if artefact_label in class_labels:
            af_in_class_labels = True
        else:
            af_in_class_labels = False
            class_labels.append(artefact_label)

        # 1. Sort annotations based on onset.
        annotation_set = annotation_set.sort(inplace=False)

        # 2. Standardize the annotation texts.
        annotation_set.standardize_annotations(inplace=True)

        # 3. Convert the standard texts to sleep stage labels.
        sleep_annotation_set = AnnotationSet(label='{}, sleep stages'.format(annotation_set.label))
        for a in annotation_set:
            a.text = annotation_to_label(a.text, class_labels,
                                         **annotation_to_label_kwargs)
            sleep_annotation_set.append(a, inplace=True)

        # 4. Fill unlabeled periods, so that we have a continuous annotations.
        sleep_annotation_set.fill_unlabeled_periods(inplace=True)

        # 5. Merge successive annotations if their label is the same.
        sleep_annotation_set.merge_successive_texts(inplace=True)

        # 6. Interpolate/replace the no_label.
        sleep_annotation_set.interpolate_artefacts(**interpolate_artefacts_kwargs, inplace=True)

        if not af_in_class_labels:
            # Remove artefact label if they were not in class labels originally.
            artefact_label = class_labels.pop()

            # Remove artefact labels in annotation set (replace by no_label).
            sleep_annotation_set.replace_texts(texts=artefact_label,
                                               new_text=SLEEP_LABELS['no_label'],
                                               inplace=True)

        # Make the class_mapping dictionary, the order of the class labels determines the class number.
        class_mapping = dict(zip(class_labels, range(len(class_labels))))
        class_mapping[SLEEP_LABELS['no_label']] = np.nan

        # Create sleep_stages_annotations object.
        sleep_stages_result = SleepStagesResult(sleep_annotation_set,
                                                algorithm_parameters=self.parameters,
                                                class_mapping=class_mapping)

        # 7. Remove short sleep stages.
        sleep_stages_result.remove_short_stages(**remove_short_stages_kwargs, inplace=True)

        return sleep_stages_result


class SleepStagesResult(ResultBase):
    """
    High-level interface for manipulating sleep stage annotations as created by nnsa.SleepStages().sleep_stages().

    Args:
        annotation_set (nnsa.AnnotationSet): AnnotationSet consisting of sleep labels. All labels must have a
            duration. Each unique label is considered a class.
        algorithm_parameters (nnsa.Parameters, optional): see ResultBase.
        class_mapping (dict, optional): dictionary that maps a class label (annotation) to a class number. If None,
            a default mapping is applied with class numbers ranging from 0 to N-1, with N the number of unique
            class labels.
            Defaults to None.
        data_info (str, optional): see ResultBase.
    """
    def __init__(self, annotation_set, algorithm_parameters=None, class_mapping=None, data_info=None):
        # Call parent's __init___.
        super().__init__(algorithm_parameters=algorithm_parameters, data_info=data_info)

        if class_mapping is None:
            # Use a default mapping of the labels to class numbers.
            # Get all the unique class labels.
            unique_class_labels = np.unique([a.text for a in annotation_set]).tolist()

            # Remove no_label, create a default mapping for the real labels using integers, and add
            # no_label afterwards and map it to nan.
            if SLEEP_LABELS['no_label'] in unique_class_labels:
                unique_class_labels.remove(SLEEP_LABELS['no_label'])
                add_no_label = True
            else:
                add_no_label = False
            class_mapping = dict(zip(unique_class_labels, range(len(unique_class_labels))))
            if add_no_label:
                class_mapping[SLEEP_LABELS['no_label']] = np.nan

        self.annotation_set = annotation_set
        self.class_mapping = class_mapping

    def __iter__(self):
        """
        Return iterator that iterates over Annotation objects in this SleepStagesResult object.

        This makes it possible to iterate over the annotations in the annotation_set attributes by iterating over
        SleepStagesResult directly instead of having to iterate over SleepStagesResult.annotation_set.

        Returns:
            (list_iterator): iterator that iterates over Annotation objects in this SleepStagesResult.
        """
        # Return the iterator of the annotation_set attribute.
        return self.annotation_set.__iter__()

    @property
    def annotation_set(self):
        return self._annotation_set

    @annotation_set.setter
    def annotation_set(self, annotation_set):
        """
        Set the annotation_set property.

        Args:
            annotation_set (nnsa.AnnotationSet): AnnotationSet consisting of sleep labels. All labels must have a
                duration. Each unique label is considered a class.

        Raises:
            TypeError: if annotation_set if not of type nnsa.AnnotationSet.
            ValueError: if annotation_set contains annotations with unspecified duration.
        """
        # Check type.
        if not isinstance(annotation_set, AnnotationSet):
            raise TypeError('Invalid input of type "{}". Input must be "{}".'
                            .format(type(annotation_set).__name__, 'AnnotationSet'))

        # Check durations of annotations.
        if any(np.isnan(annotation_set.durations())):
            raise ValueError('Unspecified duration (np.nan) of sleep stages in sleep_annotation_set.')

        # Set the attribute.
        self._annotation_set = annotation_set

    @property
    def class_labels(self):
        return list(self.class_mapping.keys())

    @property
    def class_mapping(self):
        return self._class_mapping

    @property
    def class_mapping_invert(self):
        """
        Invert class mapping.

        Returns:
            mapping (dict): inverted class mapping.
        """
        mapping = {v: k for k, v in self.class_mapping.items()}
        return mapping

    @class_mapping.setter
    def class_mapping(self, class_mapping):
        """
        Set the class_mapping property.

        Args:
            class_mapping (dict): dictionary that maps a class label (annotation) to a class number.

        Raises:
            ValueError: if not all unique labels in the annotation set are mapped to a class numberin class_mapping.
            ValueError: if the class numbers given in class_mapping are not unique.
        """
        # Get all the unique class labels.
        unique_class_labels = np.unique([a.text for a in self.annotation_set]).tolist()

        # Verify that all classes that appear in the annotations are mapped to an integer in class_mapping.
        if not all([label in class_mapping for label in unique_class_labels]):
            raise ValueError('Not all unique labels in the annotation set ({}) are mapped to an integer class '
                             'number in specified class_mapping argument ({}).'.format(unique_class_labels,
                                                                                       class_mapping))
        # Verify that there are no double class numbers.
        if len(np.unique(list(class_mapping.values()))) != len(list(class_mapping.values())):
            raise ValueError('Class numbers in class_mapping argument are not unique: {}'
                             .format(class_mapping))

        # Set the attribute.
        self._class_mapping = class_mapping

    @property
    def num_segments(self):
        """
        Return the number of segments/samples.

        Returns:
            (int): number of segments/samples.
        """
        # No segments.
        return None

    @property
    def total_duration(self):
        """
        Return the total duration of the annotated period in seconds.

        Returns:
            (float): total duration in seconds.
        """
        return self.annotation_set.durations().sum()

    def class_numbers_to_labels(self, class_numbers):
        """
        Convert class numbers to class labels.

        Nans are convert to `no_label` labels.

        Args:
            class_numbers (list, np.ndarray): class numbers to convert.

        Returns:
            labels (list): class labels.
        """
        labels = []
        for num in class_numbers:
            # Check for nan (does not work as a key in dicts).
            if np.isnan(num):
                lab = SLEEP_LABELS['no_label']
            else:
                lab = self.class_mapping_invert[num]
            labels.append(lab)
        return labels

    def compute_global_features(self, **kwargs):
        """
        Compute global features.

        Args:
            **kwargs (optional): keyword arguments for pd.DataFrame.

        Returns:
            df (pd.DataFrame): dataframe with one row, and feature values in columns.
        """
        # Collect features for every sleep stage.
        global_features = dict()
        for class_label in self.class_mapping.keys():
            if class_label == SLEEP_LABELS['no_label']:
                # Skip no label segments.
                continue
            features = self.extract_features(class_label)
            global_features.update(features)

        df = pd.DataFrame(global_features, **kwargs)

        return df

    def count_annotations_per_class(self, default_value=0):
        """
        Count the number of annotations per class.

        Args:
            default_value (float, optional): default value if no annotations of a class are present.
                Set this e.g. to np.nan or zero.
                Defaults to 0.

        Returns:
            count_dict (dict): dictionary with the counts per class.
        """
        count = self.to_dataframe().groupby('text')['onset'].count()
        count_dict = {}
        for k in self.class_mapping:
            count_dict[k] = count.get(k, default_value)
        return count_dict

    def create_mask(self, class_label, query_times, check_class_label=True, no_label_to_nan=False):
        """
        Return a boolean mask for (any of the) class_label on the query time points.

        Args:
            class_label (str or list): the label(s) of the class to get the mask from. If a list, True is
                returned if the label at the query point is in class_label.
            query_times (np.ndarray): query time points for the mask in seconds.
            check_class_label (bool, optional): check if the class labels that are passed exist in self.
                If check_class_label is True and a label does not occur in self, an error is raised.
                Defaults to True.
            no_label_to_nan (bool, optional): returns a mask with nans where the label is 'no_label'.
                If False, returns False at locations where the sleep label is nan.
                Defaults to False.

        Returns:
            mask (np.ndarray): boolean array with same shape as time specifying whether the sleep stage equals the
                class_label at the corresponding time point.
        """
        if not isinstance(class_label, list):
            class_label = [class_label]

        # Interpolate.
        interpolated_classes = self.interpolate_classes(query_times=query_times, kind='previous')

        mask = np.zeros(np.shape(interpolated_classes), int)
        for label in class_label:
            # Check class_label.
            if label not in self.class_mapping:
                if check_class_label:
                    self._check_class_label(label)
                else:
                    # Skip this label.
                    continue

            # Extract the class number of the given class label.
            class_number = self.class_mapping[label]

            # Return mask.
            if np.isnan(class_number):
                mask += np.isnan(interpolated_classes)
            else:
                mask += (interpolated_classes == class_number).astype(int)

        # Return True if any of the labels in class_labels is found at the query point.
        mask = mask > 0

        # If desired, replace points that correspond to 'no_label' with nan.
        if no_label_to_nan:
            mask = mask.astype(float)  # Use floats to insert nans.
            mask[self.create_mask(SLEEP_LABELS['no_label'], query_times, check_class_label=False,
                                  no_label_to_nan=False)] = np.nan

        return mask

    def cycle_count(self, class_label, ignore_labels=None):
        """
        Count the number of times a sleep stage transitioned into a different one and back.

        Ignores no_label.

        Assumes the annotations are sorted by onset.

        Args:
            class_label (str): the label of the sleep stage for which to compute the cycle_count.
            ignore_labels (str or list, optional): specify a class label or list of class labels to ignore. These labels
                won't be considered as valid transitions (e.g. artefacts, no_label). no_label segments will
                automatically be ignored. If None, a default list of labels to ignore will be used.
                Defaults to None.

        Returns:
            (int): number of interruptions.
        """
        # Check class_label.
        self._check_class_label(class_label)

        if ignore_labels is None:
            ignore_labels = [
                SLEEP_LABELS['artefact'],
            ]
        elif isinstance(ignore_labels, str):
            # Convert to list.
            ignore_labels = [ignore_labels]

        # Always add no_label to ignore list.
        if not SLEEP_LABELS['no_label'] in ignore_labels:
            ignore_labels.append(SLEEP_LABELS['no_label'])

        texts = self.annotation_set.texts()
        idx_all = np.where(texts == class_label)[0]
        count = 0
        # Look at labels between the found class labels.
        for i in range(len(idx_all) - 1):
            # If at least one label in the interval is not to be ignored, the transition is valid.
            labels_in_interval = texts[idx_all[i] + 1: idx_all[i + 1]]
            if any((lab not in ignore_labels for lab in labels_in_interval)):
                # Increment cycle count.
                count += 1
        return count

    def extract_cleanest_epoch(self, epoch_length, dt=1, inplace=False):
        """
        Extract the cleanest epoch (a window of `epoch_length` with least 'NL' coverage).

        Args:
            epoch_length (float): length of the epoch to extract (in seconds).
            dt (float): stepsize when searching for the cleanest epoch.
                Decrease for more accuracy, at the cost of computational time.
            inplace (bool): whether to extract inplace or not.

        Returns:
            (SleepStagesResult): extracted epoch, if inplace is False.
        """
        # Create query time vector.
        t_q = np.arange(self.annotation_set.start_time(), self.annotation_set.end_time(), dt)

        # Get labels at query times and find the nans.
        nan_mask = np.isnan(self.interpolate_classes(query_times=t_q)).astype(float)

        # The length of the epoch window in samples.
        n_epoch = int(epoch_length / (t_q[1] - t_q[0]))

        # Convolve the nan_mask to find the coverage of nans in each possible epoch.
        nan_coverage = convolve(nan_mask, np.ones(n_epoch) / n_epoch, mode='valid')

        # Fix floating point precision problems.
        decimals = int(np.ceil(np.log10(n_epoch)))
        nan_coverage = nan_coverage.round(decimals=decimals)

        # Find epoch with least amounts of nan.
        idx_start = np.argmin(nan_coverage)
        t_start = t_q[idx_start]
        t_stop = t_start + (n_epoch * dt)

        # Extract epoch.
        return self.extract_epoch(begin=t_start, end=t_stop, inplace=inplace)

    def extract_epoch(self, begin=None, end=None, inplace=False):
        """
        Extract epoch specified by begin and end.

        Args:
            begin (float): begin time of epoch (seconds).
            end (float): end time of epoch (seconds).
            inplace (bool): whether to apply the operation inplace (True) or not (False).

        Returns:
            sleep_stages_result (SleepStagesResult): new object with extracted epoch (if inplace if False).
        """
        if inplace:
            sleep_stages_result = self
        else:
            sleep_stages_result = copy.deepcopy(self)

        # Extract annotations.
        sleep_stages_result.annotation_set = sleep_stages_result.annotation_set.extract_epoch(
            begin=begin,
            end=end)

        if not inplace:
            return sleep_stages_result

    def extract_global_features(self, postfix=None):
        """
        Extract features that characterize the entire recording.

        Args:
            postfix (str, optional): postfix for the keys in the output dictionary.
                If None, no postfix is added.
                Defaults to None.

        Returns:
            (dict): dictionary containing the feature name and value pairs.
        """
        # Collect features for every sleep stage.
        global_features = dict()
        for class_label in self.class_mapping.keys():
            features = self.extract_features(class_label)
            if postfix:
                for key in list(features.keys()):  # Note the use of a list instead of iterator.
                    features['SLEEP_{}_{}'.format(key, postfix)] = features.pop(key)
            global_features.update(features)

        return global_features

    def extract_features(self, class_label):
        """
        Extract some features from the sleep stage annotations.

        Sorts the annotations (inplace).

        Args:
            class_label (str): the class label to extract features from.

        Returns:
            (dict): dictionary with features.
        """
        # Check class_label.
        self._check_class_label(class_label)

        # Sort by onset (to be sure).
        self.annotation_set.sort(inplace=True)

        inter_class_intervals = self.inter_class_intervals(class_label)
        median_duration, iqr_duration = [s.get(class_label, np.nan) for s in self.median_iqr_duration_per_class()]
        total_duration = self.total_duration
        features = {
            'SLEEP_mean_inter_{}_interval'.format(class_label): np.nanmean(inter_class_intervals),
            'SLEEP_std_inter_{}_interval'.format(class_label): np.nanstd(inter_class_intervals),
            'SLEEP_cycles_{}_per_h'.format(class_label): self.cycle_count(class_label) / total_duration,
            'SLEEP_median_duration_{}'.format(class_label): median_duration,
            'SLEEP_iqr_duration_{}'.format(class_label): iqr_duration,
            'SLEEP_proportion_{}'.format(class_label): self.proportion(class_label),
        }
        return features

    def get_classes(self):
        """
        Convert the annotation labels to class numbers and return them as an array.

        Returns:
            (np.ndarray): array with the class numbers corrsponding to the sleep stage annotations.
        """
        # Convert class labels to class numbers.
        classes = []
        for annot in self.annotation_set:
            label = annot.text
            class_number = self.class_mapping[label]
            classes.append(class_number)

        return np.array(classes)

    def inter_class_intervals(self, class_label, ignore_labels=None):
        """
        Compute the intervals between a specific sleep stages, specified by class_label.

        Assumes the annotations are sorted by onset.

        Args:
            class_label (str): the label of the sleep stage for which to compute the intervals.
            ignore_labels (str or list, optional): specify a class label or list of class labels to ignore. These labels
                won't be considered as valid intervals (e.g. artefacts, no_label). If None, ignore nothing.
                Defaults to None.

        Returns:
            (np.ndarray): array with all the intervals that were found.
        """
        # Check class_label.
        self._check_class_label(class_label)

        if ignore_labels is None:
            ignore_labels = []

        # Sum the durations between class_label labels.
        texts = self.annotation_set.texts()
        onsets = self.annotation_set.onsets()
        idx_all = np.where(texts == class_label)[0]
        all_intervals = []
        for i in range(len(idx_all) - 1):
            # If at least one label in the interval is not to be ignored, the interval is valid.
            labels_in_interval = texts[idx_all[i] + 1: idx_all[i + 1]]
            if any((lab not in ignore_labels for lab in labels_in_interval)):
                # Add interval.
                interval = onsets[idx_all[i + 1]] - onsets[idx_all[i] + 1]
                all_intervals.append(interval.sum())
        return np.array(all_intervals)

    def interpolate_artefacts(self, artefact_texts=None, max_duration=180, inplace=False, **kwargs):
        """
        Interpolate artefact labels if they are short enough and surrounded by the same non-artefact label.

        See Also:
            AnnotationSet.interpolate_artefacts().

        Args:
            artefact_texts (str or list, optional): specify a class label or list of class labels to interpolate.
                These labels will count as artefact. If None, a default list of corrupt labels will be used.
                Defaults to None.
            max_duration (float): maximal duration (seconds) of the artefact label to replace them.
            inplace (bool): whether to do this inplace or not.

        Returns:
            sleep_stages_result (SleepStagesResult): new object with sleep stages (if `inplace` is False).
        """
        if inplace:
            sleep_stages_result = self
        else:
            sleep_stages_result = copy.deepcopy(self)

        # Interpolate annotations (inplace, since we already made a copy if needed).
        sleep_stages_result.annotation_set.interpolate_artefacts(artefact_texts=artefact_texts,
                                                                 max_duration=max_duration,
                                                                 inplace=True)

        if not inplace:
            return sleep_stages_result

    def interpolate_classes(self, query_times, kind='previous'):
        """
        Interpolate class numbers to the time points specified by query_times.

        Args:
            query_times (np.ndarray): query time points to interpolate the annotations to.
            kind (str, optional): how to interpolate, see interpolate.interp1d().
                Defaults to 'previous'. This takes the value of the last known sample, which is typically the correct
                way to interpolate sleep stage annotations.

        Returns:
            interpolated_classes (np.ndarray): array with interpolated class numbers at query points.
        """
        # Get the classes (class numbers).
        classes = self.get_classes()

        # Create a mask and corresponding time points that will be interpolated.
        # Append the sleep stage of the last segment at the end such that the interpolation function will not say that
        # a query point in the last segment is out of the interpolation range.
        classes = np.append(classes, classes[-1])
        time = [a.onset for a in self.annotation_set]
        time.append(self.annotation_set.end_time())

        # Interpolate to find the sleep stage at the query time points.
        finterp = interpolate.interp1d(time, classes, kind=kind, assume_sorted=True,
                                       fill_value=np.nan, bounds_error=False)

        # Compute sleep stages at query time points.
        interpolated_classes = finterp(query_times)

        return interpolated_classes

    def mean_std_duration_per_class(self, default_value=np.nan):
        """
        Compute mean and standard deviation of the duration of each sleep stage class.

        Args:
            default_value (float, optional): default value if no annotations of a class are present.
                Set this e.g. to np.nan or zero.
                Defaults to np.nan.

        Returns:
            out_dict_means (dict): defaultdict with of mean duration per class.
            out_dict_stds (dict): defaultdict with std of the durations per class.
        """
        # Convert to DataFrame.
        df = self.to_dataframe()

        out_dict_means = defaultdict(lambda: default_value)
        out_dict_stds = defaultdict(lambda: default_value)

        # Per class, compute mean and std duration.
        grouped_df = df[['text', 'duration']].groupby('text')
        out_dict_means.update(grouped_df.mean()['duration'].to_dict())
        out_dict_stds.update(grouped_df.std()['duration'].to_dict())
        return out_dict_means, out_dict_stds

    def median_duration(self, class_label):
        """
        Return the median duration of the `class_label` annotations.

        Args:
            class_label (str): class label.

        Returns:
            median_duration (float): median duration of `class_label`.
        """
        # Check class_label.
        self._check_class_label(class_label)

        # To dataframe.
        df = self.to_dataframe()

        # Compute durations per class/unique label.
        durations = df['duration'][df['text'] == class_label]

        # Median.
        median_duration = durations.median()

        return median_duration

    def median_iqr_duration_per_class(self, default_value=np.nan):
        """
        Compute median and inter quartile range of the duration of each sleep stage class.

        Args:
            default_value (float, optional): default value if no annotations of a class are present.
                Set this e.g. to np.nan or zero.
                Defaults to np.nan.

        Returns:
            out_dict_medians (dict): defaultdict with of median duration per class and default value np.nan.
            out_dict_iqrs (dict): defaultdict with IQR of the durations per class and default value np.nan.
        """
        # Convert to DataFrame.
        df = self.to_dataframe()

        out_dict_medians = defaultdict(lambda: default_value)
        out_dict_iqrs = defaultdict(lambda: default_value)

        # Per class, compute mean and std duration.
        grouped_df = df[['text', 'duration']].groupby('text')
        out_dict_medians.update(grouped_df.median()['duration'].to_dict())
        iqr = grouped_df.quantile(0.75)['duration'] - grouped_df.quantile(0.25)['duration']
        count = grouped_df.count()['duration']
        iqr[count <= 1] = np.nan  # np.nan if only 1 value.
        out_dict_iqrs.update(iqr.to_dict())
        return out_dict_medians, out_dict_iqrs

    def pieplot(self, **kwargs):
        # SPecify default kwargs, but let user-kwargs overrule them.
        pie_kwargs = dict(dict(
            startangle=90,
            add_legend=True,
            textprops=dict(ha='center', va='center'),
            labeldistance=1.15,
        ), **kwargs)

        pieplot(x='text', weight='duration', data=self.to_dataframe(), **pie_kwargs)

    def plot(self, *args, offset=0, time_scale='minutes', fill=False, ax=None, **kwargs):
        """
        Quick plot of the sleep stages as function of time.

        Args:
            *args (optional): optional arguments for the plt.plot() function.
            offset (float): offset to the y axis. Useful for plotting multiple hypnograms and plotting them slightly
                above each other.
            ax (plt.Axes, optional): axes to plot is. If None, plots in a new figure.
                Defaults to None.
            time_scale (str, optional): the time scale to use. Choose from 'seconds', 'minutes', 'hours'.
                Defaults to 'seconds'.
            fill (bool): whether to fill the hypnogram or not.
            **kwargs (optional): optional keyword arguments for the plt.plot() function.
        """
        if ax is not None:
            # Set current axes.
            plt.sca(ax)
        else:
            # Create new figure.
            plt.figure()

        # Get the sleep stage per annotation.
        sleep_stage = self.get_classes()

        # Extract onset times and convert to seconds.
        onsets = self.annotation_set.onsets()
        durations = self.annotation_set.durations()

        # Add another point at the end of each annotation (to get the rectangular shaped plot).
        sleep_stage = np.tile(sleep_stage.reshape(-1, 1), (1, 2)).flatten()
        time = np.concatenate((onsets.reshape(-1, 1), (onsets + durations).reshape(-1, 1)), axis=1).flatten()

        # Convert time scale to requested scale.
        time = convert_time_scale(time, time_scale)

        # Plot.
        plt.plot(time, sleep_stage + offset, *args, **kwargs)
        if fill:
            plt.fill_between(x=time, y1=sleep_stage + offset, y2=offset, **kwargs)
        plt.xlabel('Time ({})'.format(time_scale))
        y_limits = plt.ylim()
        plt.ylim([y_limits[0] - 0.1, y_limits[1] + 0.1])
        plt.yticks(list(self.class_mapping.values()), list(self.class_mapping.keys()))
        plt.title('Hypnogram')

    def plot_hypnogram(self, time_scale=None, order=None, missing_color='C7',
                       add_legend=None, ax=None, **kwargs):
        """
        Plot the hypnogram (sleep stages as function of time).

        Args:
            time_scale (str, optional): the time scale to use. Choose from 'seconds', 'minutes', 'hours', None.
                If None or 'timedelta', plots with a datetime.timedelta format (hh:mm:ss).
            order (list): order from top-to-bottom that the sleep stages will appear.
            missing_color (str, dict): missing labels will be shaded using this color scheme.
            add_legend (bool): whether to add a legend for the shaded missing labels.
            ax (plt.Axes): matplotlib axis to plot is.
            **kwargs (optional): optional keyword arguments for the fillplot() function.
        """
        if ax is not None:
            # Set current axes.
            plt.sca(ax)
        else:
            # Create new figure.
            plt.figure()
            ax = plt.gca()

        if add_legend is None:
            # add legend if missing_color is a dict.
            add_legend = isinstance(missing_color, dict)

        df = self.to_dataframe()

        # Convert time scale if requested.
        if time_scale is not None:
            if time_scale == 'timedelta':
                time_scale = None
            else:
                df.onset = convert_time_scale(df.onset, time_scale=time_scale)
                df.duration = convert_time_scale(df.duration, time_scale=time_scale)

        if order is not None:
            # Reverse, so it appears top-to-bottom.
            order = order[::-1]
        else:
            # Default order.
            order = np.unique(df.text)

        # Add end time to make plot complete.
        end_time = df.onset.iat[-1] + df.duration.iat[-1]
        df_ = pd.concat([df, pd.DataFrame(dict(
            onset=[end_time], text=['dummy'], duration=[0]), index=[0])],
                         ignore_index=True)

        # Plot.
        plot_kwargs = dict(dict(color='#E1EED8', edgecolor='C7',
                                alpha=1.0), **kwargs)
        fillplot(x='onset', y='text', data=df_,
                 y_order=order, ax=ax, step='post', **plot_kwargs)

        an_set = AnnotationSet().from_df(df). \
            merge_successive_texts().filter_fun(
            fun=lambda lab: lab not in order)

        shade_axis(onsets=an_set.onsets(), durations=an_set.durations(),
                   labels=an_set.texts(), color=missing_color, alpha=1,
                   add_legend=add_legend,
                   legend_kwargs=dict(bbox_to_anchor=(1, 1)), ax=ax)

        ax.set_ylim([0, 1.05])

        ax.set_title('Hypnogram')
        ax.set_ylabel('')

        if time_scale is None:
            def timeTicks(x, pos):
                d = datetime.timedelta(seconds=x)
                return str(d)
            formatter = FuncFormatter(timeTicks)
            ax.xaxis.set_major_formatter(formatter)
            ax.set_xlabel('Time (h:mm:ss)')
        else:
            ax.set_xlabel('Time ({})'.format(time_scale))

    def proportion(self, class_label, ignore_labels=None, default_value=0):
        """
        Return the proportion of segments classified as class_label.

        Args:
            class_label (str): the label of the class for which to compute the proportion.
            ignore_labels (str or list, optional): specify a class label or list of class labels to ignore. These labels
                won't be considered in the denominator. If None, ignore no_label only. If empty list, ignore nothing.
                Defaults to None.
            default_value (float, optional): default value if no annotations of the class is present.
                Set this e.g. to np.nan or zero.
                Defaults to 0.

        Returns:
            (float): proportion of segments classified as class_label.
        """
        # Check class_label.
        self._check_class_label(class_label)

        if ignore_labels is None:
            ignore_labels = [SLEEP_LABELS['no_label']]
        elif isinstance(ignore_labels, str):
            # Covert to list.
            ignore_labels = [ignore_labels]

        # Compute duration per class/unique label.
        durations = self.total_duration_per_class()

        # Numerator.
        num = durations.get(class_label, default_value)

        # Denominator.
        for lab in ignore_labels:
            durations[lab] = 0

        den = np.sum(list(durations.values()))

        return num/den

    def remove_corrupted_stages(self, corrupt_labels=None, inplace=False):
        """
        Remove the sleep stages that are considered corrupted or incomplete.

        A sleep stage is considered complete if it has a clear onset and transition.
        Annotations with one of `corrupt_labels` are not counted as transitions and if present during a sleep stage,
        the sleep stage is considered corrupted (anything may have happened, e.g. get out of the sleep stage).

        Args:
            inplace (bool, optional): if True, remove the annotations in place. If False, a new SleepStagesResult object
                with the new set of annotations is returned.
                Defaults to False.
            corrupt_labels (str or list, optional): specify a class label or list of class labels that do not count as
                valid sleep stages. These labels won't be considered as valid transitions (e.g. artefacts, no_label).
                no_label segments will automatically be considered corrupt. If None, a default list of corrupt labels
                will be used.
                Defaults to None.
        Returns:
            sleep_stages_result (SleepStagesResult): new object with only complete sleep stages (and no_label segments
                to fill the gaps).
        """
        if inplace:
            sleep_stages_result = self
        else:
            # Create a copy of the current object (to preserve the original one).
            sleep_stages_result = copy.deepcopy(self)

        if corrupt_labels is None:
            corrupt_labels = [
                SLEEP_LABELS['artefact'],
            ]
        elif isinstance(corrupt_labels, str):
            # Convert to list.
            corrupt_labels = [corrupt_labels]

        # Always add no_label to corrupt_labels list.
        if not SLEEP_LABELS['no_label'] in corrupt_labels:
            corrupt_labels.append(SLEEP_LABELS['no_label'])

        annotations = sleep_stages_result.annotation_set.annotations

        # The first annotation is incomplete (no clear onset).
        remove_idx = [0]

        # Any annotation that has either a corrupt label preceding or following is incomplete (any other label
        # is considered a valid transition from/to another sleep stage).
        for idx in range(1, len(annotations) - 1):
            annot = annotations[idx]
            if annot.text != SLEEP_LABELS['no_label']:
                if (annotations[idx-1].text in corrupt_labels or
                        annot.text in corrupt_labels or
                        annotations[idx+1].text in corrupt_labels):
                    remove_idx.append(idx)

        # The last annotation is incomplete (no clear end/transition).
        remove_idx.append(len(annotations) - 1)

        # Replace the to be removed annotations with no_label annotation text.
        for idx in remove_idx:
            annotations[idx].text = SLEEP_LABELS['no_label']

        # Merge successive equal labels.
        sleep_stages_result.annotation_set.merge_successive_texts(inplace=True)

        if not inplace:
            return sleep_stages_result

    def remove_short_stages(self, min_duration=180, inplace=False):
        """
        Remove sleep stages that last too short by replacing the labels by no_label (NL).

        Keep only annotations that last > min_duration.

        Args:
            min_duration (float, optional): minimum duration in seconds that a sleep stage should last to keep it.
                Defaults to 180.
            inplace (bool, optional): if True, remove the annotations in place. If False, a new SleepStagesResult object
                with the new set of annotations is returned.
                Defaults to False.

        Returns:
            sleep_stages_result (SleepStagesResult): new object where all sleep stages last for at least min_duration
                seconds.
        """
        if inplace:
            sleep_stages_result = self
        else:
            # Create a copy of the current object (to preserve the original one).
            sleep_stages_result = copy.deepcopy(self)

        durations = sleep_stages_result.annotation_set.durations()
        remove_idx = np.where(durations < min_duration)[0]
        annotations = sleep_stages_result.annotation_set.annotations
        for idx in remove_idx:
            annotations[idx].text = SLEEP_LABELS['no_label']

        # Merge successive equal labels.
        sleep_stages_result.annotation_set.merge_successive_texts(inplace=True)

        if not inplace:
            return sleep_stages_result

    def segment_labels(self, segment_start_times, segment_end_times, return_labels=False):
        """
        Return an array of labels corresponding to segments with specified start and end times.

        Segment i lasts from segment_start_times[i] until segment_end_times[i].
        If segment i does not fall entirely in one annotation, the value of y_true[i] will be np.nan.

        Args:
            segment_start_times (list, np.ndarray): array with start times of the segments (must be sorted).
            segment_end_times (list, np.ndarray): array with end times of the segments (must be sorted).
            return_labels (bool, optional): if False, return the class numbers. If True, return class labels.

        Returns:
            y_true (np.ndarray): 1D array with class numbers (or labels) for each segment.

        Examples:
            >>> texts = ['QS', 'NQS', 'QS', 'NQS', 'QS']
            >>> onsets = [10, 40, 120, 125, 140]
            >>> durations = list(np.diff(onsets)) + [30]
            >>> annotations = [Annotation(text=t, onset=o, duration=d) for (t, o, d) in zip(texts, onsets, durations)]
            >>> anset = AnnotationSet(annotations=annotations)
            >>> ss = SleepStagesResult(annotation_set=anset, algorithm_parameters=dict())
            >>> print(ss.to_dataframe())
               onset  duration text
            0   10.0      30.0   QS
            1   40.0      80.0  NQS
            2  120.0       5.0   QS
            3  125.0      15.0  NQS
            4  140.0      30.0   QS
            >>> segment_start_times = [0, 40, 120, 130, 150, 160]
            >>> segment_end_times = [20, 80, 130, 140, 160, 180]
            >>> labels = ss.segment_labels(segment_start_times, segment_end_times, return_labels=True)
            >>> print(labels)
            ['NL' 'NQS' 'NL' 'NQS' 'QS' 'NL']
        """
        segment_start_times = np.asarray(segment_start_times)
        segment_end_times = np.asarray(segment_end_times)
        class_mapping = self.class_mapping

        # Initialize y_true with nans.
        y_true = np.full(len(segment_start_times), np.nan)

        # Loop over annotations and fill the y_true array with the corresponding class number.
        for annot in self:
            # Get the class number corresponding to annotation.
            class_number = class_mapping[annot.text]

            # Select segments that fall entirely inside the annotation.
            if annot.onset > segment_start_times[-1]:
                # If annotation falls outside segments, skip it.
                continue
            else:
                start_idx = np.where(segment_start_times >= annot.onset)[0][0]

            if annot.onset + annot.duration >= segment_end_times[-1]:
                stop_idx = len(y_true)
            else:
                stop_idx = np.where(segment_end_times > annot.onset + annot.duration)[0][0]

            # Set the class number for segments that coincide entirely in the annotation.
            y_true[start_idx: stop_idx] = class_number

        if return_labels:
            y_true = np.asarray(self.class_numbers_to_labels(y_true))

        return y_true

    def to_dataframe(self):
        """
        Return a dataframe of self.annotation_set.

        Returns:
            (pd.DataFrame): dataframe of self.annotation_set.
        """
        return self.annotation_set.to_dataframe()

    def total_duration_per_class(self, default_value=0):
        """
        Compute the total duration of each sleep stage class.

        Args:
            default_value (float, optional): default value if no annotations of a class are present.
                Set this e.g. to np.nan or zero.
                Defaults to 0.

        Returns:
            (dict): defaultdict of total durations per class and default value `fill_value`.
        """
        df = self.to_dataframe()
        out_dict = defaultdict(lambda: default_value)
        out_dict.update(df[['text', 'duration']].groupby(['text']).sum()['duration'].to_dict())
        return out_dict

    def _check_class_label(self, class_label):
        """
        Check if class_label is in self.class_mapping.

        Args:
            class_label (str): class label to check.

        Raises:
            ValueError: if class_label is invalid.
        """
        if class_label not in self.class_mapping:
            raise ValueError('Invalid input argument "{}" for class_label. Choose from: {}.'
                             .format(class_label, '"' + '", "'.join(self.class_mapping.keys()) + '"'))

    @staticmethod
    def _read_from_csv(filepath):
        """
        Read result from csv file into a SleepStagesResult class.

        Args:
            filepath (str): see ResultBase._read_from_csv().

        Returns:
            result (nnsa.SleepStagesResult): instance of SleepStagesResult containing the sleep stages annotations.
        """
        # Lines 1-4: Standard csv header (use the ResultBase method).
        algorithm_parameters, data_info = ResultBase._read_csv_header(filepath)[1:3]

        # Re-open the file and read the rest of the file, line by line.
        with open(filepath, 'r') as f:
            reader = csv.reader(f)

            # Lines 1-4: Standard csv header (already read, skip).
            [next(reader) for i in range(4)]

            # Line 5: Non-array data header (skip).
            next(reader)

            # Line 6: Non-array data.
            annotation_set_label = next(reader)[0]

            # Line 7: Class mapping keys (str).
            class_mapping_keys = next(reader)

            # Line 8: Class mapping values (float).
            class_mapping_values = [convert_string(i, 'float') for i in next(reader)]

            # Line 9: Annotation header (skip).
            next(reader)

            # Line 10-... : Annotation data. Each Annotation is saved on one row.
            annotation_set = AnnotationSet(label=annotation_set_label)
            for row in reader:
                onset, duration, text = row
                annot = Annotation(onset=float(onset), duration=float(duration), text=text)
                annotation_set.append(annot, inplace=True)

        # Create class mapping dict.
        class_mapping = dict(zip(class_mapping_keys, class_mapping_values))

        # Create a result object.
        result = SleepStagesResult(annotation_set=annotation_set, algorithm_parameters=algorithm_parameters,
                                   class_mapping=class_mapping, data_info=data_info)

        return result

    @staticmethod
    def _read_from_hdf5(filepath):
        """
        Read result from hdf5 file into a SleepStagesResult class.

        Args:
            filepath (str): see ResultBase._read_from_hdf5().

        Returns:
            result (nnsa.SleepStagesResult): instance of SleepStagesResult containing the sleep stages annotations.
        """
        # Read standard csv header (use the ResultBase method).
        algorithm_parameters, data_info = ResultBase._read_hdf5_header(filepath)[1:3]

        # Re-open the file and read the rest of the file.
        with h5py.File(filepath, 'r') as f:
            # Read Annotation data.
            onsets = f['onsets'][:]
            durations = f['durations'][:]
            texts = [text.decode() for text in f['texts'][:]]

            # Read non-array data: annotation_set label.
            annotation_set_label = f['texts'].attrs['annotation_set_label'].decode()

            # Read non-array data: class_mapping.
            class_mapping = read_dict_from_hdf5(f, 'class_mapping')

        # Create AnnotationSet.
        annotation_set = AnnotationSet(label=annotation_set_label)
        for ons, dur, tex in zip(onsets, durations, texts):
            annot = Annotation(onset=ons, duration=dur, text=tex)
            annotation_set.append(annot, inplace=True)

        # Create a result object.
        result = SleepStagesResult(annotation_set=annotation_set, algorithm_parameters=algorithm_parameters,
                                   class_mapping=class_mapping, data_info=data_info)

        return result

    def _write_to_csv(self, filepath):
        """
        Write the contents of the object to a csv file.

        Args:
            filepath (str): see ResultBase._write_to_csv().
        """
        # Lines 1-4: Standard csv header (use the ResultBase method).
        self._write_csv_header(filepath)

        # Append attributes to the csv file, line by line.
        with open(filepath, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Line 5: Non-array data header.
            writer.writerow(['annotation_set_label'])

            # Line 6: Non-array data.
            writer.writerow([self.annotation_set.label])

            # Line 7: Class mapping keys (str).
            writer.writerow(self.class_mapping.keys())

            # Line 8: Class mapping values (float).
            writer.writerow(self.class_mapping.values())

            # Line 9: Annotation header.
            writer.writerow(['annotation_onset', 'annotation_duration', 'annotation_text'])

            # Line 10-... : Annotation data. Each Annotation is saved on one row.
            for a in self.annotation_set.annotations:
                writer.writerow([a.onset, a.duration, a.text])

    def _write_to_hdf5(self, filepath):
        """
        Write the contents of the object to an hdf5 file.

        Args:
            filepath (str): see ResultBase._write_to_hdf5().
        """
        # Write standard hdf5 header (use the ResultBase method).
        self._write_hdf5_header(filepath)

        # Append attributes to the hdf5 file.
        with h5py.File(filepath, 'a') as f:
            # Write Annotation data.
            f.create_dataset('onsets', data=self.annotation_set.onsets())
            f.create_dataset('durations', data=self.annotation_set.durations())
            texts = f.create_dataset('texts', data=np.array([np.string_(a.text) for a in self.annotation_set]))

            # Write non-array data: annotation_set label.
            texts.attrs['annotation_set_label'] = np.string_(self.annotation_set.label)

            # Write non-array data: class_mapping.
            write_dict_to_hdf5(f, self.class_mapping, target='class_mapping')


class SleepStagesCnn(ClassWithParameters):
    """
    Sleep stage classification using a Convolutional Neural Network.

    References:
    A. H. Ansari et al., “A convolutional neural network outperforming
    state-of-the-art sleep staging algorithms for both preterm and term infants”
    Journal of Neural Engineering, vol. 17, no. 1, p. 16028, Jan. 2020,
    doi: 10.1088/1741-2552/ab5469.

    main method: sleep_stages_cnn()

    Note:
        Tested with keras version 2.2.4 and tensorflow version 1.13.1 and with python version 3.6.

    Args:
        num_classes (int): number of classes in sleep stage classification (2-class (2), or 4-class (4)).

        **kwargs (optional): optional keyword arguments for nnsa.ClassWithParameters.

    Examples:
        >>> np.random.seed(0)
        >>> x = np.random.rand(20, 900, 8)*400 - 200
        >>> cnn = SleepStagesCnn()
        >>> assert_equal(cnn.parameters, SleepStagesCnn.default_parameters())
        >>> print(type(cnn.parameters).__name__)
        Parameters

        >>> cnn_result = cnn.sleep_stages_cnn(x, verbose=0)
        >>> print(type(cnn_result).__name__)
        SleepStagesCnnResult

        >>> print(cnn_result.probabilities[:, 0])
        [0.04391909 0.95608091]

    """
    def __init__(self, num_classes=2, detect_novelties=True, **kwargs):
        # Call parent's __init__.
        super().__init__(num_classes=num_classes, detect_novelties=detect_novelties, **kwargs)

        self._keras_model = None  # (keras.model)
        self._keras_model_activations = None  # (keras.model)
        self._keras_model_latent = None  # (keras.model)
        self._normalization_parameters = None  # (tuple)

    @property
    def keras_model(self):
        """
        Return the model.

        Returns:
            (nnsa.model or keras.model): model object.
        """
        if self._keras_model is None:
            self._keras_model = self._load_keras_model()
        return self._keras_model

    @property
    def keras_model_activations(self):
        """
        Return the model for intermediate activations.

        Returns:
            (nnsa.model or keras.model): model object.
        """
        if self._keras_model_activations is None:
            self._keras_model_activations = self._get_keras_model_activations()
        return self._keras_model_activations

    @property
    def keras_model_latent(self):
        """
        Return the model for latent features.

        Returns:
            (nnsa.model or keras.model): model object.
        """
        if self._keras_model_latent is None:
            self._keras_model_latent = self._get_keras_model_latent()
        return self._keras_model_latent

    @staticmethod
    def default_parameters():
        """
        Return the default parameters as a dictionary.

        Returns:
            (nnsa.Parameters): a default set of parameters for the model.
        """
        # Keyword arguments to pass to keras' model.predict method.
        predict_kwargs = {
            # Batch size (int). Note that with the default model it requires 0.864 MB of RAM per sample (when the
            # biggest two layers are completely filled with np.float64 values). This is not much, so we can afford a
            # relatively large batch size which may speed up computation time:
            'batch_size': 320,
        }

        # Parameters for artefact detection/exclusion, see
        # nnsa.artefacts.artefact_detection.default_eeg_signal_quality_criteria()
        artefact_criteria = {
            'max_nan_frac': 1e-12,  # Do not accept nans in a channel segment.
            'max_fraction_of_artefact_channels': 1/8,  # Accept 1 artefacted channel.
        }

        # Default parameters.
        pars = {
            # Number of classes in sleep stage classification (2-class (2), or 4-class (4)).
            'num_classes': 2,
            # Whether to detect novelties in data inputs when predicting.
            'detect_novelties': True,
            # Which features to use for novelty detection. Select 'time_freq', 'spectral_pirya'.
            'which_novelty_features': 'spectral_pirya',
            # Kernel size for the smoothing filter in postprocessing (int):
            'smoothing_kernel_size': 6,  # 6 corresponds to 3 minutes.
            # Keyword arguments to pass to keras' model.predict method:
            'predict_kwargs': predict_kwargs,
            # Monte Carlo settings: the number of times to repeat the prediction after enabling monte carlo dropout.
            # If None or False, does not apply Monte Carlo dropout (and the entire network is used,
            # i.e., no neurons are dropped).
            'monte_carlo': False,
            # Compute latent features.
            'latent_features': False,
            # Artefact criteria.
            'artefact_criteria': artefact_criteria,
        }

        return Parameters(**pars)

    @property
    def model_dir(self):
        """
        Return the directory containing model files.

        Returns:
            model_dir (str): path to directory with model files.
        """
        num_classes = self.parameters['num_classes']
        model_dir = os.path.join(MODEL_DATA_DIR, 'sleep_stage_classifier', f'{num_classes}_class')
        return model_dir

    @property
    def model_path(self):
        """
        Return the filpath of the model.

        Returns:
            model_path (str): filepath of the model.
        """
        num_classes = self.parameters['num_classes']

        if num_classes == 2:
            model_path = os.path.join(self.model_dir, 'model_v5_paramC_CH8.h5')
        elif num_classes == 4:
            model_path = os.path.join(self.model_dir, 'R_model_v5_paramC_CH8_148_1.h5')
        else:
            raise NotImplementedError('No CNN model implemented for num_classess={}.'.format(num_classes))
        return model_path

    @property
    def norm_param_path(self):
        """
        Return the filpath with the normalization parameters.

        Returns:
            norm_param_path (str): filepath of the pickle file with the normalization parameters. The file contains
                2 arrays. The first array contains the means per channel and the second the stds per channel for
                normalization (see self._load_normalization_parameters).
        """
        num_classes = self.parameters['num_classes']

        if num_classes == 2:
            norm_param_path = os.path.join(self.model_dir, 'normParam_paramC_CH8.pkl')
        elif num_classes == 4:
            norm_param_path = os.path.join(self.model_dir, 'R_model_v5_paramC_CH8_148_1_normParam.pkl')
        else:
            raise NotImplementedError('No CNN model implemented for num_classess={}.'.format(num_classes))
        return norm_param_path

    @property
    def data_requirements(self):
        """
        Return a dictionary with requirements for the input of the CNN.

        Returns:
            data_requirements (dict): a dictionary with requirements for the input of the CNN
                (e.g., channel order, fs, segment length).
        """
        # Extract parameters (data requirements depend on input parameters).
        num_classes = self.parameters['num_classes']

        data_requirements = dict()
        if num_classes in [2, 4]:
            channel_order = ['Fp1', 'Fp2', 'C3', 'C4', 'T3', 'T4', 'O1', 'O2']
            fs = 30
            segment_length = 30
            reference_channel = 'Cz'

        else:
            raise NotImplementedError('Not implemented for num_classes={}.'
                                      .format(num_classes))

        data_requirements = dict(
            # Order of the channels in the input tensor.
            channel_order=channel_order,

            # The sample frequency (Hz).
            fs=fs,

            # The segment length (seconds).
            segment_length=segment_length,

            # Reference for EEG data.
            reference_channel=reference_channel,
        )

        return data_requirements

    @property
    def normalization_parameters(self):
        """
        Return the normalization parameters.

        Returns:
            (tuple): normalization parameters.
        """
        if self._normalization_parameters is None:
            self._normalization_parameters = self._load_normalization_parameters()
        return self._normalization_parameters

    def preprocess_recording(self, raw_eeg, raw_fs, verbose=1):
        """
        Run the preprocessing routine (filtering, resampling) on an entire recording.

        Args:
            raw_eeg (np.ndarray): raw EEG data to wct with shape (time, channels).
            raw_fs (float): sample frequency of raw EEG data.
            verbose: (int): verbosity level.

        Returns:
            eeg (np.ndarray): preprocessed EEG data.
            fs (float): sample frequency of preprocessed EEG data.
        """
        channel_order = self.data_requirements['channel_order']
        num_classes = self.parameters['num_classes']

        # Check input dimensions.
        if raw_eeg.shape[0] < raw_eeg.shape[1] or raw_eeg.ndim != 2:
            raise ValueError('Shape of `raw_eeg` should correspond to (time, channels). Got shape {}.'
                             .format(raw_eeg.shape))

        # Make sure that the number of channels matches the length of channel order.
        if raw_eeg.shape[1] != len(channel_order):
            raise ValueError(f'Expected EEG with {len(channel_order)} channels, but got {raw_eeg.shape[1]} channels. '
                             f'Expected channel order is: {channel_order}.')

        if num_classes == 2:
            eeg, fs = self._preprocess_recording_2_class(raw_eeg, raw_fs, verbose=verbose)
        elif num_classes == 4:
            eeg, fs = self._preprocess_recording_4_class(raw_eeg, raw_fs, verbose=verbose)
        else:
            raise NotImplementedError('Not implemented for num_classes={}.'.format(num_classes))

        assert self.data_requirements['fs'] == fs

        return eeg, fs

    def process(self, eeg, fs, verbose=1, **kwargs):
        """
        Process raw EEG data (channel order should match self.data_requirements['channel_order']).

        Args:
            eeg (np.ndarray): 2D array with shape (time, channels) with raw (unfiltered) EEG data.
                The data should be referenced as required and in the channels should have the
                order as required in self.data_requirements.
            fs (float): sampling frequency of `eeg` (Hz).
            verbose (int): verbosity level.
        """
        segment_length = self.data_requirements['segment_length']

        # Check input shape (n_time, n_channels), transposes if needed.
        eeg = check_eeg_is_long(eeg=eeg, mode='transpose')

        # Preprocess raw EEG recording.
        eeg_pp, fs_pp = self.preprocess_recording(raw_eeg=eeg, raw_fs=fs, verbose=verbose)

        # Segment the EEG signals (n_seg, n_time, n_channels).
        if verbose:
            print('Segmenting...')
        x = get_all_segments(eeg_pp, fs=fs_pp, segment_length=segment_length, overlap=0, axis=0)

        # Run sleep_stages_cnn().
        return self.sleep_stages_cnn(x=x, verbose=verbose, **kwargs)

    def sleep_stages_cnn(self, x, segment_start_times=None, segment_end_times=None, verbose=1):
        """
        Apply the sleep stage classification algortihm to the preprocessed (filtered/resampled/segmented) input x:
        1) normalize input x,
        2) predict the CNN output,
        3) postprocess the output.

        Args:
            x (np.ndarray): (unnormalized) input array with shape corresponding to (segments, time, channels)
                with preprocessed (filtered and resampled) EEG data. Note
                that the time and channels dimension depend on the shape of the input layer in the CNN model. E.g. for
                the default model, shape of x must be (num_segments, 900, 8), corresponding to 30 second segments
                sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in the following order:
                Fp1, Fp2, C3, C4, T3, T4, O1, O2.
                The array may also be 4 dimensional with shape (segments, time, channels, 1), which is the shape for the
                CNN model. If x does not have this fourth dimension, it will be added by this preprocessing.
            verbose (int, optional): verbose level (0 or 1).
                Default to 1.
        Returns:
            (SleepStagesCnnResult): nnsa object containing the results of the CNN sleep stage classification.
        """
        if verbose > 1:
            print(HORIZONTAL_RULE)
            print('Predicting sleep stages using SleepStagesCnn with parameters:')
            print(self.parameters)

        # Preprocess (normalize) the input, replace nans by zeros.
        x_preprocessed = self._preprocess(x, verbose=verbose > 1)

        # Predict novelties if requested.
        if self.parameters['detect_novelties'] is True:
            if verbose > 1:
                print('Detecting novelties...')
            features = self._compute_novelty_features(x_preprocessed[:, :, :, 0], verbose=verbose > 1)
            is_novelty, novelty_score = self._detect_novelties(features=features, verbose=verbose > 1)
        else:
            is_novelty = None
            novelty_score = None

        # Compute the CNN output.
        y, X_latent = self._predict_cnn(x_preprocessed, verbose=verbose)

        # Postprocess the CNN output.
        y_postprocessed = self._postprocess(y, verbose=verbose > 1)

        if self.parameters['artefact_criteria'] is not None:
            # Remove output at artefacts labels (replace with nan).
            af_mask = detect_artefact_signals(
                x, axis=1, channel_axis=-1, demean=False,
                keepdims=False, **self.parameters['artefact_criteria'])
            while af_mask.ndim > 1:
                af_mask = np.all(af_mask, axis=-1)
            y_postprocessed[af_mask] = np.nan

        if verbose > 0:
            print('Finished predicting sleep stages with CNN.')

        # Return as SleepStageCnn object.
        return SleepStagesCnnResult(probabilities=y_postprocessed.T,  # (classes, time).
                                    is_novelty=is_novelty,
                                    novelty_score=novelty_score,
                                    class_labels=self._get_class_labels(),
                                    latent_features=X_latent.T if X_latent is not None else None,  # (features, time).
                                    fs=1/self.data_requirements['segment_length'],
                                    segment_start_times=segment_start_times,
                                    segment_end_times=segment_end_times,
                                    algorithm_parameters=self.parameters)

    def predict_activations(self, x, verbose=1):
        if verbose > 1:
            print(HORIZONTAL_RULE)
            print('Predicting activations of SleepStagesCnn with parameters:')
            print(self.parameters)

        # Preprocess (normalize) the input, replace nans by zeros.
        x_preprocessed = self._preprocess(x, verbose=verbose > 1)

        # Compute the CNN output.
        y = self._predict_activations(x_preprocessed, verbose=verbose)

        return y

    def _compute_novelty_features(self, x, verbose=1):
        """
        Compute features for novelty detection.

        Args:
            x (np.ndarray): array with preprocessed data with shape (n_seg, n_time, n_channels).
            verbose (int): verbosity level.

        Returns:
            features (pd.DataFrame): dataframe with features for each segment.
        """
        if x.ndim != 3:
            raise ValueError('Expected `x`to be 3D, but got shape {}.'.format(x.shape))

        # Extract some info.
        segment_length = self.data_requirements['segment_length']
        fs = self.data_requirements['fs']
        num_classes = self.parameters['num_classes']
        which_features = self.parameters['which_novelty_features']
        stepsize = segment_length

        if num_classes in [4]:
            # Reshape back to (n_seg, n_channels) (for feature computation function).
            eeg = x.reshape(-1, x.shape[-1])

            # Compute features (n_feat, n_chan, n_seg).
            features, feature_labels = compute_features(
                eeg, fs, window=segment_length, which=which_features, stepsize=stepsize, verbose=verbose)

            # To shape (n_seg, n_chan, n_feat).
            features = np.swapaxes(features, axis1=0, axis2=2)

            # Aggregate features over channels to shape (n_segments, n_features).
            features = pd.DataFrame(np.nanmean(features, axis=1), columns=feature_labels)
        else:
            msg = '\nNo novelty detection trained for num_classes={}. ' \
                  'Disable novelty detection to suppress this warning.'.format(num_classes)
            warnings.warn(msg)
            features = None

        return features

    def _detect_novelties(self, features, verbose=1):
        """

        Args:
            features (np.ndarray): dataframe with features of each segment, has shape (n_seg, n_features).
            verbose (int): verbosity level.

        Returns:
            is_novelty (np.ndarray): array with same length as `features` (n_seg,) containing True
                at locations where there is an out-of-data segment.
        """
        num_classes = self.parameters['num_classes']
        which_features = self.parameters['which_novelty_features']

        if num_classes in [4]:
            # Load novelty detection model.
            model_name = f'IF_0.05_{which_features}'
            novelty_model_path = os.path.join(self.model_dir, f'novelty_detection_{model_name}.joblib')
            novelty_model = joblib.load(novelty_model_path)

            # Extract parts of model.
            feature_names, scaler, pca, clf = novelty_model

            # Extract the needed features in correct order (check).
            X = features[feature_names]

            # Log transform.
            if which_features == 'spectral_pirya':
                # Only first 7 features are non-negative (the last 2 are entropies, we shouldn't take the log of these).
                X.iloc[:, :7] = np.log(X.iloc[:, :7])
            else:
                X = np.log(X)

            # Replace nans and infs with zeros.
            X = np.nan_to_num(X, neginf=0, posinf=0)

            # Normalize.
            X = scaler.transform(X)

            # PCA.
            if pca is not None:
                X = pca.transform(X)

            # To dataframe.
            X = pd.DataFrame(X, columns=feature_names)

            # Predict (1 inlier, -1 outlier).
            is_novelty = clf.predict(X) == -1
            novelty_score = -clf.score_samples(X)

        else:
            msg = '\nNo novelty detection trained for num_classes="{}".'.format(num_classes)
            # raise NotImplementedError(msg)
            is_novelty = None
            novelty_score = None

        if verbose and is_novelty is not None:
            print(f'{np.nanmean(is_novelty) * 100:.1f}% novelties detected.')

        return is_novelty, novelty_score

    def _get_class_labels(self):
        """
        Return the class labels corresponding to the output array.

        Returns:
            class_labels (list of str): class labels corresponding to the output array.
        """
        # Infer model type.
        num_classes = self.parameters['num_classes']

        # Get segment length for a given model type.
        if num_classes == 2:
            # First column corresponds to non quiet sleep (NQS), seconds column to quiet sleep (QS).
            class_labels = [SLEEP_LABELS['non_quiet_sleep'], SLEEP_LABELS['quiet_sleep']]
        elif num_classes == 4:
            class_labels = [SLEEP_LABELS['active_sleep_2'], SLEEP_LABELS['active_sleep_1'],
                            SLEEP_LABELS['quiet_sleep_hvs'], SLEEP_LABELS['quiet_sleep_ta']]
        else:
            raise NotImplementedError('_get_class_labels() not implemented for num_classes="{}".'
                                      .format(num_classes))

        return class_labels

    def _get_keras_model_activations(self, verbose=0):
        """
        Get the keras model for predicting the intermediate activation feature maps.

        Args:
            verbose (int, optional): verbose level.
                Defaults to 0.

        Returns:
            activations_model (keras.model): keras model object.
        """
        from tensorflow import keras
        model = self.keras_model

        # All intermediate layers.
        layer_outputs = [layer.output for layer in model.layers]

        # Create model that outputs all intermediate activations.
        activations_model = keras.models.Model(
            inputs=model.input, outputs=layer_outputs)

        if verbose > 0:
            activations_model.summary()

        return activations_model

    def _get_keras_model_latent(self, verbose=0):
        """
        Get the keras model for predicting the latent features.

        Args:
            verbose (int, optional): verbose level.
                Defaults to 0.

        Returns:
            latent_model (keras.model): keras model object.
        """
        # Locally import keras due to long loading time.
        from tensorflow import keras

        # Get name of latent layer.
        num_classes = self.parameters['num_classes']
        if num_classes == 2:
            layer_name = 'dense_15'
        elif num_classes == 4:
            layer_name = 'dense1'
        else:
            raise NotImplementedError('Do not know which layer to take for num_classes={}.'.format(num_classes))

        # Create model that outputs the features.
        model = self.keras_model
        latent_model = keras.Model(inputs=model.input, outputs=model.get_layer(layer_name).output)

        if verbose > 0:
            latent_model.summary()

        return latent_model

    def _load_keras_model(self, verbose=0):
        """
        Load the keras model.

        Args:
            verbose (int, optional): verbose level.
                Defaults to 0.

        Returns:
            model (keras.model): keras model object.
        """
        model_path = self.model_path

        # Check if model path exists.
        if not os.path.exists(model_path):
            raise FileNotFoundError('Model path "{}" does not exist.'
                                    .format(model_path))

        if verbose > 0:
            print('Loading model {}...'.format(os.path.basename(model_path)))

        # Locally import keras due to long loading time.
        try:
            # keras can be installed as standalone package (old), or as part of tensorflow (new).
            import keras
        except (ModuleNotFoundError, ImportError):
            # https://stackoverflow.com/questions/66964492/importerror-cannot-import-name-get-config-from-tensorflow-python-eager-conte
            from tensorflow import keras

        # Load model.
        model = keras.models.load_model(model_path)

        # Enable Monte Carlo dropout if requested.
        if self.parameters['monte_carlo']:
            from nnsa.keras.utils import enable_monte_carlo_dropout
            model = enable_monte_carlo_dropout(model, rate_multiplier=1)

        if verbose > 0:
            model.summary()

        return model

    def _load_normalization_parameters(self):
        """
        Load the normalization parameters.

        Returns:
            means (np.ndarray): means per channel for normalization.
            stds (np.ndarray): stds per channel for normalization.
        """
        norm_param_path = self.norm_param_path

        # Check if pickle filepath exists.
        if not os.path.exists(norm_param_path):
            raise FileNotFoundError('Normalization parameter path "{}" does not exist.'
                                    .format(norm_param_path))

        # Read normalization parameters.
        with open(norm_param_path, 'rb') as f:
            means, stds = pickle.load(f)

        return means, stds

    def _postprocess(self, y, verbose=1):
        """
        Postprocess the data y.

        Args:
            y (np.ndarray): the (raw) output array with shape (n_segments, n_classes).

        Returns:
            y_post (np.ndarray): the postprocessed output array of size (n_segments, n_classes).
        """
        # Extract parameters for postprocessing.
        smoothing_kernel_size = self.parameters['smoothing_kernel_size']

        if smoothing_kernel_size:
            if verbose:
                print('Smoothing...')
            # Check size of y.
            if len(y) <= smoothing_kernel_size*2:
                raise ValueError('Must have more than {} segments for postprocessing. Got {} segments.'
                                 .format(smoothing_kernel_size*2, len(y)))

            # Create smoothing kernel (moving average).
            smoothing_kernel = np.ones(smoothing_kernel_size) / smoothing_kernel_size

            # Smoothen the output per class (sum of the probabilities per class will still be 1).
            y_post = scipy.signal.filtfilt(b=smoothing_kernel, a=1, x=y, axis=0)
        else:
            # No smoothing.
            y_post = y

        return y_post

    def _predict_activations(self, x, verbose=0):
        """
        Predict the model activations (intermediate layers) of input x.

        Args:
            x (np.ndarray): preprocessed (normalized) input array with shape corresponding to
                (segments, time, channels, 1). E.g. for the default model, shape of x must be (num_segments, 900, 8, 1),
                corresponding to 30 second segments sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in
                the following order: Fp1, Fp2, C3, C4, T3, T4, O1, O2.
            verbose (int, optional): verbose level (0 or 1).
                Defaults to 0.
        Returns:
            y (np.ndarray): the (raw) output array.
        """
        # Check input shape.
        input_shape = self.keras_model.input.shape.as_list()
        if list(x.shape)[1:] != input_shape[1:]:
            raise ValueError('Invalid input shape {}. Expected input shape {}.'.format(x.shape, input_shape))

        # Get options for the predict method.
        predict_kwargs = self.parameters['predict_kwargs']

        # Add verbose keyword argument for predict method.
        predict_kwargs.update({'verbose': verbose})

        if verbose:
            print('Predicting...')
        y = self.keras_model_activations.predict(x, **predict_kwargs)

        return y

    def _predict_cnn(self, x, verbose=0):
        """
        Predict the model outcome of input x.

        Args:
            x (np.ndarray): preprocessed (normalized) input array with shape corresponding to
                (segments, time, channels, 1). E.g. for the default model, shape of x must be (num_segments, 900, 8, 1),
                corresponding to 30 second segments sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in
                the following order: Fp1, Fp2, C3, C4, T3, T4, O1, O2.
            verbose (int, optional): verbose level (0 or 1).
                Defaults to 0.
        Returns:
            y (np.ndarray): the (raw) output array.
        """
        # Check input shape.
        input_shape = self.keras_model.input.shape.as_list()
        if list(x.shape)[1:] != input_shape[1:]:
            raise ValueError('Invalid input shape {}. Expected input shape {}.'.format(x.shape, input_shape))

        # Get options for the predict method.
        predict_kwargs = self.parameters['predict_kwargs']

        # Add verbose keyword argument for predict method.
        predict_kwargs.update({'verbose': verbose})

        if verbose:
            print('Predicting...')

        if self.parameters['latent_features']:
            X_latent = self.keras_model_latent.predict(x, **predict_kwargs)
        else:
            X_latent = None

        monte_carlo = self.parameters['monte_carlo']
        if not monte_carlo:
            # No Monte Carlo repetitions.
            y = self.keras_model.predict(x, **predict_kwargs)
        else:
            # Do Monte Carlo repetitions and take the average.
            y_all = [self.keras_model.predict(x, **predict_kwargs) for _ in range(monte_carlo)]
            y = np.nanmean(np.array(y_all), axis=0)

        return y, X_latent

    def _preprocess(self, x, verbose=1):
        """
        Preprocess the segmented data x.

        The preprocessing consists of normalization with fixed constants (from training).

        Args:
            x (np.ndarray): (unnormalized) input array with shape corresponding to (segments, time, channels). Note
                that the time and channels dimension depend on the shape of the input layer in the CNN model. E.g. for
                the default model, shape of x must be (num_segments, 900, 8), corresponding to 30 second segments
                sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in the following order:
                Fp1, Fp2, C3, C4, T3, T4, O1, O2.
                The array may also be 4 dimensional with shape (segments, time, channels, 1), which is the shape for the
                CNN model. If x does not have this fourth dimension, it will be added by this preprocessing.

        Returns:
            x (np.ndarray): input x normalized (zero mean and std 1) per channel. Shape (n_seg, n_time, n_channels, 1).
        """
        # Normalize.
        if verbose:
            print('Normalizing...')
        means, stds = self.normalization_parameters
        x = normalize_eeg_channels(x, means, stds)[0]

        # Replace nans by zeros.
        x[np.isnan(x)] = 0.0

        # Add a fourth dimension to x if not already there.
        if x.ndim != 4:
            x = np.expand_dims(x, axis=3)

        return x

    @staticmethod
    def _preprocess_recording_2_class(raw_eeg, raw_fs, verbose=1):
        """
        Preprocessing routine for 2 class classification.

        Args:
            raw_eeg (np.ndarray): EEG signal array with shape (n, 8). I.e., n time samples and 8 channels.
                The code will raise a ValueError if the size of the second dimension is not 8.
            raw_fs (float, int): sample frequency of the EEG data.
            verbose (int, optional): verbosity level.
                Defaults to 1.
        """
        raw_eeg = np.asarray(raw_eeg)

        # Check number of channels.
        if raw_eeg.shape[1] != 8 or raw_eeg.ndim != 2:
            raise ValueError('This function only supports 2D arrays with 8 channels! '
                             'Got input with {} channels (shape = {}).'
                             .format(raw_eeg.shape[1], raw_eeg.shape))

        # Replace nan by zero.
        nan_mask = np.isnan(raw_eeg)
        raw_eeg[nan_mask] = 0

        # Filter.
        if verbose > 0:
            print('Filtering...')
        eeg = filter_cnn_ansari_2019(x=raw_eeg, fs=raw_fs, axis=0)

        # Put nans back (so later we can know which segments were artefacts).
        eeg[nan_mask] = np.nan

        # Resample.
        if verbose > 0:
            print('Resampling...')
        fs = 30
        eeg = resample_by_filtering(x=eeg, fs=raw_fs, fs_new=fs, axis=0)

        return eeg, fs

    @staticmethod
    def _preprocess_recording_4_class(raw_eeg, raw_fs, verbose=1):
        """
        Preprocessing routine for 4 class classification.

        This code was ported from Matlab (CNN_Preproc2) to Python and should be exactly equivalent.

        Args:
            raw_eeg (np.ndarray): EEG signal array with shape (n, 8). I.e., n time samples and 8 channels.
                The code will raise a ValueError if the size of the second dimension is not 8.
            raw_fs (float, int): sample frequency of the EEG data.
            verbose (int, optional): verbosity level.
                Defaults to 1.
        """
        raw_eeg = np.asarray(raw_eeg)

        # Check number of channels.
        if raw_eeg.shape[1] != 8 or raw_eeg.ndim != 2:
            raise ValueError('This function only supports 2D arrays with 8 channels! Got input with {} channels (shape = {}).'
                             .format(raw_eeg.shape[1], raw_eeg.shape))

        # Params.
        fs_resamp = 30
        filtLen = int(raw_fs)
        lowpassSet = (40 / raw_fs) * 2
        highpassSet = (0.5 / raw_fs) * 2

        # Design filters.
        Bhp = signal.firwin(numtaps=filtLen + 1, cutoff=highpassSet, pass_zero='highpass')
        Blp = signal.firwin(numtaps=filtLen + 1, cutoff=lowpassSet, pass_zero='lowpass')

        # Replace nan by zero.
        nan_mask = np.isnan(raw_eeg)
        raw_eeg[nan_mask] = 0

        # Filter.
        if verbose > 0:
            print('Filtering...')
        filteredData2 = signal.lfilter(Bhp, 1, raw_eeg, axis=0)
        filteredData2 = signal.lfilter(Blp, 1, filteredData2, axis=0)

        # Correct lag.
        eeg = np.concatenate((filteredData2[int(raw_fs):, :], raw_eeg[-int(raw_fs):, :]), axis=0)

        # Put nans back (so later we can know which segments were artefacts).
        eeg[nan_mask] = np.nan

        # Resample.
        if verbose > 0:
            print('Resampling...')
        eeg = resample_by_filtering(x=eeg, fs=int(raw_fs), fs_new=int(fs_resamp), axis=0)

        return eeg, fs_resamp


class SleepStagesCnnResult(ResultBase):
    """
    High-level interface for manipulating sleep stage probabilities as predicted by
    nnsa.SleepStagesCnn().sleep_stages_cnn().

    Also note the method .to_sleep_stages_result(), which returns a more general object SleepStagesResult that provides
    a high level interface for manipulating sleep stages/labels in general. SleepStagesCnnResult class differs from
    SleepStagesResult, in that this class contains the result of a classifier, and provides methods to analyze the
    outcome of the classifier (e.g. compute score metrics, investigate probabilities etc.), whereas the
    SleepStagesResult class focusses on the analysis of the sleep stages (which may either be defined automatically
    or manually by a clinician).

    Args:
        probabilities (np.ndarray): probabilties for a number of segments per class with shape
            (num_classes, num_segments).
        class_labels (list of str): class labels corresponding to the rows of self.probabilities.
        fs (float): 1/segment length in Hz.
        algorithm_parameters (nnsa.Parameters): see ResultBase.
        is_novelty (np.ndarray, None): optional boolean array with shape (n_segments,) indicating which segmentes were
            classified as novelties (out-of-data).
        is_novelty (np.ndarray, None): optional array with shape (n_segments,) containing novelty scores for each segment.
        data_info (str, optional): see ResultBase.
        segment_start_times (np.ndarray, optional): see ResultBase.
        segment_end_times (np.ndarray, optional): see ResultBase.
    """
    def __init__(self, probabilities, class_labels, fs, algorithm_parameters,
                 is_novelty=None, novelty_score=None, latent_features=None,
                 data_info=None, segment_start_times=None, segment_end_times=None, time_offset=0):
        # Call parent's __init___.
        super().__init__(algorithm_parameters=algorithm_parameters, data_info=data_info,
                         segment_start_times=segment_start_times, segment_end_times=segment_end_times, fs=fs,
                         time_offset=time_offset)

        # Verify that a label is given for each class in probabilities array.
        if len(probabilities) != len(class_labels):
            raise ValueError(
                "Number of classes in self.probabilities is not equal to the number of class labels given.")

        # Store variables that are not already stored by the parent class (ResultBase).
        self.probabilities = probabilities
        self.class_labels = class_labels
        self.is_novelty = is_novelty
        self.novelty_score = novelty_score
        self.latent_features = latent_features
        self._df = None

    @property
    def class_mapping(self):
        """
        Return class mapping (=dictionary that maps a class label to a class number).

        Returns:
            (dict): dictionary that maps a class label to a class number.
        """
        num_classes = self.probabilities.shape[0]
        return dict(zip(self.class_labels, range(num_classes)))

    @property
    def df(self):
        if self._df is None:
            self._df = self.to_dataframe()
        return self._df

    @property
    def num_segments(self):
        """
        Return the number of segments.

        Returns:
            (int): number of segments.
        """
        return self.probabilities.shape[-1]

    @property
    def y_pred(self):
        """
        Return the class predictions.

        Returns:
            (np.ndarray): array with integer values representing classes.
        """
        return self.get_classes()

    def compute_global_features(self, **kwargs):
        """
        Compute global features.

        Args:
            **kwargs (optional): keyword arguments for SleepStagesResult.compute_global_features().

        Returns:
            df (pd.DataFrame): dataframe with one row, and feature values in columns.
        """
        return self.to_sleep_stages_result(threshold=0.5, class_labels=self.class_labels).compute_global_features(**kwargs)

    def confusion_matrix(self, y_true):
        """
        Return a ConfusionMatrix object conatining the confusion matrix.

        Args:
            y_true (np.ndarray): 1D array with true class numbers.

        Returns:
            confusion_matrix (nnsa.ConfusionMatrix): object containing the confusion matrix.
        """
        y_true = np.asarray(y_true)
        y_pred = self.get_classes()

        confusion_matrix = ConfusionMatrix(y_true=y_true, y_pred=y_pred,
                                           class_mapping=self.class_mapping)

        return confusion_matrix

    def convert_classes(self, target_class_labels):
        """
        Convert to different, derivative classes.

        Args:
            target_class_labels (list): list with new class labels.

        Returns:
            ss_out (SleepStagesCnn): new object with probabilities corresponding to the target classes.
        """
        # Copy self.
        ss_out = copy.copy(self)

        # Init new probabilities array.
        new_probabilities = np.zeros((len(target_class_labels), self.probabilities.shape[-1]))

        # For each class in the current class, determine which target class it belongs to.
        for i, label in enumerate(self.class_labels):
            # Convert to target class labels.
            new_label = annotation_to_label(label, target_class_labels)
            idx_new_label = target_class_labels.index(new_label)

            # Add probability to target class probabilities.
            new_probabilities[idx_new_label] += self.probabilities[i]

        # Set new probabilities and class labels.
        ss_out.probabilities = new_probabilities
        ss_out.class_labels = target_class_labels

        return ss_out

    def get_classes(self, threshold=None, return_probs=False):
        """
        Convert the probabilities to class numbers.

        Args:
            threshold (float, optional): threshold of probability. If probability for the maximum class is lower
                than the threshold, the class number of that segment will be np.nan. If None, the clas with the
                maximum probability will be taken.
                Defaults to None.
            return_probs (bool): return probabilities as well (True).

        Returns:
            classes (np.ndarray): 1D array with same length as self.probabilities with integers representing the classes
                as defined in self.class_labels, i.e. class i corresponds to self.class_labels[i].
            return_probs (np.ndarray): arrray with same length as classes with the maximum probabilities per segment.
        """
        # Set nan to -1 to prevent them being chosen if other classes are not nan.
        probs = self.probabilities
        probs[np.isnan(probs)] = -1

        # Convert probabilities to classes. Use float to insert nans.
        idx_max = np.argmax(probs, axis=0)
        classes = idx_max.astype(float)

        # Insert nans where max probability is nan (-1) (this happens when probabilities for all classes were nan).
        max_prob = np.max(probs, axis=0)
        classes[max_prob == -1] = np.nan
        if threshold is not None:
            # Where threshold was not high enough.
            classes[max_prob < threshold] = np.nan

        if return_probs:
            return classes, max_prob
        else:
            return classes

    def get_naive_labels(self, threshold=None):
        """
        Return the sleep label for each class.

        Args:
            threshold (float, optional): threshold of probability. If probability for the maximum class is lower
                than the threshold, the class number of that segment will be np.nan. If None, the clas with the
                maximum probability will be taken.
                Defaults to None.

        Returns:
            labels (list): list with same length as self.probabilities with the sleep labels (str).
        """
        # Set nan to -1 to prevent them being chosen if other classes are not nan.
        probs = self.probabilities
        probs[np.isnan(probs)] = -1

        # Convert probabilities to classes.
        class_idx = np.argmax(probs, axis=0)
        labels = [self.class_labels[idx] for idx in class_idx]

        return labels

    def get_probabilities(self, class_label):
        """
        Return the classification probabilities per segment for a given class_label.

        Args:
            class_label (str): the label of the class to get the probabilities from.

        Returns:
            (np.ndarray): probabilities of each segment for the class_label class.
        """
        # Check class_label.
        self._check_class_label(class_label)

        # Extract the class number of the given class label.
        class_number = self.class_labels.index(class_label)

        # Extract probabilities of class.
        return self.probabilities[class_number]

    def get_segment_indices(self, class_label):
        """
        Return the indices of the segments classified as class_label.

        Args:
            class_label (str): the label of the class to get the segment indices from.

        Returns:
            (np.ndarray): indices of segments classified as class_label.
        """
        # Check class_label.
        self._check_class_label(class_label)

        # Extract the class number of the given class label.
        class_number = self.class_labels.index(class_label)

        # Get the classes.
        classes = self.get_classes()

        # Return the indices of the segments classified as class_label.
        return np.where(classes == class_number)[0]

    def plot(self, *args, **kwargs):
        return self.plot_probabilities(*args, **kwargs)

    def plot_probability(self, *args, **kwargs):
        return self.plot_probabilities(*args, **kwargs)

    def plot_probabilities(self, class_label=None, class_number=None, time_scale='hours',
                           ax=None, novelty_color='C3', *args, **kwargs):
        """
        Plot the probility of the segments belonging to the secified class.

        Args:
            class_label (str): the label of the class to plot the probabilities of.
            novelty_color (str): color for highlighting novelties (if available). If None, novelties are not highlighted.
           *args (optional): optional arguments for the plt.plot() function.
            **kwargs (optional): optional keyword arguments for the plt.plot() function.
        """
        if ax is not None:
            plt.sca(ax)
        if class_number is None:
            if class_label is None:
                # If nothing specified, use first label.
                class_number = 0
                class_label = self.class_labels[class_number]
            else:
                # Check class_label.
                self._check_class_label(class_label)

                # Extract the class number of the given class label.
                class_number = self.class_labels.index(class_label)

        # get probabilities for given class label.
        sleep_stage = self.probabilities[class_number]

        # Extract segment times and convert to time scale.
        time = convert_time_scale(self.segment_times, time_scale)

        # Plot.
        if self.is_novelty is not None and novelty_color is not None:
            plt.plot(time, sleep_stage, *args, **dict(kwargs, color=novelty_color))
            sleep_stage_ = sleep_stage.copy()
            sleep_stage_[self.is_novelty] = np.nan
            plt.plot(time, sleep_stage_, *args, **kwargs)
        else:
            plt.plot(time, sleep_stage, *args, **kwargs)

        plt.xlabel('Time ({})'.format(time_scale))
        plt.ylim([-0.1, 1.1])
        plt.title('Probability {}'.format(class_label))
        plt.ylabel('Probability {}'.format(class_label))

    def remove_artefacts(self, af_mask, fs_mask=None, t_mask=None,
                         max_af_samples_frac=0.5, max_af_channels_frac=1/8,
                         inplace=False):
        """
        Remove artefacts by setting the probabilities of segment during which too many samples were artefacts to nan.

        Args:
            af_mask (np.ndarray): array with shape (n_channels, n_samples).
            fs_mask (float): optional sampling frequency of `af_mask`. If you do not specify this, specify `t_mask`.
            t_mask (np.ndarray): array with shape (n_samples) containing the time vector for `af_mask`.
            max_af_samples_frac (float): if the fraction of artefacts in a channel segment is larger than this,
                the channel is considered artefact for that segment.
            max_af_channels_frac (float): if the fraction of artefacted channels is larger than this.
                the segment is considered artefact and the corresponding values in self.probabilities will
                be set to nan.
            inplace (bool): whether to remove artefacts inplcae (True) or not (False). If not inplace, returns the
                new object, otherwise does not return anything.

        Returns:
            ss_out (SleepStagesCnnResult): copy of the object, but with artefacts set to nan (only if inplace is False).
        """
        if not inplace:
            ss_out = copy.deepcopy(self)
        else:
            ss_out = self

        if t_mask is None and fs_mask is None:
            raise ValueError('`fs_mask` and `t_mask` cannot both be None. Specify either `fs_mask` or `t_mask`.')

        af_mask = np.asarray(af_mask)
        if fs_mask is not None:
            # Try to segment the mask.
            seg_kwargs = dict(x=af_mask, segment_length=round(ss_out.segment_length),
                              overlap=0, fs=fs_mask, axis=1)
            n_seg = compute_n_segments(**seg_kwargs)
        else:
            n_seg = -1

        if n_seg >= ss_out.probabilities.shape[1]:
            # Segment artefact mask.
            af_mask_seg = get_all_segments(**seg_kwargs)[:ss_out.probabilities.shape[1]]

            # Check which channels are artefact.
            af_chan = af_mask_seg.mean(axis=-1) > max_af_samples_frac

            # Check if too many channels are artefact.
            ss_af = af_chan.mean(axis=-1) > max_af_channels_frac
        else:
            if t_mask is None:
                raise ValueError('Argument `t_mask` required since segmenting the mask does not lead to same '
                                 'number of segments {} as sleep stages {}. Specify `t_mask`.'
                                 .format(n_seg, ss_out.probabilities.shape[1]))

            # Loop over segments and extract the corresponding af mask segment based on time stamps
            # (slower than segmenting).
            ss_af = []
            for t_start, t_end in zip(ss_out.segment_start_times, ss_out.segment_end_times):
                idx = np.logical_and(t_mask >= t_start, t_mask < t_end)
                mask_i = af_mask[:, idx]

                # Check which channels are artefact.
                af_chan = mask_i.mean(axis=1) > max_af_samples_frac

                # Check if too many channels are artefact.
                af = af_chan.mean() > max_af_channels_frac
                ss_af.append(af)

            # To array.
            ss_af = np.array(ss_af)

        # Replace probabilities of artefacts by nan.
        ss_out.probabilities[:, ss_af] = np.nan

        if not inplace:
            return ss_out

    def robust_sleep_labels(self, aci_per_chan, amp, add_naive=False):
        """
        Robust sleep prediction by introducing more classes, like artefacts, movement, novelty, uncertain, transitional.

        Args:
            aci_per_chan (np.ndarray): 2D array with shape (n_segments, n_channels)
                containing the artefact contamination index (%) for each segment.
            amp (np.ndarray): 2D array  with shape (n_segments, n_channels) containing
                amplitudes of each segment and channel.
            add_naive (bool): if True or 'all' adds the naive/raw sleep label to the robust label in brackets.
                If 'missing': adds the naive/raw sleep label to the robust label only for missing sleep stages.
                If None or False, does not include the naive label.

        Returns:
            robust_sleep_labels (list): list with length n_segments containing robust labels.
        """
        raise DeprecationWarning('Use SleepStagesRobust')
        # Determine settings.
        num_classes = self.algorithm_parameters['num_classes']
        if num_classes == 4:
            aci_thres = 28.3333333
            entropy_thres = 1.68
            max_transition_duration = 180
            max_interpolation_duration = 180
            min_duration = 31
            max_duration = 3600

            # Minimum channels with high amplitudes to classify as movement.
            mov_min_channels = 5

        else:
            raise NotImplementedError('Not implemented for {} class sleep staging.'.format(num_classes))

        # Compute af mask per segment, per channel.
        if aci_thres is not None:
            is_af_chan = aci_per_chan > aci_thres
        else:
            is_af_chan = None

        # Compute af mask per segment.
        aci_per_seg = np.nanmean(aci_per_chan, axis=-1)
        if aci_thres is not None:
            is_art = aci_per_seg > aci_thres
        else:
            is_art = np.full(self.num_segments, fill_value=False)

        # Find movement.
        is_high_amp = detect_high_amplitudes(amp, af_mask=is_af_chan)
        is_high_amp_af = is_high_amp & is_af_chan
        num_high_amp_art_channels = np.sum(is_high_amp_af, axis=-1)
        enough_channels = (num_high_amp_art_channels >= mov_min_channels).squeeze()
        is_mov = is_art & enough_channels

        # Find uncertainties.
        if entropy_thres is not None:
            # Determine which are uncertain (too high entropy).
            entropy = -1 * np.sum(self.probabilities * np.log2(self.probabilities), axis=0)
            is_unc = entropy > entropy_thres
        else:
            is_unc = np.full(self.num_segments, fill_value=False)

        # Convert raw/naive labels to robust labels.
        raw_slp_labels = self.get_naive_labels()
        robust_slp_labels = get_robust_labels(
            slp_labels=raw_slp_labels, is_mov=is_mov, is_art=is_art, is_unc=is_unc, is_nov=self.is_novelty,
            segment_length=self.segment_length, slp_classes=self.class_labels,
            min_duration=min_duration, max_duration=max_duration,
            max_interpolation_duration=max_interpolation_duration, max_transition_duration=max_transition_duration)

        if add_naive:
            if not isinstance(add_naive, str):
                add_naive = 'all'

            if add_naive == 'missing':
                slp_classes = self.class_labels
                robust_slp_labels = [f"{slp_lab} ({raw_lab})" if slp_lab not in slp_classes else slp_lab
                                     for slp_lab, raw_lab in
                                     zip(robust_slp_labels, raw_slp_labels)]
            elif add_naive == 'all':
                robust_slp_labels = [f"{slp_lab} ({raw_lab})" for slp_lab, raw_lab in
                                     zip(robust_slp_labels, raw_slp_labels)]
            else:
                raise ValueError('Invalid add_naive="{}".').format(add_naive)

        return robust_slp_labels

    def to_annotation_set(self, threshold=None):
        """
        Convert the CNN output to an AnnotationSet with the sleep labels.

        Args:
            threshold (float, optional): theshold for probability, see self.get_classes().
                Defaults to None.

        Returns:
            annotation_set (nnsa.AnnotationSet): object containing the sleep stages as annotations.
        """
        if self.is_discontinuous():
            raise NotImplementedError('Not implemented for discontinuous data. Please implement this.')

        # Create an empty AnnotationSet.
        annotation_set = AnnotationSet(label='cnn')

        # Add epochs of one sleep stage as annotations to the annotation set.
        class_numbers = self.get_classes(threshold=threshold)

        # Replace nans with -10 to make life easier.
        class_numbers[np.isnan(class_numbers)] = -10

        # Get transitions of sleep labels.
        transition_idx = np.append(np.nonzero(np.diff(class_numbers))[0], len(class_numbers) - 1)
        time = self.segment_start_times[0]
        for idx in transition_idx:
            # Compute duration.
            duration = self.segment_end_times[idx] - time

            # Extract text label.
            class_num = class_numbers[idx]

            if class_num == -10:
                # Use 'no_label' for segments that did not meet the threshold.
                label = STANDARD_ANNOTATIONS['no_label']
            else:
                label = self.class_labels[int(class_num)]

            # Add annotation.
            annotation = Annotation(onset=time, duration=duration, text=label)
            annotation_set.append(annotation, inplace=True)

            # Update the time.
            time += duration

        return annotation_set

    def to_annotation_set_per_segment(self, threshold=None):
        """
        Convert the CNN output to an AnnotationSet with the sleep labels creating one Annotation per segment.

        Args:
            threshold (float, optional): theshold for probability, see self.get_classes().
                If None, takes class with maximum probability.

        Returns:
            annotation_set (nnsa.AnnotationSet): object containing the sleep stages as annotations.
        """
        # Create an empty AnnotationSet.
        annotation_set = AnnotationSet(label='cnn')

        # Add each segment as one annotation to the annotation set.
        segment_start_times = self.segment_start_times
        durations = self.segment_end_times - self.segment_start_times
        class_numbers = self.get_classes(threshold=threshold)
        for onset, dur, class_num in zip(segment_start_times, durations, class_numbers):
            # Extract text label.
            if np.isnan(class_num):
                # Use 'no_label' for segments that did not meet the threshold.
                label = STANDARD_ANNOTATIONS['no_label']
            else:
                label = self.class_labels[int(class_num)]

            # Add annotation.
            annotation = Annotation(onset=onset, duration=dur, text=label)
            annotation_set.append(annotation, inplace=True)

        return annotation_set

    def to_dataframe(self, *args, **kwargs):
        """
        Collect relevant results in a pandas dataframe where each row is a segment.

        Args:
            *args:
            **kwargs:

        Returns:
            df (pd.DataFrame): pandas DataFrame with segment-wise results.
        """
        # Collect results.
        data = {
            'sleep_label_cnn': self.get_naive_labels(),
            'QS_probability': self.get_probabilities(class_label='QS'),
            'start_time': self.segment_start_times,
            'end_time': self.segment_end_times,
        }

        df = pd.DataFrame(data)

        return df

    def to_df(self, *args, **kwargs):
        self.to_dataframe(*args, **kwargs)

    def to_frame(self, *args, **kwargs):
        self.to_dataframe(*args, **kwargs)

    def to_sleep_stages_result(self, threshold=None, verbose=1, **kwargs):
        """
        Convert the probabilities to sleep stages and return the sleep stages as a SleepStagesResult object.

        This is a wrapper that prepares the input for SleepStages.sleep_stages() and returns the result.

        Args:
            threshold (float, optional): theshold for probability, see self.get_classes().
                Defaults to None.
            verbose (int, optional): verbosity level for creation of sleep stages.
            **kwargs (optional): optional keyword arguments to overrule default parameters of the SleepStages class.

        Returns:
            result (nnsa.SleepStagesResult): SleepStagesResult object containing annotations related to
                sleep stages.
        """
        # Initialize SleepStages object (updates default parameters with user specified keyword arguments).
        sleep_kwargs = dict({
            'class_labels': self.class_labels,
        }, **kwargs)
        sleep_stages = SleepStages(**sleep_kwargs)

        # Convert the CNN output to annotations.
        annotation_set = self.to_annotation_set(threshold=threshold).compute_nan_durations()

        # Extract the sleep stages.
        result = sleep_stages.sleep_stages(annotation_set, verbose=verbose)

        return result

    def _check_class_label(self, class_label):
        """
        Check if class_label is in self.class_labels.

        Args:
            class_label (str): class label to check.

        Raises:
            ValueError: if class_label is invalid.
        """
        if class_label not in self.class_labels:
            raise ValueError('Invalid input argument "{}" for class_label. Choose from: {}.'
                             .format(class_label, '"' + '", "'.join(self.class_labels) + '"'))

    def _extract_epoch(self, mask):
        self.probabilities = self.probabilities[:, mask]
        if self.latent_features is not None:
            self.latent_features = self.latent_features[:, mask]
        if self.is_novelty is not None:
            self.is_novelty = self.is_novelty[mask]

    def _merge(self, other, index=None):
        """
        See ResultBase.
        """
        # TODO If novelty_score is useful, add it here (and in the save/read methods).
        if index is not None:
            n_classes, n_segments = self.probabilities.shape
            if index < n_segments:
                # Cut piece off.
                msg = 'Overwriting data while merging.'
                warnings.warn(msg)
                self.probabilities = self.probabilities[:, :index]
                if self.latent_features is not None:
                    self.latent_features = self.latent_features[:, :index]
                if self.is_novelty is not None:
                    self.is_novelty = self.is_novelty[:index]
            else:
                # Add nans.
                self.probabilities = np.concatenate([self.probabilities,
                                                   np.full((n_classes, index-n_segments),
                                                           fill_value=np.nan)], axis=-1)
                if self.latent_features is not None:
                    self.latent_features = np.concatenate([self.latent_features,
                                                       np.full((self.latent_features.shape[0], index-n_segments),
                                                               fill_value=np.nan)], axis=-1)
                if self.is_novelty is not None:
                    self.is_novelty = np.concatenate([self.is_novelty,
                                                       np.full((index-n_segments),
                                                               fill_value=np.nan)], axis=-1)
        # Merge.
        self.probabilities = np.concatenate([self.probabilities, other.probabilities], axis=-1)
        if self.latent_features is not None and other.latent_features is not None:
            self.latent_features = np.concatenate([self.latent_features, other.latent_features], axis=-1)
        if self.is_novelty is not None and other.is_novelty is not None:
            self.is_novelty = np.concatenate([self.is_novelty, other.is_novelty], axis=-1)

    @staticmethod
    def _read_from_csv(filepath):
        """
        Read result from csv file into a SleepStagesCnnResult class.

        Args:
            filepath (str): see ResultBase._read_from_csv().

        Returns:
            result (nnsa.SleepStagesCnnResult): instance of SleepStagesCnnResult containing the SleepStagesCnn result.
        """
        # Lines 1-4: Standard csv header (use the ResultBase method).
        algorithm_parameters, data_info, fs = ResultBase._read_csv_header(filepath)[1:]

        # Re-open the file and read the rest of the file, line by line.
        with open(filepath, 'r') as f:
            reader = csv.reader(f)

            # Lines 1-4: Standard csv header (already read, skip).
            [next(reader) for i in range(4)]

            # Line 5: Non-array data header (skip).
            next(reader)

            # Line 6: Non-array data.
            class_labels, probabilities_shape = [convert_string_auto(i) for i in next(reader)]

            # Line 7: Array header (skip).
            next(reader)

            # Line 8: Array data (flattened).
            array_as_list = [float(i) for i in next(reader)]

        # Convert to numpy array and reshape.
        probabilities = np.reshape(array_as_list, probabilities_shape)

        # Create a result object.
        result = SleepStagesCnnResult(probabilities=probabilities,
                                      class_labels=class_labels,
                                      algorithm_parameters=algorithm_parameters,
                                      fs=fs,
                                      data_info=data_info)

        return result

    @staticmethod
    def _read_from_hdf5(filepath):
        """
        Read result from hdf5 file into a SleepStageCnnResult class.

        Args:
            filepath (str): see ResultBase._read_from_hdf5().

        Returns:
            result (nnsa.PsdResult): instance of SleepStageCnnResult containing the result.
        """
        # Read standard csv header (use the ResultBase method).
        algorithm_parameters, data_info, segment_start_times, segment_end_times, fs, time_offset =\
            ResultBase._read_hdf5_header(filepath)[1:]

        # Re-open the file and read the rest of the file.
        with h5py.File(filepath, 'r') as f:
            # Read array data.
            p_ds = f['probabilities']
            probabilities = p_ds[:]

            if 'latent_features' in f:
                latent_features = f['latent_features'][:]
            else:
                latent_features = None

            if 'is_novelty' in f:
                is_novelty = f['is_novelty'][:]
            else:
                is_novelty = None

            # Read non-array data.
            class_labels = [label.decode() for label in p_ds.attrs['class_labels']]

        # Create a result object.
        result = SleepStagesCnnResult(probabilities=probabilities,
                                      is_novelty=is_novelty,
                                      class_labels=class_labels,
                                      latent_features=latent_features,
                                      algorithm_parameters=algorithm_parameters,
                                      data_info=data_info,
                                      segment_start_times=segment_start_times,
                                      segment_end_times=segment_end_times,
                                      fs=fs,
                                      time_offset=time_offset)

        return result

    def _write_to_csv(self, filepath):
        """
        Write the contents of the object to a csv file.

        Args:
            filepath (str): see ResultBase._write_to_csv().
        """
        raise DeprecationWarning
        # Lines 1-4: Standard csv header (use the ResultBase method).
        self._write_csv_header(filepath)

        # Append attributes to the csv file, line by line.
        with open(filepath, 'a', newline='') as csvfile:
            writer = csv.writer(csvfile)

            # Line 5: Non-array data header.
            writer.writerow(['class_labels', 'probabilities.shape'])

            # Line 6: Non-array data.
            writer.writerow([self.class_labels, self.probabilities.shape])

            # Line 7: Array header.
            writer.writerow(['probabilities'])

            # Line 8: Array data (flattened).
            writer.writerow(self.probabilities.reshape(-1).tolist())

    def _write_to_hdf5(self, filepath):
        """
        Write the contents of the object to an hdf5 file.

        Args:
            filepath (str): see ResultBase._write_to_hdf5().
        """
        # Write standard hdf5 header (use the ResultBase method).
        self._write_hdf5_header(filepath)

        # Append attributes to the hdf5 file.
        with h5py.File(filepath, 'a') as f:
            # Write array data.
            pd_ds = f.create_dataset('probabilities', data=self.probabilities)
            if self.latent_features is not None:
                f.create_dataset('latent_features', data=self.latent_features)
            if self.is_novelty is not None:
                f.create_dataset('is_novelty', data=self.is_novelty)

            # Write non-array data as attributes to the 'probabilities' dataset.
            # Convert strings to np.string_ type as recommended for compatibility.
            pd_ds.attrs['class_labels'] = [np.string_(label) for label in self.class_labels]


class SleepLatentFeatures(ClassWithParameters):
    """
    Latent features extraction using the sleep stage CNNs.

    References:
    A. H. Ansari et al., “A convolutional neural network outperforming
    state-of-the-art sleep staging algorithms for both preterm and term infants”
    Journal of Neural Engineering, vol. 17, no. 1, p. 16028, Jan. 2020,
    doi: 10.1088/1741-2552/ab5469.

    main method: process()

    Note:
        Requires keras package.
        Tested with keras version 2.2.4 and tensorflow version 1.13.1 and with python version 3.6.

    Args:
        **kwargs (optional): optional keyword arguments for nnsa.ClassWithParameters.
    """
    def __init__(self, **kwargs):
        # Call parent's __init__.
        super().__init__(**kwargs)

        self._keras_model = None  # (keras.model)
        self._keras_model_latent = None  # (keras.model)
        self._normalization_parameters = None  # (tuple)

        # Seed the random generator (needed?).
        np.random.seed(0)

    @property
    def keras_model(self):
        """
        Return the model.

        Returns:
            (nnsa.model or keras.model): model object.
        """
        if self._keras_model is None:
            self._keras_model = self._load_keras_model()
        return self._keras_model

    @property
    def keras_model_latent(self):
        """
        Return the model for latent features.

        Returns:
            (nnsa.model or keras.model): model object.
        """
        if self._keras_model_latent is None:
            self._keras_model_latent = self._get_keras_model_latent()
        return self._keras_model_latent

    @staticmethod
    def default_parameters():
        """
        Return the default parameters as a dictionary.

        Returns:
            (nnsa.Parameters): a default set of parameters for the model.
        """
        # Keyword arguments to pass to keras' model.predict method.
        predict_kwargs = {
            # Batch size (int). Note that with the default model it requires 0.864 MB of RAM per sample (when the
            # biggest two layers are completely filled with np.float64 values). This is not much, so we can afford a
            # relatively large batch size which may speed up computation time:
            'batch_size': 320,
        }

        # Parameters for artefact detection/exclusion, see
        # nnsa.artefacts.artefact_detection.default_eeg_signal_quality_criteria()
        artefact_criteria = {
            'max_nan_frac': 1e-12,  # Do not accept nans in a channel segment.
            'max_fraction_of_artefact_channels': 1/8,  # Accept 1 artefacted channel.
        }

        # Default parameters.
        pars = {
            # Number of classes in sleep stage classification (2-class (2), or 4-class (4)).
            'num_classes': 2,
            # Layer name to use as output (e.g. 'flatten_8' for large latent vector for 4 class).
            'layer_name': None,
            # Keyword arguments to pass to keras' model.predict method:
            'predict_kwargs': predict_kwargs,
            # Artefact criteria.
            'artefact_criteria': artefact_criteria,
        }

        return Parameters(**pars)

    @property
    def model_path(self):
        """
        Return the filpath of the model.

        Returns:
            model_path (str): filepath of the model.
        """
        num_classes = self.parameters['num_classes']

        if num_classes == 2:
            model_path = os.path.join(self.model_dir, 'model_v5_paramC_CH8.h5')
        elif num_classes == 4:
            model_path = os.path.join(self.model_dir, 'R_model_v5_paramC_CH8_148_1.h5')
        else:
            raise NotImplementedError('No CNN model implemented for num_classess={}.'.format(num_classes))
        return model_path

    @property
    def model_dir(self):
        """
        Return the directory containing model files.

        Returns:
            model_dir (str): path to directory with model files.
        """
        num_classes = self.parameters['num_classes']
        model_dir = os.path.join(MODEL_DATA_DIR, 'sleep_stage_classifier', f'{num_classes}_class')
        return model_dir

    @property
    def norm_param_path(self):
        """
        Return the filpath with the normalization parameters.

        Returns:
            norm_param_path (str): filepath of the pickle file with the normalization parameters. The file contains
                2 arrays. The first array contains the means per channel and the second the stds per channel for
                normalization (see self._load_normalization_parameters).
        """
        num_classes = self.parameters['num_classes']

        if num_classes == 2:
            norm_param_path = os.path.join(self.model_dir, 'normParam_paramC_CH8.pkl')
        elif num_classes == 4:
            norm_param_path = os.path.join(self.model_dir, 'R_model_v5_paramC_CH8_148_1_normParam.pkl')
        else:
            raise NotImplementedError('No CNN model implemented for num_classess={}.'.format(num_classes))
        return norm_param_path

    @property
    def data_requirements(self):
        """
        Return a dictionary with requirements for the input of the CNN.

        Returns:
            data_requirements (dict): a dictionary with requirements for the input of the CNN
                (channel order, fs, segment length).
        """
        # Extract parameters (data requirements depend on input parameters).
        num_classes = self.parameters['num_classes']

        data_requirements = dict()
        if num_classes == 2:
            # Order of the channels in the input tensor.
            data_requirements['channel_order'] = ['Fp1', 'Fp2', 'C3', 'C4', 'T3', 'T4', 'O1', 'O2']
            # The sample frequency (Hz).
            data_requirements['fs'] = 30
            # The segment length (seconds).
            data_requirements['segment_length'] = 30

        elif num_classes == 4:
            # Order of the channels in the input tensor.
            data_requirements['channel_order'] = ['Fp1', 'Fp2', 'C3', 'C4', 'T3', 'T4', 'O1', 'O2']
            # The sample frequency (Hz).
            data_requirements['fs'] = 30
            # The segment length (seconds).
            data_requirements['segment_length'] = 30

        else:
            raise NotImplementedError('Not implemented for num_classes={}.'
                                      .format(num_classes))

        return data_requirements

    @property
    def normalization_parameters(self):
        """
        Return the normalization parameters.

        Returns:
            (tuple): normalization parameters.
        """
        if self._normalization_parameters is None:
            self._normalization_parameters = self._load_normalization_parameters()
        return self._normalization_parameters

    def preprocess_recording(self, raw_eeg, raw_fs, verbose=1):
        """
        Run the preprocessing routine (filtering, resampling) on an entire recording.

        Args:
            raw_eeg (np.ndarray): raw EEG data to wct with shape (time, channels).
            raw_fs (float): sample frequency of raw EEG data.
            verbose: (int): verbosity level.

        Returns:
            eeg (np.ndarray): preprocessed EEG data.
            fs (float): sample frequency of preprocessed EEG data.
        """
        # Check input dimensions.
        if raw_eeg.shape[0] < raw_eeg.shape[1]:
            raise ValueError('Shape of `raw_eeg` should correspond to (time, channels). Got shape {}.'
                             .format(raw_eeg.shape))

        num_classes = self.parameters['num_classes']

        if num_classes == 2:
            eeg, fs = self._preprocess_recording_2_class(raw_eeg, raw_fs, verbose=verbose)
        elif num_classes == 4:
            eeg, fs = self._preprocess_recording_4_class(raw_eeg, raw_fs, verbose=verbose)
        else:
            raise NotImplementedError('Not implemented for num_classes={}.'.format(num_classes))

        assert self.data_requirements['fs'] == fs

        return eeg, fs

    def process(self, x, segment_start_times=None, segment_end_times=None, verbose=1):
        """
        Apply the feature extraction to the input x:
        1) preprocess_eeg (normalize) input x,
        2) predict the CNN output,

        Args:
            x (np.ndarray): (unnormalized) input array with shape corresponding to (segments, time, channels)
                with preprocessed (filtered and resampled) EEG data. Note
                that the time and channels dimension depend on the shape of the input layer in the CNN model. E.g. for
                the default model, shape of x must be (num_segments, 900, 8), corresponding to 30 second segments
                sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in the following order:
                Fp1, Fp2, C3, C4, T3, T4, O1, O2.
                The array may also be 4 dimensional with shape (segments, time, channels, 1), which is the shape for the
                CNN model. If x does not have this fourth dimension, it will be added by this preprocessing.
            verbose (int, optional): verbose level (0 or 1).
                Default to 1.
        Returns:
            (SleepLatentFeaturesResult): nnsa object containing the results of the CNN.
        """
        if verbose > 1:
            print(HORIZONTAL_RULE)
            print('Extracting features using {} with parameters:'.format(self.__class__.__name__))
            print(self.parameters)

        # Preprocess (normalize) the input, replace nans by zeros.
        x_preprocessed = self._preprocess(x)

        # Compute the CNN output.
        y = self._predict_cnn(x_preprocessed, verbose=verbose)

        # Postprocess the CNN output.
        y_postprocessed = self._postprocess(y)

        if self.parameters['artefact_criteria'] is not None:
            # Remove output at artefacts labels (replace with nan).
            af_mask = detect_artefact_signals(
                x, axis=1, channel_axis=-1, demean=False,
                keepdims=False, **self.parameters['artefact_criteria'])
            while af_mask.ndim > 1:
                af_mask = np.all(af_mask, axis=-1)
            y_postprocessed[af_mask] = np.nan

        if verbose > 0:
            print('Finished predicting sleep stages with CNN.')

        # Create feature labels.
        assert len(y_postprocessed) == len(x)
        feature_labels = ['F{}'.format(i) for i in range(y_postprocessed.shape[-1])]

        # Return as SleepStageCnn object.
        return FeatureSetResult(
            features=y_postprocessed,  # (segments, features).
            feature_labels=feature_labels,
            fs=1/self.data_requirements['segment_length'],
            segment_start_times=segment_start_times,
            segment_end_times=segment_end_times,
            algorithm_parameters=self.parameters)

    def _get_class_labels(self):
        """
        Return the class labels corresponding to the output array.

        Returns:
            class_labels (list of str): class labels corresponding to the output array.
        """
        # Infer model type.
        num_classes = self.parameters['num_classes']

        # Get segment length for a given model type.
        if num_classes == 2:
            # First column corresponds to non quiet sleep (NQS), seconds column to quiet sleep (QS).
            class_labels = [SLEEP_LABELS['non_quiet_sleep'], SLEEP_LABELS['quiet_sleep']]
        elif num_classes == 4:
            class_labels = [SLEEP_LABELS['active_sleep_2'], SLEEP_LABELS['active_sleep_1'],
                            SLEEP_LABELS['quiet_sleep_hvs'], SLEEP_LABELS['quiet_sleep_ta']]
        else:
            raise NotImplementedError('_get_class_labels() not implemented for num_classes="{}".'
                                      .format(num_classes))

        return class_labels

    def _get_keras_model_latent(self, verbose=0):
        """
        Get the keras model for predicting the latent features.

        Args:
            verbose (int, optional): verbose level.
                Defaults to 0.

        Returns:
            latent_model (keras.model): keras model object.
        """
        # Locally import keras due to long loading time.
        try:
            # keras can be installed as standalone package (old), or as part of tensorflow (new).
            import keras
        except (ModuleNotFoundError, ImportError):
            # https://stackoverflow.com/questions/66964492/importerror-cannot-import-name-get-config-from-tensorflow-python-eager-conte
            from tensorflow import keras

        # Get name of latent layer.
        layer_name = self.parameters['layer_name']
        if layer_name is None:
            # Default layers.
            num_classes = self.parameters['num_classes']
            if num_classes == 2:
                layer_name = 'dense_15'
            elif num_classes == 4:
                layer_name = 'dense1'
            else:
                raise NotImplementedError('Do not know which layer to take for num_classes={}.'.format(num_classes))

        # Create model that outputs the features.
        model = self.keras_model
        latent_model = keras.Model(inputs=model.input, outputs=model.get_layer(layer_name).output)

        if verbose > 0:
            latent_model.summary()

        return latent_model

    def _load_keras_model(self, verbose=0):
        """
        Load the keras model.

        Args:
            verbose (int, optional): verbose level.
                Defaults to 0.

        Returns:
            model (keras.model): keras model object.
        """
        model_path = self.model_path

        # Check if model path exists.
        if not os.path.exists(model_path):
            raise FileNotFoundError('Model path "{}" does not exist.'
                                    .format(model_path))

        if verbose > 0:
            print('Loading model {}...'.format(os.path.basename(model_path)))

        # Locally import keras due to long loading time.
        try:
            # keras can be installed as standalone package (old), or as part of tensorflow (new).
            import keras
        except (ModuleNotFoundError, ImportError):
            # https://stackoverflow.com/questions/66964492/importerror-cannot-import-name-get-config-from-tensorflow-python-eager-conte
            from tensorflow import keras

        # Load model.
        model = keras.models.load_model(model_path)

        if verbose > 0:
            model.summary()

        return model

    def _load_normalization_parameters(self):
        """
        Load the normalization parameters.

        Returns:
            means (np.ndarray): means per channel for normalization.
            stds (np.ndarray): stds per channel for normalization.
        """
        norm_param_path = self.norm_param_path

        # Check if pickle filepath exists.
        if not os.path.exists(norm_param_path):
            raise FileNotFoundError('Normalization parameter path "{}" does not exist.'
                                    .format(norm_param_path))

        # Read normalization parameters.
        with open(norm_param_path, 'rb') as f:
            means, stds = pickle.load(f)

        return means, stds

    def _postprocess(self, y):
        """
        Postprocess the data y.

        Args:
            y (np.ndarray): the (raw) output array with shape (n_segments, n_classes).

        Returns:
            y_post (np.ndarray): the postprocessed output array of size (n_segments, n_classes).
        """
        # No postprocessing.
        y_post = y

        return y_post

    def _predict_cnn(self, x, verbose=0):
        """
        Predict the model outcome of input x.

        Args:
            x (np.ndarray): preprocessed (normalized) input array with shape corresponding to
                (segments, time, channels, 1). E.g. for the default model, shape of x must be (num_segments, 900, 8, 1),
                corresponding to 30 second segments sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in
                the following order: Fp1, Fp2, C3, C4, T3, T4, O1, O2.
            verbose (int, optional): verbose level (0 or 1).
                Defaults to 0.
        Returns:
            y (np.ndarray): the (raw) output array.
        """
        # Check input shape.
        input_shape = self.keras_model_latent.input.shape.as_list()
        if list(x.shape)[1:] != input_shape[1:]:
            raise ValueError('Invalid input shape {}. Expected input shape {}.'.format(x.shape, input_shape))

        # Get options for the predict method.
        predict_kwargs = self.parameters['predict_kwargs']

        # Add verbose keyword argument for predict method.
        predict_kwargs.update({'verbose': verbose})

        y = self.keras_model_latent.predict(x, **predict_kwargs)

        return y

    def _preprocess(self, x):
        """
        Preprocess the segmented data x.

        The preprocessing consists of normalization with fixed constants (from training).

        Args:
            x (np.ndarray): (unnormalized) input array with shape corresponding to (segments, time, channels). Note
                that the time and channels dimension depend on the shape of the input layer in the CNN model. E.g. for
                the default model, shape of x must be (num_segments, 900, 8), corresponding to 30 second segments
                sampled at 30 Hz and 8 mono-polar EEG channels with reference Cz in the following order:
                Fp1, Fp2, C3, C4, T3, T4, O1, O2.
                The array may also be 4 dimensional with shape (segments, time, channels, 1), which is the shape for the
                CNN model. If x does not have this fourth dimension, it will be added by this preprocessing.

        Returns:
            x (np.ndarray): input x normalized (zero mean and std 1) per channel.
        """
        # Normalize.
        means, stds = self.normalization_parameters
        x = normalize_eeg_channels(x, means, stds)[0]

        # Replace nans by zeros.
        x[np.isnan(x)] = 0.0

        # Add a fourth dimension to x if not already there.
        if x.ndim != 4:
            x = np.expand_dims(x, axis=3)

        return x

    @staticmethod
    def _preprocess_recording_2_class(raw_eeg, raw_fs, verbose=1):
        """
        Preprocessing routine for 2 class classification.

        Args:
            raw_eeg (np.ndarray): EEG signal array with shape (n, 8). I.e., n time samples and 8 channels.
                The code will raise a ValueError if the size of the second dimension is not 8.
            raw_fs (float, int): sample frequency of the EEG data.
            verbose (int, optional): verbosity level.
                Defaults to 1.
        """
        raw_eeg = np.asarray(raw_eeg)

        # Check number of channels.
        if raw_eeg.shape[1] != 8 or raw_eeg.ndim != 2:
            raise ValueError('This function only supports 2D arrays with 8 channels! '
                             'Got input with {} channels (shape = {}).'
                             .format(raw_eeg.shape[1], raw_eeg.shape))

        # Replace nan by zero.
        nan_mask = np.isnan(raw_eeg)
        raw_eeg[nan_mask] = 0

        # Filter.
        if verbose > 0:
            print('Filtering...')
        eeg = filter_cnn_ansari_2019(x=raw_eeg, fs=raw_fs, axis=0)

        # Put nans back (so later we can know which segments were artefacts).
        eeg[nan_mask] = np.nan

        # Resample.
        if verbose > 0:
            print('Resampling...')
        fs = 30
        eeg = resample_by_filtering(x=eeg, fs=raw_fs, fs_new=fs, axis=0)

        return eeg, fs

    @staticmethod
    def _preprocess_recording_4_class(raw_eeg, raw_fs, verbose=1):
        """
        Preprocessing routine for 4 class classification.

        This code was ported from Matlab (CNN_Preproc2) to Python and should be exactly equivalent.

        Args:
            raw_eeg (np.ndarray): EEG signal array with shape (n, 8). I.e., n time samples and 8 channels.
                The code will raise a ValueError if the size of the second dimension is not 8.
            raw_fs (float, int): sample frequency of the EEG data.
            verbose (int, optional): verbosity level.
                Defaults to 1.
        """
        raw_eeg = np.asarray(raw_eeg)

        # Check number of channels.
        if raw_eeg.shape[1] != 8 or raw_eeg.ndim != 2:
            raise ValueError('This function only supports 2D arrays with 8 channels! Got input with {} channels (shape = {}).'
                             .format(raw_eeg.shape[1], raw_eeg.shape))

        # Params.
        fs_resamp = 30
        filtLen = int(raw_fs)
        lowpassSet = (40 / raw_fs) * 2
        highpassSet = (0.5 / raw_fs) * 2

        # Design filters.
        Bhp = signal.firwin(numtaps=filtLen + 1, cutoff=highpassSet, pass_zero='highpass')
        Blp = signal.firwin(numtaps=filtLen + 1, cutoff=lowpassSet, pass_zero='lowpass')

        # Replace nan by zero.
        nan_mask = np.isnan(raw_eeg)
        raw_eeg[nan_mask] = 0

        # Filter.
        if verbose > 0:
            print('Filtering...')
        filteredData2 = signal.lfilter(Bhp, 1, raw_eeg, axis=0)
        filteredData2 = signal.lfilter(Blp, 1, filteredData2, axis=0)

        # Correct lag.
        eeg = np.concatenate((filteredData2[int(raw_fs):, :], raw_eeg[-int(raw_fs):, :]), axis=0)

        # Put nans back (so later we can know which segments were artefacts).
        eeg[nan_mask] = np.nan

        # Resample.
        if verbose > 0:
            print('Resampling...')
        eeg = resample_by_filtering(x=eeg, fs=int(raw_fs), fs_new=int(fs_resamp), axis=0)

        return eeg, fs_resamp


class SleepStagesRobust(ClassWithParameters):
    """
    Robust algorthmic pipleine for 4-class sleep staging for neoantes > 36 weeks PMA.
    """
    def __init__(self, **kwargs):
        # Call parent's __init__.
        super().__init__(**kwargs)

        self._clean_detector_cnn = None
        self._hmm_model = None
        self._sleep_stages_cnn = None

    @staticmethod
    def default_parameters():
        """
        Return the default parameters as a dictionary.

        Returns:
            (nnsa.Parameters): a default set of parameters for the model.
        """
        # Options for data cleaning, i.e., kwargs for substitute_bad_channels().
        clean_options = Parameters(
            aci_window=3,
            aci_threshold=50,
            max_channels=3,
            min_duration=0,
            fs_grid=1,
        )

        # Thresholds for robust sleep determination, see also self._get_robust_labels().
        thresholds = Parameters({
            'aci_thres': 28.333,
            'entropy_cnn_thres': 1.68,
            'entropy_hmm_thres': 0.83,
            'novelty_thres': 'auto',
            'max_transition_duration': 360,
            'max_interpolation_duration': 180,
            'mov_min_channels': 5,
            'missing_window': 9 * 60 + 30,
            'max_missing_percentage': 50,
            'min_duration': 31,
            'max_duration': 7200,
        })

        # Default parameters.
        pars = {
            'clean_options': clean_options,
            'thresholds': thresholds,
        }

        return Parameters(**pars)

    @property
    def clean_detector_cnn(self):
        """
        Return the SleepStagesCnn object.
        """
        if self._clean_detector_cnn is None:
            self._clean_detector_cnn = self._init_clean_detector_cnn()
        return self._clean_detector_cnn

    @property
    def hmm_model(self):
        """
        Return the hidden-Markov model.
        """
        if self._hmm_model is None:
            self._hmm_model = self._load_hmm_model()
        return self._hmm_model

    @property
    def segment_length(self):
        return self.sleep_stages_cnn.data_requirements['segment_length']

    @property
    def sleep_stages_cnn(self):
        """
        Return the SleepStagesCnn object.
        """
        if self._sleep_stages_cnn is None:
            self._sleep_stages_cnn = self._init_sleep_stages_cnn()
        return self._sleep_stages_cnn

    @property
    def data_requirements(self):
        """
        Return a dictionary with requirements for the input of this algorithm.

        Returns:
            data_requirements (dict): a dictionary with requirements for the input of self.process()
        """
        data_requirements_cnn = self.sleep_stages_cnn.data_requirements
        channel_order = data_requirements_cnn['channel_order']
        reference_channel = data_requirements_cnn['reference_channel']

        data_requirements = dict(
            # Order of the channels in the input tensor.
            channel_order=channel_order,

            # Reference for EEG data.
            reference_channel=reference_channel,
        )

        return data_requirements

    def process(self, eeg, fs, verbose=2):
        """
        Process raw EEG data (channel order should match self.data_requirements['channel_order']).

        Args:
            eeg (np.ndarray): 2D array with shape (time, channels) with raw (unfiltered) EEG data.
                The data should be referenced as required and in the channels should have the
                order as required in self.data_requirements.
            fs (float): sampling frequency of `eeg` (Hz). Should be at minimum 128 Hz.
            verbose (int): verbosity level.
        """
        # Check input shape (n_time, n_channels), transposes if needed.
        eeg = check_eeg_is_long(eeg=eeg, mode='transpose')

        # Preprocess for artefact detection.
        eeg_pp, fs_pp = self.clean_detector_cnn.preprocess_eeg(eeg, fs, axis=0, verbose=verbose > 1)

        # Detect artefacts.
        af_mask = self._detect_artefacts(eeg_pp, fs_pp, verbose=verbose)

        # Data cleaning.
        eeg, eeg_pp, af_mask = self._clean_data(
            eeg=eeg, fs=fs, eeg_pp=eeg_pp, fs_pp=fs_pp, af_mask=af_mask, verbose=verbose)

        # Predict sleep with CNN.
        slp_cnn_result = self.sleep_stages_cnn.process(eeg=eeg, fs=fs, verbose=verbose)

        # Probabilities (n_segments, n_classes).
        p_cnn = slp_cnn_result.probabilities.T

        # Postprocess sleep with HMM.
        if verbose > 1:
            print('Post-processing with HMM...')
        p_hmm = self.hmm_model.predict_proba(p_cnn)

        # Reliability analysis.
        df = self._analyze_reliability(eeg_pp=eeg_pp, fs_pp=fs_pp, af_mask=af_mask,
                                       slp_cnn_result=slp_cnn_result, p_hmm=p_hmm, verbose=verbose)

        # Hypnogram construction.
        df_out = self._construct_hypnogram(df=df)

        # To Results class.
        result = SleepStagesRobustResult(
            df=df_out,
            algorithm_parameters=self.parameters,
            segment_start_times=slp_cnn_result.segment_start_times,
            segment_end_times=slp_cnn_result.segment_end_times
        )

        return result

    def _analyze_reliability(self, eeg_pp, fs_pp, af_mask, slp_cnn_result, p_hmm,
                             verbose=1):
        """
        Analyze relaibility by determining where artefacts/movements/novelties/uncertainties occur.
        Results per segment are collect and returned in a dataframe.
        """
        if verbose:
            print('Analysing reliability...')

        # Some useful variables.
        sleep_classes = slp_cnn_result.class_labels
        n_segments = slp_cnn_result.num_segments
        segment_length = round(slp_cnn_result.segment_length, 3)  # Round to prevent floating point problems.

        # CNN probabilities (n_segments, n_classes).
        p_cnn = slp_cnn_result.probabilities.T

        # Winning CNN class.
        y_cnn = np.argmax(p_cnn, axis=-1)
        sleep_labels_cnn = np.asarray(sleep_classes)[y_cnn]

        # Winning HMM class.
        y_hmm = np.argmax(p_hmm, axis=-1)
        sleep_labels_hmm = np.asarray(sleep_classes)[y_hmm]

        # Entropies.
        entropy_cnn = compute_entropy(p_cnn, axis=1)
        entropy_hmm = compute_entropy(p_hmm, axis=-1)

        assert n_segments == len(sleep_labels_cnn) == len(sleep_labels_hmm) == len(entropy_cnn) == len(entropy_hmm)

        # Artefact contamination index per segment per channel (n_segments, n_channels).
        aci_per_chan = stepwise_reduce(
            x=af_mask.astype(float), window=segment_length, stepsize=None,
            reduce_fun=np.nanmean, fs=fs_pp, axis=0)[0] * 100
        assert aci_per_chan.shape[0] > aci_per_chan.shape[1]

        # ACI per segment (n_segments,).
        aci_per_seg = np.nanmean(aci_per_chan, axis=1)
        assert aci_per_seg.shape == (n_segments,)

        # Clamp the EEG to reduce dynamic range (reduces effect of high amplitude outliers).
        eeg_pp_seg = get_all_segments(
            x=clamp(eeg_pp, neg_thres=-500, pos_thres=500),
            segment_length=segment_length, overlap=0, fs=fs_pp, axis=0)
        amp = np.nanmean(np.abs(eeg_pp_seg), axis=1)
        is_high_amp = detect_high_amplitudes(amp)

        # Count number of channels with high amplitude.
        num_high_amp_channels = np.sum(is_high_amp, axis=-1)
        assert len(num_high_amp_channels) == n_segments

        # Collect all results in dataframe.
        data = {
            'sleep_label_cnn': sleep_labels_cnn,
            'sleep_label_hmm': sleep_labels_hmm,
            'entropy_cnn': entropy_cnn,
            'entropy_hmm': entropy_hmm,
            'is_novelty': slp_cnn_result.is_novelty,
            'novelty_score': slp_cnn_result.novelty_score,
            'aci': aci_per_seg,
            'num_high_amp_channels': num_high_amp_channels,
        }

        # To dataframe.
        df = pd.DataFrame(data)

        return df

    def _clean_data(self, eeg, fs, eeg_pp, fs_pp, af_mask, verbose=1):
        """
        Clean the EEG data by replacing artefact channels by the mean of clean channels.
        """
        if verbose > 1:
            print('Cleaning EEG...')

        clean_options = self.parameters['clean_options']

        # Substitute bad channels in preprocessed eeg.
        eeg_pp, _ = substitute_bad_channels(
            x=eeg_pp, af_mask=af_mask, fs=fs_pp, axis=0, verbose=0, **clean_options)

        # Interpolate/upsample artefact mask to original size.
        t_new = np.arange(eeg.shape[0])/fs
        t_old = np.arange(eeg_pp.shape[0])/fs_pp
        af_mask_up = []
        for m in af_mask.T:
            m_new = np.interp(t_new, t_old, m.astype(float), left=1, right=1) > 0.5
            af_mask_up.append(m_new)
        af_mask_up = np.vstack(af_mask_up).T

        assert af_mask_up.shape == eeg.shape

        # Substitute bad channels in original eeg.
        eeg, _ = substitute_bad_channels(
            x=eeg, af_mask=af_mask_up, fs=fs, axis=0, verbose=verbose, **clean_options)

        # Redo artefact detection on cleaned data.
        af_mask = self._detect_artefacts(eeg_pp, fs_pp)

        return eeg, eeg_pp, af_mask

    def _construct_hypnogram(self, df):
        thresholds = self.parameters['thresholds']

        # Predict robust labels.
        sleep_labels_robust, sleep_labels_hmm, quality_labels = self._get_robust_labels(
            df=df, segment_length=self.segment_length, **thresholds)

        # Determine which parts of the recording are usable.
        is_sleep = (quality_labels == '') | (quality_labels == 'TS')
        is_usable = self._get_usable_mask(is_sleep=is_sleep)

        # Collect end results in new dataframe.
        df_out = pd.DataFrame({
            'sleep_label_cnn': df['sleep_label_cnn'].copy(),
            'sleep_label_hmm': sleep_labels_hmm,
            'sleep_label_robust': sleep_labels_robust,
            'quality_label': quality_labels,
            'is_sleep': is_sleep,
            'is_usable': is_usable,
        })

        return df_out

    def _detect_artefacts(self, eeg_pp, fs_pp, verbose=1):
        """
        Detect artefacts in preprocessed EEG (preprocessed for CleanDetectorCnn).
        """
        if verbose > 1:
            print('Detecting artefacts...')

        # Predict/detect clean locations.
        clean_mask = self.clean_detector_cnn.predict(
            eeg=eeg_pp, fs=fs_pp, preprocess=False,
            detect_flats=True, detect_peaks=True,
            verbose=verbose)[0]

        # Clean mask to artefact mask.
        af_mask = clean_mask == 0

        assert af_mask.shape == eeg_pp.shape

        # Mark NaNs as artefacts too.
        af_mask = af_mask | np.isnan(eeg_pp)

        # Detect anomalous channels based on line length.
        anomaly_mask = detect_anomalous_channels(
            x=eeg_pp.T, fs=fs_pp, window=3, std_factor=8, p_trim=0.25).T

        assert anomaly_mask.shape == eeg_pp.shape

        af_mask = af_mask | anomaly_mask

        return af_mask

    @staticmethod
    def _get_robust_labels(
            df, aci_thres=28.3333333,
            entropy_cnn_thres = 1.68,
            entropy_hmm_thres=0.83,
            novelty_thres = 'auto', # 'auto', 0.5312 (time_freq), 0.5432343 (pirya) # None does not do Novelty detection.,
            mov_min_channels=5,
            max_transition_duration = 360,
            max_interpolation_duration = 180,
            missing_window=9 * 60 + 30,
            max_missing_percentage=50,
            min_duration=31,
            max_duration=7200,
            segment_length=30):
        # The labels of the 4 sleep classes (order is not important).
        slp_classes = [SLEEP_LABELS['active_sleep_2'], SLEEP_LABELS['active_sleep_1'],
                       SLEEP_LABELS['quiet_sleep_hvs'], SLEEP_LABELS['quiet_sleep_ta']]

        # Find artefact.
        if aci_thres:
            is_art = df['aci'] > aci_thres
        else:
            is_art = np.full(len(df), fill_value=False)

        # Find movement.
        if mov_min_channels:
            enough_channels = (df['num_high_amp_channels'] >= mov_min_channels).squeeze()
            is_mov = is_art & enough_channels
        else:
            is_mov = np.full(len(df), fill_value=False)

        # Determine which are novelties.
        if novelty_thres:
            if novelty_thres == 'auto':
                is_nov = df['is_novelty']
            else:
                is_nov = df['novelty_score'] > novelty_thres
        else:
            is_nov = np.full(len(df), fill_value=False)

        # Determine which are uncertain (too high entropy).
        if entropy_cnn_thres:
            is_unc = (df['entropy_cnn'] > entropy_cnn_thres)
        else:
            is_unc = np.full(len(df), fill_value=False)

        if entropy_hmm_thres:
            is_unc = is_unc | (df['entropy_hmm'] > entropy_hmm_thres)

        # Assign robust labels.
        slp_labels = np.asarray(df['sleep_label_hmm'])
        new_labels = np.array(slp_labels.copy(), dtype='<U20')

        # Check if out-of-data (novelty).
        new_labels[is_nov] = 'Novelty'

        # Check if too high uncertainty.
        new_labels[is_unc] = 'Uncertain'

        # Check if too many artefacts.
        new_labels[is_art] = 'Artefact'

        # Check if movement.
        new_labels[is_mov] = 'Movement'

        # Ignore short interruptions: place back the HMM predictions where the surroundings are clean.
        is_missing = np.array([lab not in slp_classes for lab in new_labels])
        n_kernel = int(np.round(missing_window / segment_length))
        if n_kernel > len(is_missing):
            msg = 'Length of the missing_window is larger than the data length. Decreasing the missing_window.'
            warnings.warn(msg)
            n_kernel = len(is_missing)

        kernel = np.ones(n_kernel)/n_kernel
        missing_frac = np.convolve(is_missing, kernel, mode='same')
        is_clean = missing_frac <= (max_missing_percentage / 100)
        replace_mask = is_clean & is_missing
        new_labels[replace_mask] = slp_labels[replace_mask]

        # Check for short missing labels between same labels to interpolate.
        is_missing = np.array([lab not in slp_classes for lab in new_labels])
        start_idx, stop_idx = get_onsets_offsets(is_missing)
        if (len(start_idx) > 0) and (start_idx[0] == 0):  # Skip this one, we do not know the previous label.
            start_idx = start_idx[1:]
            stop_idx = stop_idx[1:]
        if (len(stop_idx) > 0) and (
                stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
            start_idx = start_idx[:-1]
            stop_idx = stop_idx[:-1]
        durations = (stop_idx - start_idx) * segment_length
        previous = new_labels[start_idx - 1]
        next_ = new_labels[stop_idx]
        interpolation_mask = (durations <= max_interpolation_duration) & (previous == next_)  # & (previous != 'Movement')
        for start, stop in zip(start_idx[interpolation_mask], stop_idx[interpolation_mask]):
            # Fill gap with surrounding sleep label.
            new_labels[start: stop] = new_labels[stop]

        # Check for long stages and mark as uncertain or artefact.
        idx_changes = np.concatenate([[0], np.where(new_labels[1:] != new_labels[:-1])[0] + 1, [len(new_labels)]])
        start_idx = idx_changes[:-1]
        stop_idx = idx_changes[1:]
        durations = (stop_idx - start_idx) * segment_length
        is_long = durations > max_duration
        for start, stop in zip(start_idx[is_long], stop_idx[is_long]):
            # Determine which label to use.
            if np.mean(is_art[start:stop]) > 0.5:
                new_lab = 'Artefact'
            else:
                new_lab = 'Uncertain'
            new_labels[start: stop] = new_lab

        # Check for short sleep stages.
        for lab in slp_classes:
            is_sleep = new_labels == lab
            start_idx, stop_idx = get_onsets_offsets(is_sleep)
            durations = (stop_idx - start_idx) * segment_length

            # Find too short durations and mark as Uncertain.
            duration_mask = (durations < min_duration)
            for start, stop in zip(start_idx[duration_mask], stop_idx[duration_mask]):
                # # Check the neighbors (maybe a good sleep stage was interrupted by short abnormalities).
                # num_nb = 3
                # mid = int((start + stop)/2)
                # start_nb = max([0, mid - num_nb])
                # stop_nb = min([mid + num_nb + 1, len(new_labels)])
                # duration_in_neighborhood = np.sum(new_labels[start_nb: stop_nb] == new_labels[mid]) * segment_length
                # if duration_in_neighborhood >= min_duration:
                #     # Do not mark as uncertain.
                #     pass
                # else:

                # Replace sleep label with uncertain.
                new_labels[start: stop] = 'Uncertain'

        # Check for short interruptions at transitions and mark as 'TS'.
        is_missing = np.array([lab not in slp_classes for lab in new_labels])
        start_idx, stop_idx = get_onsets_offsets(is_missing)
        if (len(start_idx) > 0) and (start_idx[0] == 0):  # Skip this one, we do not know the previous label.
            start_idx = start_idx[1:]
            stop_idx = stop_idx[1:]
        if (len(stop_idx) > 0) and (
                stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
            start_idx = start_idx[:-1]
            stop_idx = stop_idx[:-1]
        durations = (stop_idx - start_idx) * segment_length
        previous = new_labels[start_idx - 1]
        next_ = new_labels[stop_idx]
        transition_mask = (durations <= max_transition_duration) & (previous != next_)
        for start, stop in zip(start_idx[transition_mask], stop_idx[transition_mask]):
            new_labels[start: stop] = 'TS'

        # To sleep labels and quality labels.
        is_missing = np.array([lab not in slp_classes for lab in new_labels])
        is_sleep = ~is_missing

        quality_labels = new_labels.copy()
        quality_labels[is_sleep] = ''

        new_hmm_labels = new_labels.copy()
        new_hmm_labels[is_missing] = slp_labels[is_missing]

        return new_labels, new_hmm_labels, quality_labels

    @staticmethod
    def _get_usable_mask(is_sleep, running_window=3630, p_thres=50, segment_length=30):
        """
        Get a mask indicating the usable parts of the hypnogram.
        """
        is_sleep = np.asarray(is_sleep)

        # Convolution kernel for running psleep.
        n_kernel = min([running_window // segment_length, len(is_sleep)])
        conv_kernel = np.ones(n_kernel) / n_kernel

        # Convolve non sleep mask with a window to compute the percentage of non-sleep in that window.
        n_left = int(np.floor(n_kernel / 2))
        n_right = int(np.ceil(n_kernel / 2)) - 1
        is_sleep_padded = mirror_boundaries(is_sleep, n_left=n_left, n_right=n_right)
        p_sleep_running = np.convolve(is_sleep_padded.astype(float), conv_kernel, mode='valid')
        assert len(p_sleep_running) == len(is_sleep)

        # Find candidates of good epoch centers.
        idx_peaks = np.where(p_sleep_running > p_thres / 100)[0]

        # Sort on height (highest sleep prob is best candidate for a good sleep epoch).
        peak_heights = p_sleep_running[idx_peaks]
        idx_peaks = idx_peaks[np.argsort(peak_heights, kind='mergesort')][::-1]

        # Assign each segment as usable or not, based on whether it is contained in a candidate epoch.
        usable_mask = np.full(len(is_sleep), fill_value=False)
        while len(idx_peaks) > 0:
            idx = idx_peaks[0]
            start = max([0, idx - n_right])
            stop = min([len(is_sleep), idx + n_left + 1])
            usable_mask[start: stop] = True

            # Remove all candidate epoch centers that overlap with the current epoch.
            idx_peaks = idx_peaks[~((idx_peaks >= start) & (idx_peaks < stop))]

        # Go over epoch borders and fine-tune the border (such that the switch happens at a switch in `is_sleep`).
        idx_start, idx_stop = get_onsets_offsets(usable_mask)
        diff_is_sleep = np.diff(np.concatenate([[-1], is_sleep, [-1]]))
        for start, stop in zip(idx_start, idx_stop):
            idx_changes = np.where(diff_is_sleep != 0)[0]

            # At onset.
            if is_sleep[start]:
                # Extend usable to the left.
                idx_changes_left = idx_changes[idx_changes <= start]
                start_new = idx_changes_left[-1]
                usable_mask[start_new: start] = True
            else:
                # Extend not usable to the right.
                idx_changes_right = idx_changes[idx_changes >= start]
                start_new = idx_changes_right[0]
                usable_mask[start: start_new] = False

            # At offset
            if not is_sleep[stop - 1]:
                # Extend not usable to the left.
                idx_changes_left = idx_changes[idx_changes < stop]
                stop_new = idx_changes_left[-1]
                usable_mask[stop_new: stop] = False
            else:
                # Extend usable to the right.
                idx_changes_right = idx_changes[idx_changes >= stop]
                stop_new = idx_changes_right[0]
                usable_mask[stop: stop_new] = True

        # Sanity check for the above for-loop: all epoch boundaries in usable must be a change in is_sleep.
        idx_change_sleep = np.where(np.diff(is_sleep) != 0)[0]
        idx_change_usable = np.where(np.diff(usable_mask) != 0)[0]
        assert np.all([idx_i in idx_change_sleep for idx_i in idx_change_usable])

        return usable_mask

    def _init_clean_detector_cnn(self):
        """
        Initialize a CleanDetectorCnn object that predicts the CNN clean/artefact detection.

        Returns:
            cdc (nnsa.CleanDetectorCnn): initialize object for clean/artefact detection.
        """
        cdc = CleanDetectorCnn(multi_channel=True)
        return cdc

    def _init_sleep_stages_cnn(self):
        """
        Initialize a SleepSategsCnn object that does the CNN sleep predictions.

        Returns:
            ssc (nnsa.SleepStagesCnn): initialize object for making CNN sleep predictions.
        """
        ssc = SleepStagesCnn(
            num_classes=4, detect_novelties=True, which_novelty_features='spectral_pirya',
            artefact_criteria=None)
        return ssc

    @staticmethod
    def _load_hmm_model():
        """
         Load the Hidden-Markov Model for postprocessing the CNN predictions.

         Returns:
             hmm_model (hmmlearn.hmm.GMMHMM): trained HMM model.
         """
        # Get path to HMM model.
        num_classes = 4  # HMM is (so far) only implemented for 4 classes.
        model_dir = os.path.join(MODEL_DATA_DIR, 'sleep_stage_classifier', f'{num_classes}_class')
        fp_model = os.path.join(model_dir, 'hmm_4class.pkl')

        # Load.
        hmm_model = pickle_load(fp_model)

        return hmm_model


class SleepStagesRobustResult(ResultBase):
    """
    High-level interface for manipulating sleep stage predictions as predicted by
    nnsa.SleepStagesRobust().process().

    Args:
        df (pd.DataFrame): data with information per segment, as created by nnsa.SleepStagesRobust().process().
        algorithm_parameters (nnsa.Parameters): see ResultBase.
        name (str, int): optional name for the result/recording.
        fs (float): optional 1/segment length in Hz.
        data_info (str, optional): see ResultBase.
        segment_start_times (np.ndarray, optional): see ResultBase.
        segment_end_times (np.ndarray, optional): see ResultBase.
    """
    def __init__(self, df, algorithm_parameters, name=0, sleep_label_column='sleep_label_robust',
                 fs=None, data_info=None, segment_start_times=None, segment_end_times=None, time_offset=0):

        if segment_start_times is None:
            if 'start_time' in df:
                segment_start_times = df['start_time'].values
        if segment_end_times is None:
            if 'end_time' in df:
                segment_end_times = df['end_time'].values

        # Call parent's __init___.
        super().__init__(algorithm_parameters=algorithm_parameters, data_info=data_info,
                         segment_start_times=segment_start_times, segment_end_times=segment_end_times, fs=fs,
                         time_offset=time_offset)

        # Check if the DataFrame contains the required columns.
        required_columns = ['sleep_label_robust', 'sleep_label_hmm', 'quality_label']
        missing_columns = [col for col in required_columns if col not in df.columns]
        if len(missing_columns) > 0:
            raise ValueError('`df` is missing the following column(s): {}'.format(missing_columns))

        # Store variables that are not already stored by the parent class (ResultBase).
        self.df = df

        # Add start and end times.
        self.df['start_time'] = self.segment_start_times
        self.df['end_time'] = self.segment_end_times

        # The unique sleep classes, incl. TS.
        self.sleep_classes = ['LVI', 'ASI', 'QS HVS', 'QS TA', 'TS']
        self._sleep_label_column = None
        self.sleep_label_column = sleep_label_column
        self.name = name

    @property
    def num_segments(self):
        """
        Return the number of segments.

        Returns:
            (int): number of segments.
        """
        return len(self.df)

    @property
    def sleep_label_column(self):
        return self._sleep_label_column

    @sleep_label_column.setter
    def sleep_label_column(self, col):
        if col not in self.df:
            raise ValueError('"{}" not in "df".'.format(col))
        self._sleep_label_column = col

    def compute_recording_composition(self, subclass_sleep=False, in_percentage=True):
        """
        Recording composition (percentages of each class in 'sleep_label_robust').

        Args:
            subclass_sleep (bool): if True, uses the subclasses for the sleep.
                If False, counts all sleep subclasses into one Sleep class.
            in_percentage (bool): if True, the composition is in percentage.
                If False, the proportions are returned (values between 0 and 1).

        Returns:
            features (pd.Series): series with features.
        """
        labels = self.df[self.sleep_label_column]

        # Compute the relative coverages.
        if not subclass_sleep:
            # All sleep labels to one sleep label.
            labels = labels.apply(lambda x: 'Sleep' if x in self.sleep_classes else x)
        counts = dict()
        all_classes = labels.unique()
        for lab in all_classes:
            counts[lab] = np.sum(labels == lab)
        total_count = np.sum(list(counts.values()))
        fracs = dict((lab, count / total_count) for (lab, count) in counts.items())

        if abs(np.sum(list(fracs.values())) - 1) > 1e-8:
            raise AssertionError

        # To Series.
        features = pd.Series(fracs, name=self.name)

        if in_percentage:
            features = features*100

        # Add prefix p to the label to indicate proportion/percentage.
        features = features.add_prefix('p')

        # Check for nans.
        if features.isna().any():
            raise AssertionError("Proportion is nan. This is unexpected. Debug this function.")

        return features

    def compute_sleep_composition(self, in_percentage=True, usable_only=False):
        """
        Compute sleep composition features (i.e., the distribution of the labeled sleep into the sleep stages).

        Args:
            in_percentage (bool): if True, the composition is in percentage.
                If False, the proportions are returned (values between 0 and 1).
            usable_only (bool): whether to include sleep segments during "unusable" epochs (False) or not (True).

        Returns:
            features (pd.Series): series with features.
        """

        sleep_label_column = self.sleep_label_column
        df_rec = self.df

        # Compute the relative coverages (only in usable sleep epochs).
        df_sleep = df_rec[df_rec['is_sleep']]

        # Only the usable sleep parts.
        if usable_only:
            df_sleep = df_sleep[df_sleep['is_usable']]

        counts = dict()
        for lab in self.sleep_classes:
            counts[lab] = np.sum(df_sleep[sleep_label_column] == lab)
        total_count = np.sum(list(counts.values()))
        fracs = dict((lab, count/total_count) for (lab, count) in counts.items())

        # Sum of fractions must equal 1.
        if abs(np.sum(list(fracs.values())) - 1) > 1e-8:
            raise AssertionError

        # To Series.
        features = pd.Series(fracs, name=self.name)

        if in_percentage:
            features = features*100

        # Combine AS and QS.
        features['AS'] = features['LVI'] + features['ASI']
        features['QS'] = features['QS TA'] + features['QS HVS']

        # Add prefix p to the label to indicate proportion/percentage.
        features = features.add_prefix('p')

        return features

    def compute_sleep_cyclicity(self):
        """
        Compute number of sleep cycles per hour.

        Returns:
            features (pd.Series): series with features.
        """
        sleep_label_column = self.sleep_label_column
        sleep_classes = [sc for sc in self.sleep_classes if sc != 'TS']  # Ignore TS.
        df_rec = self.df

        # Minimum duration of a AS/QS sleep stage to count.
        min_duration = 3 * 60 - 1  # In seconds.

        # Loop over usable parts and keep track of total number of cycles and elapsed time.
        is_usable = df_rec['is_usable']
        num_cycles = 0
        elapsed_hours = 0
        elapsed_sleep_time = 0
        for start_idx, stop_idx in zip(*get_onsets_offsets(is_usable)):
            # Extract the current part.
            df_i_all = df_rec.iloc[start_idx: stop_idx]

            # Only keep sleep.
            keep = df_i_all[sleep_label_column].apply(lambda x: x in sleep_classes)
            df_i = (df_i_all[keep]).copy()

            # Convert to 2 class.
            df_i['2_class_sleep_label'] = df_i[sleep_label_column].apply(to_2_class)

            # Merge successive stages (does not merge same sleep labels with gap in between).
            df_i['duration'] = df_i['end_time'] - df_i['start_time']
            an_set = AnnotationSet().from_df(
                df=df_i,
                onset='start_time',
                duration='duration',
                text='2_class_sleep_label').merge_successive_texts()

            # Remove short QS or AS (ignoring the gaps).
            idx_transitions = np.where(np.diff(an_set.texts() == 'QS', prepend=-1, append=-1) != 0)[0]
            sleep_start_idx, sleep_stop_idx = idx_transitions[:-1], idx_transitions[1:]
            durations = an_set.durations()
            total_sleep_durations = np.zeros(len(durations))
            for start, stop in zip(sleep_start_idx, sleep_stop_idx):
                sleep_duration = np.sum(durations[start: stop])
                total_sleep_durations[start: stop] = sleep_duration
            idx_remove = np.where(total_sleep_durations <= min_duration)[0]
            an_set = an_set.replace_text_by_index(
                idx_remove, new_text='remove', inplace=False).remove(patterns='remove')

            # Find transitions.
            idx_transitions = np.where(np.diff(an_set.texts() == 'QS') != 0)[0]

            # Count number of cycles (is a half in case of no transitions).
            num_cycles_i = (len(idx_transitions) + 1) / 2

            # Measure time window.
            time_start = df_i['start_time'].iat[0]
            time_stop = df_i['end_time'].iat[-1]
            elapsed_hours_i = (time_stop - time_start) / 3600

            # Measure the total amount of sleep time (in min).
            is_sleep = df_i_all[sleep_label_column].apply(lambda x: x in sleep_classes + ['TS'])
            elapsed_sleep_time_i = is_sleep.sum() * self.segment_length / 60

            # Add to counts for the different usable parts.
            num_cycles += num_cycles_i
            elapsed_hours += elapsed_hours_i
            elapsed_sleep_time += elapsed_sleep_time_i

        if is_usable.any():
            # Compute total cyclicity (per hour).
            cycles_per_hour = num_cycles / elapsed_hours

            # Compute average sleep time (in min) in one cycle.
            sleep_cycle_duration = elapsed_sleep_time / num_cycles
        else:
            cycles_per_hour = np.nan
            sleep_cycle_duration = np.nan

        # Collect.
        data = {
            'Cycles/h': cycles_per_hour,
            'Sleep time/cycle': sleep_cycle_duration,
            'num_cycles': num_cycles,
            'elapsed_usable_hours': elapsed_hours,
            'elapsed_sleep_minutes': elapsed_sleep_time
        }

        # To Series.
        features = pd.Series(data, name=self.name)

        return features

    def compute_survival(self, two_class=False, df_km=None):
        """
        Compute surivival analysis for bout-durations features.

        Args:
            two_class (bool): whether to do a 2-class analysis (True) or 4-class (False).

        Returns:
            features (pd.Series): series with features.
        """
        if two_class:
            sleep_classes = ['AS', 'QS']
        else:
            sleep_classes = ['LVI', 'ASI', 'QS HVS', 'QS TA', 'TS']

        if df_km is None:
            df_km = self.prepare_kaplan_meier(two_class=two_class)

        # Loop over labels.
        data = dict()  # For results.
        for label in sleep_classes:
            median_duration = np.nan
            median_duration_km = np.nan
            exponential_lambda = np.nan

            data_ii = df_km[df_km['Label'] == label]
            if len(data_ii) > 0:
                T = np.asarray(data_ii['duration'])/60  # In minutes.
                E = np.asarray(data_ii['ending_observed'])
                median_duration = np.nanmedian(T)

                # kmf = KaplanMeierFitter()
                # kmf.fit(T, event_observed=E)
                # median_duration_km = kmf.median_survival_time_

                # if median_duration_km > 1e8:
                #     # Happens when there are too little sleep bouts.
                #     median_duration_km = np.nan
                # if len(T) > 1:
                #     # Compute parametric features.
                #     exf = ExponentialFitter().fit(T, E, label='ExponentialFitter')
                #     exponential_lambda = exf.lambda_
                #
                #     if exponential_lambda > 1e8:
                #         # When no ending is observed.
                #         exponential_lambda = np.nan

            data[f'Median {label}'] = median_duration
            # data[f'Median KM {label}'] = median_duration_km
            # data[f'Exponential lambda {label}'] = exponential_lambda

        # To Series.
        features = pd.Series(data, name=self.name)

        return features

    def compute_unexpected_transitions(self):
        """
        Compute the number of unexpected sleep transitions per hour.
        This only looks at usable parts and ignores any non-sleep or TS stages in between sleep stages.

        Returns:
            features (pd.Series): series with features.
        """
        sleep_label_column = self.sleep_label_column
        sleep_classes = [sc for sc in self.sleep_classes if sc != 'TS']  # Ignore TS.
        df_rec = self.df

        # Define to which other sleep stage(s) each sleep stage is expected to transition into.
        expected_transitions = {
            'LVI': ['ASI'],
            'ASI': ['QS HVS', 'QS TA'],
            'QS HVS': ['QS TA'],
            'QS TA': ['LVI'],
        }

        # Loop over usable parts and keep track of total number of unexpected transitions and elapsed time.
        is_usable = df_rec['is_usable']
        num_unexpected = 0
        elapsed_hours = 0
        for start_idx, stop_idx in zip(*get_onsets_offsets(is_usable)):
            # Extract the current part.
            df_i = df_rec.iloc[start_idx: stop_idx].copy()

            # Merge successive stages.
            df_i['duration'] = df_i['end_time'] - df_i['start_time']
            an_set = AnnotationSet().from_df(
                df=df_i,
                onset='start_time',
                duration='duration',
                text=sleep_label_column).merge_successive_texts()

            # Get the labels.
            sleep_labels = an_set.texts()

            # Remove the 'TS' labels.
            sleep_labels = sleep_labels[sleep_labels != 'TS']

            # Count unexpected transitions.
            num_unexpected_i = 0
            for label, next_label in zip(sleep_labels[:-1], sleep_labels[1:]):
                if (label in sleep_classes) and (next_label in sleep_classes):
                    if next_label not in expected_transitions[label]:
                        num_unexpected_i += 1

            # Measure time window.
            time_start = df_i['start_time'].iat[0]
            time_stop = df_i['end_time'].iat[-1]
            elapsed_hours_i = (time_stop - time_start) / 3600

            # Add to counts for the different usable parts.
            num_unexpected += num_unexpected_i
            elapsed_hours += elapsed_hours_i

        # Compute total per hour.
        unexpected_per_hour = num_unexpected / elapsed_hours

        # Collect.
        data = {
            'Unexpected trans/h': unexpected_per_hour,
            'num_unexpected': num_unexpected,
            'elapsed_hours': elapsed_hours,
        }

        # To Series.
        features = pd.Series(data, name=self.name)

        return features

    def compute_usable_percentage(self):
        """
        Return the percentage of usable data.

        Returns:
            usable_percentage (pd.Series): % usable data.
        """
        usable_percentage = self.df['is_usable'].mean() * 100
        return usable_percentage

    def get_hypnogram(self, which='4class'):
        """
        Helper function to collect durations and ending observed for survivial analysis.
        Args:
            which (bool): how specific the hypnogram is:
                '1class': non-sleep vs sleep.
                '2class': non-sleep vs 2-class sleep.
                '4class': non-sleep vs 4-class sleep.

        Returns:
            labels (np.ndarray): sequence of labels.
        """
        df_rec = self.df
        sleep_label_column = self.sleep_label_column

        # Get sleep label per segment.
        labels = df_rec[sleep_label_column].copy()

        # Check if TS in labels and remove.
        if 'TS' in labels.values:
            labels = df_rec['sleep_label_hmm'].copy()
            quality_labels = df_rec['quality_label']
            mask_sleep = (quality_labels == 'Ok') | (quality_labels == 'TS')
            labels[~mask_sleep] = 'Non-sleep'

        # All non-sleep to Non-sleep.
        sleep_stages = ['LVI', 'ASI', 'QS HVS', 'QS TA']
        labels = labels.apply(lambda x: x if x in sleep_stages else 'Non-sleep')

        # Merge classes if needed.
        if which == '1class':
            labels = labels.apply(lambda x: to_1_class(x, raise_error=False))
        elif which == '2class':
            labels = labels.apply(lambda x: to_2_class(x, raise_error=False))
        elif which == '4class':
            pass
        else:
            raise ValueError(f'Invalid input which="{which}".')

        return labels.values

    def plot(self, which=None, order=None, label_colors=None,
             add_legend=True, ax=None, mask_alpha=0.7, plot_quality=True, mask_unusable=True):
        """"
        Helper function to quikly plot the hypnogram.
        """
        if which is None:
            which = self.sleep_label_column

        df = self.df
        if order is None:
            order = HYPNOGRAM_ORDER[::-1]
        if label_colors is None:
            label_colors = LABEL_COLORS
        if ax is None:
            ax = plt.gca()

        hyp_kwargs = dict(dict(
            order=order,
            quality_colors=label_colors),
            quality_alpha=1)

        sleep_labels = df[which]
        quality_labels = df['quality_label']
        time = self.segment_start_times

        plot_hypnogram_quality(
            sleep_labels=sleep_labels,
            quality_labels=quality_labels if plot_quality else None,
            onsets=time, durations=None, mask_color='w', mask_alpha=mask_alpha,
            add_legend=add_legend, ax=ax, **hyp_kwargs)

        if mask_unusable and 'is_usable' in df:
            # Mask non-usable parts.
            usable_mask = df['is_usable']
            start_idx, stop_idx = get_onsets_offsets((~usable_mask).astype(int))
            for start, stop in zip(start_idx, stop_idx):
                # ax.add_patch(Rectangle((start * 30, 0), (stop - start) * 30, 1,
                #                        facecolor='r', alpha=0.3, edgecolor=None))
                ax.add_patch(Rectangle((start * 30, 0), (stop - start) * 30, 1,
                                       facecolor=np.ones(3) * 0.5, alpha=1., edgecolor=None,
                                       hatch='////'
                                       ))

    def prepare_kaplan_meier(self, two_class, missing_not_observed=False):
        """
        Helper function to collect durations and ending observed for survivial analysis.

        Args:
            two_class (bool): whether to do a 2-class analysis (True) or 4-class (False).
            missing_not_observed (bool): if True, sets the ending_observed to False for sleep stages
                followed by a non-sleep (missing) label. If False, only sets the first and last one to not observed.

        Returns:
            df (pd.DataFrame): dataframe with bout durations, ending observed the corresponding label.
        """
        df_rec = self.df
        sleep_label_column = self.sleep_label_column

        # Interpolate short missing (non-sleep) labels.
        max_interpolation_duration = 3 * 60 + 1  # In seconds.

        if two_class:
            sleep_classes = ['AS', 'QS']
        else:
            sleep_classes = ['LVI', 'ASI', 'QS HVS', 'QS TA', 'TS']

        # Loop over usable parts and keep track of total number of cycles and elapsed time.
        is_usable = df_rec['is_usable']
        data = defaultdict(lambda: defaultdict(list))
        for start_idx, stop_idx in zip(*get_onsets_offsets(is_usable)):
            # Extract the current part.
            df_i = df_rec.iloc[start_idx: stop_idx].copy()

            # To 2 class sleep.
            if two_class:
                df_i['new_label'] = df_i[sleep_label_column].apply(lambda x: to_2_class(x, raise_error=False))
            else:
                df_i['new_label'] = df_i[sleep_label_column]

            # Check for short missing labels between same labels to interpolate.
            if max_interpolation_duration > 0:
                new_labels = df_i['new_label'].values.copy()
                is_missing = np.array([lab not in sleep_classes for lab in new_labels])
                start_idx, stop_idx = get_onsets_offsets(is_missing)
                if (len(start_idx) > 0) and (
                        start_idx[0] == 0):  # Skip this one, we do not know the previous label.
                    start_idx = start_idx[1:]
                    stop_idx = stop_idx[1:]
                if (len(stop_idx) > 0) and (
                        stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
                    start_idx = start_idx[:-1]
                    stop_idx = stop_idx[:-1]
                durations = (stop_idx - start_idx) * self.segment_length
                previous = new_labels[start_idx - 1]
                next_ = new_labels[stop_idx]
                interpolation_mask = (durations <= max_interpolation_duration) & (
                        previous == next_)
                for start, stop in zip(start_idx[interpolation_mask], stop_idx[interpolation_mask]):
                    # Fill gap with surrounding sleep label.
                    new_labels[start: stop] = new_labels[stop]
                df_i['new_label'] = new_labels

            # Merge successive.
            df_i['duration'] = df_i['end_time'] - df_i['start_time']
            an_set = AnnotationSet().from_df(
                df=df_i,
                onset='start_time',
                duration='duration',
                text='new_label').merge_successive_texts()

            # For each sleep stage, create the durations and ending observed arrays for kaplan meier.
            for idx, annot in enumerate(an_set):
                label = annot.text
                if label not in sleep_classes:
                    continue
                if idx == 0:
                    # First one: ending not observed (actually onset not observed...).
                    ending_observed = False
                elif idx == (len(an_set) - 1):
                    # Last one: ending not observed.
                    ending_observed = False
                else:
                    if missing_not_observed:
                        ending_observed = an_set[idx + 1].text in sleep_classes
                    else:
                        # All is observed (better when looking per recording, otherwise chances are high no endings are observed, which leads to weird Kaplan-Meier curves).
                        ending_observed = True
                data[label]['duration'].append(annot.duration)
                data[label]['ending_observed'].append(ending_observed)

        # Convert to a dataframe.
        df = []
        for label, data_i in data.items():
            df_i = pd.DataFrame(data_i)
            df_i['Label'] = label
            df.append(df_i)
        df = pd.concat(df, axis=0, ignore_index=True)

        return df

    @staticmethod
    def _read_from_hdf5(filepath):
        """
        Read result from hdf5 file into a SleepStageCnnResult class.

        Args:
            filepath (str): see ResultBase._read_from_hdf5().

        Returns:
            result (nnsa.PsdResult): instance of SleepStageCnnResult containing the result.
        """
        # Read standard csv header (use the ResultBase method).
        algorithm_parameters, data_info, segment_start_times, segment_end_times, fs, time_offset = \
            ResultBase._read_hdf5_header(filepath)[1:]

        # Re-open the file and read the rest of the file.
        df = pd.read_hdf(filepath, key='df')

        # Create a result object.
        result = SleepStagesRobustResult(
            df=df,
            algorithm_parameters=algorithm_parameters,
            data_info=data_info,
            segment_start_times=segment_start_times,
            segment_end_times=segment_end_times,
            fs=fs,
            time_offset=time_offset)

        return result

    def _write_to_hdf5(self, filepath):
        """
        Write the contents of the object to an hdf5 file.

        Args:
            filepath (str): see ResultBase._write_to_hdf5().
        """
        # Write standard hdf5 header (use the ResultBase method).
        self._write_hdf5_header(filepath)

        # Append attributes to the hdf5 file.
        self.df.to_hdf(filepath, key='df', mode='a')


def add_sleep_features_2(sleep_stages_result, subject_number, timing, sleep_features_dict):
    """
    Helper function to add features in a 2-class SleepStagesResult to a dictionary.

    Args:
        sleep_stages_result (nnsa.SleepStagesResult): object containing sleep stages result.
        subject_number (int): number/id of the subject.
        timing (str): string specifying the timing of the recording (e.g. 'ante', or 'post').
        sleep_features_dict (defaultdict): defaultdict that defaults to an empty list to which to append the features.
    """
    # Add subject number.
    sleep_features_dict['subject_number'].append(subject_number)

    # Add pre or post.
    sleep_features_dict['timing'].append(timing)

    # Add duration of the annotated part of the recording in hours.
    annot_set = sleep_stages_result.annotation_set
    annotated_recording_duration = annot_set.onsets()[-1] + annot_set.durations()[-1] - annot_set.onsets()[0]  # In sec.
    sleep_features_dict['annotated_recording_duration'].append(annotated_recording_duration / 60 / 60)

    # Add duration of no_label.
    sleep_features_dict['duration_NL'].append(sleep_stages_result.to_dataframe()
                                              .groupby('text')['duration'].sum()
                                              .get(SLEEP_LABELS['no_label'], np.nan))

    # Add QS sleep features.
    qs_features = sleep_stages_result.extract_features('QS')
    for k, v in qs_features.items():
        sleep_features_dict[k].append(v)

    # Add counts of annotations.
    counts = sleep_stages_result.count_annotations_per_class()
    for k, v in counts.items():
        sleep_features_dict['count_{}'.format(k)].append(v)


def get_cnn_sleep_result(filepath, sleep_stages_cnn=None, save_cnn_result=False, verbose=1):
    """
    Helper function to read an EDF+ file and do the sleep stage classification using the CNN.

    Args:
        filepath (str): filepath of the EDF+.
        sleep_stages_cnn (SleepStagesCnn, optional): object containing the CNN model and parameters.
            If None, the default model and parameters will be used.
        save_cnn_result (bool, optional): if True, saves the cnn result.
            If False, does not save it.
        verbose (int, optional): verbosity level.
            Defaults to 1.

    Returns:
        sleep_cnn (nnsa.SleepStagesCnnResult): object containing the result of the sleep classification.
    """
    from nnsa.io.readers import EdfReader
    with EdfReader(filepath) as r:
        # Read EEG data.
        ds = r.read_eeg_dataset()

        if sleep_stages_cnn is None:
            # Use default.
            sleep_cnn = ds.sleep_stages_cnn(filter=True, resample=True, verbose=verbose)

        else:
            # Prepare the dataset for the CNN.
            x = ds._prepare_sleep_stages_cnn(filter=True, resample=True)[0]

            # Run sleep stages cnn algorithm using the specified object.
            sleep_cnn = sleep_stages_cnn.sleep_stages_cnn(x, verbose=verbose)

        # Save result of CNN.
        if save_cnn_result:
            name = os.path.splitext(os.path.basename(filepath))[0]
            dir_out = 'output'
            sleep_cnn.save_to_file(os.path.join(dir_out, '{}_sleep_stages_cnn.hdf5'.format(name)))

    return sleep_cnn


def merge_annotation_sets(ss_left, ss_right, dt=0.01, label=None):
    """
    Merge the annotation sets of two sleep stage objects.

    1) Sort.
    2) Resample both annotation sets with a sampling period dt.
    3) Merge the annotation texts of resampled annotation sets.
    4) Merge successive texts.

    Args:
        ss_left (nnsa.SleepStagesResult):
        ss_right (nnsa.SleepStagesResult):
        dt (float, optional): resampling period (in seconds). Make sure this is smaller
            than the precision of the onset and duration times.
            Defaults to 0.01
        label (str, optional): label for the new annotation set.
            Defaults to None.

    Returns:
        as_merged (nnsa.AnnotationSet): AnnotationSet object with the merged sleep stage annotations.
    """
    # Sort.
    ss_left.annotation_set.sort(inplace=True)
    ss_right.annotation_set.sort(inplace=True)

    # Get start times.
    start_time_left = ss_left.annotation_set.annotations[0].onset
    start_time_right = ss_right.annotation_set.annotations[0].onset

    # Get end times.
    end_time_left = ss_left.annotation_set.annotations[-1].onset + ss_left.annotation_set.annotations[-1].duration
    end_time_right = ss_right.annotation_set.annotations[-1].onset + ss_right.annotation_set.annotations[-1].duration

    # Get global start and end time.
    t_start = min([start_time_left, start_time_right])
    t_end = max([end_time_left, end_time_right])

    # Create time array.
    start_times = np.arange(t_start, t_end, dt)
    end_times = start_times + dt*0.9  # Make it end just before to work on transitions.

    # Get labels at the high freq times.
    labels_left = ss_left.segment_labels(start_times, end_times)
    labels_right = ss_right.segment_labels(start_times, end_times)

    map_left = ss_left.class_mapping_invert
    map_right = ss_right.class_mapping_invert

    # Add to empty AnnotationSet.
    as_merged = AnnotationSet(label=label)
    for left, right, onset in zip(labels_left, labels_right, start_times):
        if not np.isnan(left) and not np.isnan(right):
            new_text = '{} + {}'.format(map_left[left], map_right[right])
        elif not np.isnan(left):
            new_text = map_left[left]
        elif not np.isnan(right):
            new_text = map_right[right]
        else:
            # Both nan.
            new_text = SLEEP_LABELS['no_label']

        as_merged.append(Annotation(onset=onset, duration=dt, text=new_text), inplace=True)

    # Merge successive annotations.
    as_merged.merge_successive_texts(inplace=True)

    return as_merged


def get_robust_labels(slp_labels, is_mov, is_art, is_unc, is_nov,
                      segment_length=30, slp_classes=None,
                      min_duration=0, max_duration=np.inf,
                      max_interpolation_duration=0, max_transition_duration=0):
    if slp_classes is None:
        # Unique sleep labels.
        slp_classes = list(set(slp_labels))

    # Check for short stages.
    slp_labels = np.array(slp_labels.copy(), dtype='<U20')
    for lab in slp_classes:
        is_sleep = slp_labels == lab
        start_idx, stop_idx = get_onsets_offsets(is_sleep)
        durations = (stop_idx - start_idx) * segment_length

        # Find too short durations and mark as Uncertain.
        duration_mask = (durations < min_duration)
        for start, stop in zip(start_idx[duration_mask], stop_idx[duration_mask]):
            # Check the neighbors (maybe a good sleep stage was interrupted by short abnormalities).
            num_nb = 3
            mid = int((start + stop)/2)
            start_nb = max([0, mid - num_nb])
            stop_nb = min([mid + num_nb + 1, len(slp_labels)])
            duration_in_neighborhood = np.sum(slp_labels[start_nb: stop_nb] == slp_labels[mid]) * segment_length
            if duration_in_neighborhood >= min_duration:
                # Do not mark as uncertain.
                pass
            else:
                # Replace sleep label with uncertain.
                slp_labels[start: stop] = 'Uncertain'

    is_short = slp_labels == 'Uncertain'

    # Check if out-of-data (novelty).
    slp_labels[is_nov] = 'Novelty'

    # Check if too high uncertainty.
    slp_labels[is_unc] = 'Uncertain'

    # Check if too many artefacts.
    slp_labels[is_art] = 'Artefact'

    # Check if movement.
    slp_labels[is_mov] = 'Movement'

    # Check for short missing labels to interpolate. v2: do interpolate movement (e.g. in LVI).
    is_missing = (is_short | is_nov | is_unc | is_art | is_mov)# & ~is_mov
    start_idx, stop_idx = get_onsets_offsets(is_missing)
    if (len(start_idx) > 0) and (start_idx[0] == 0):  # Skip this one, we do not know the previous label.
        start_idx = start_idx[1:]
        stop_idx = stop_idx[1:]
    if (len(stop_idx) > 0) and (
            stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
        start_idx = start_idx[:-1]
        stop_idx = stop_idx[:-1]
    durations = (stop_idx - start_idx) * 30  # Each segment is 30 seconds.
    previous = slp_labels[np.clip(start_idx - 1, 0, np.inf).astype(int)]
    next_ = slp_labels[np.clip(stop_idx, 0, len(is_missing) - 1).astype(int)]

    # Find short gaps surrounded by same label and interpolate. v1: do not interpolate movement.
    interpolation_mask = (durations <= max_interpolation_duration) & (previous == next_)# & (previous != 'Movement')
    for start, stop in zip(start_idx[interpolation_mask], stop_idx[interpolation_mask]):
        # Only interpolate if short movement (max 2 segments).
        if np.sum(slp_labels[start: stop] == 'Movement') > 2:
            # Long movement, do not interpolate.
            continue

        # Fill gap with surrounding sleep label.
        slp_labels[start: stop] = slp_labels[stop]

    # Check for short uncertainties/novelties for transitions. v2: do not count movement/art as a transition.
    is_missing = np.array([lab not in slp_classes for lab in slp_labels])
    start_idx, stop_idx = get_onsets_offsets(is_missing)
    if (len(start_idx) > 0) and (start_idx[0] == 0):  # Skip this one, we do not know the previous label.
        start_idx = start_idx[1:]
        stop_idx = stop_idx[1:]
    if (len(stop_idx) > 0) and (
            stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
        start_idx = start_idx[:-1]
        stop_idx = stop_idx[:-1]
    durations = (stop_idx - start_idx) * 30  # Each segment is 30 seconds.
    previous = slp_labels[start_idx - 1]
    next_ = slp_labels[stop_idx]

    # Find transitions and mark as TS.
    transition_mask = (durations <= max_transition_duration) & (previous != next_)
    for start, stop in zip(start_idx[transition_mask], stop_idx[transition_mask]):
        # v2: only assign Uncertain to TS.
        new_labels = slp_labels[start: stop].copy()
        new_labels[(new_labels == 'Uncertain')] = 'TS'
        # new_labels[is_unc[start:stop]] = 'TS'
        slp_labels[start: stop] = new_labels

    # Check for remaining stages that are too long.
    for lab in slp_classes:
        is_sleep = slp_labels == lab
        start_idx, stop_idx = get_onsets_offsets(is_sleep)
        durations = (stop_idx - start_idx) * 30  # Each segment is 30 seconds.

        # Find too long durations and mark as Uncertain.
        duration_mask = (durations > max_duration)
        for start, stop in zip(start_idx[duration_mask], stop_idx[duration_mask]):
            # Replace sleep label with uncertain.
            slp_labels[start: stop] = 'Uncertain'

    return slp_labels.tolist()


def interpolate_labels(labels, labels_to_interpolate, max_interpolation_length):
    """
    Interpolate `labels_to_interpolate` in an array of labels when the missing data last max for
    `max_interpolation_length` number of successive labels, and the label preceding and succeeding
    the missing data is the same.
    """
    labels = np.asarray(labels).copy()

    # Check for short missing labels to interpolate.
    is_sleep = np.asarray([lab in labels_to_interpolate for lab in labels])
    is_missing = ~is_sleep
    start_idx, stop_idx = get_onsets_offsets(is_missing)
    if (len(start_idx) > 0) and (start_idx[0] == 0):  # Skip this one, we do not know the previous label.
        start_idx = start_idx[1:]
        stop_idx = stop_idx[1:]
    if (len(stop_idx) > 0) and (
            stop_idx[-1] == len(is_missing)):  # Skip this one, we do not know the next label.
        start_idx = start_idx[:-1]
        stop_idx = stop_idx[:-1]
    durations = (stop_idx - start_idx)  # In number of segments/samples.
    previous = labels[np.clip(start_idx - 1, 0, np.inf).astype(int)]
    next_ = labels[np.clip(stop_idx, 0, len(is_missing) - 1).astype(int)]

    # Find short gaps surrounded by same label and interpolate (only interpolate sleep).
    interpolation_mask = (durations <= max_interpolation_length) & (previous == next_) & \
                         ((previous == 0) | (previous >= 4))
    for start, stop in zip(start_idx[interpolation_mask], stop_idx[interpolation_mask]):
        # Fill gap with surrounding sleep label.
        labels[start: stop] = labels[stop]

    return labels


def plot_hypnogram_old(sleep_labels, onsets, durations=None, time_scale=None,
                   order=None, missing_colors=None, missing_alpha=1,
                   add_legend=True, ax=None):
    """
    Helper function to plot the hypnograms from sleep_labels, onsets and durations.
    If durations is not specified, sleep_labels are assumed to follow each other consecutively.
    """
    if ax is None:
        ax = plt.gca()

    if durations is None:
        durations = np.diff(onsets)
        durations = np.append(durations, durations[-1])

    # To annotation set.
    an_set = AnnotationSet().from_lists(
        onsets=onsets,
        durations=durations,
        texts=sleep_labels).merge_successive_texts()

    # To dataframe.
    df = an_set.to_dataframe()

    # Add end time to make plot complete.
    end_time = df.onset.iat[-1] + df.duration.iat[-1]
    df_ = pd.concat([df, pd.DataFrame(dict(
        onset=[end_time], text=['dummy'], duration=[0]), index=[0])],
                     ignore_index=True)

    # Plot.
    plot_kwargs = dict(dict(color='#E1EED8', edgecolor='C7',
                            alpha=1.0))
    fillplot(x='onset', y='text', data=df_,
             y_order=order, ax=ax, step='post', **plot_kwargs)

    # Plot missing (labels not in order).
    an_set = an_set.filter_fun(
        fun=lambda lab: lab not in order)
    if len(an_set) > 0:
        shade_axis(onsets=an_set.onsets(), durations=an_set.durations(),
                   labels=an_set.texts(), color=missing_colors, alpha=missing_alpha,
                   add_legend=add_legend,
                   legend_kwargs=dict(bbox_to_anchor=(1, 1)), ax=ax)

    ax.set_ylim([0, 1.05])
    ax.set_title('Hypnogram')
    ax.set_ylabel('')

    format_time_axis(time_scale=time_scale, ax=ax)


def plot_hypnogram(sleep_labels, onsets, durations=None, time_scale=None,
                   order=None, missing_colors=None, missing_alpha=1, missing_height=0.1,
                   transition_label='TS',
                   add_legend=True, legend_kwargs=None, ax=None):
    """
    Helper function to plot the hypnograms from sleep_labels, onsets and durations.
    If durations is not specified, sleep_labels are assumed to follow each other consecutively.
    """
    if ax is None:
        ax = plt.gca()

    if durations is None:
        durations = np.diff(onsets)
        durations = np.append(durations, durations[-1])

    # To annotation set.
    an_set = AnnotationSet().from_lists(
        onsets=onsets,
        durations=durations,
        texts=sleep_labels).merge_successive_texts()

    # Create x and y for fillplot.
    x_height = np.array([(order.index(xi) + 1) / len(order) if xi in order else 0 for xi in an_set.texts()])
    x_new = []
    y_new = []
    for i_annot, annot in enumerate(an_set):
        label = annot.text
        if label != transition_label:
            onsets_i_all = [annot.onset]
            offsets_i_all = [annot.onset + annot.duration - 1e-8]
            heights_i_all = [x_height[i_annot]]
        else:
            if i_annot == 0 or i_annot == (len(an_set) - 1):
                # Skip.
                continue
            else:
                # Make the transition label switch previous and next class in the middle.
                height_1 = x_height[i_annot - 1]
                height_2 = x_height[i_annot + 1]
                x_switch = annot.onset + annot.duration/2

                onsets_i_all = [annot.onset, x_switch]
                offsets_i_all = [x_switch - 1e-8, annot.onset + annot.duration - 1e-8]
                heights_i_all = [height_1, height_2]

        for onset_i, offset_i, height_i in zip(onsets_i_all, offsets_i_all, heights_i_all):
            x_new.append(onset_i)
            y_new.append(height_i)

            x_new.append(offset_i)
            y_new.append(height_i)

    # Plot.
    plot_kwargs = dict(dict(color='#E1EED8', edgecolor='C7',
                            alpha=1.0))
    ax.fill_between(
        x=x_new, y1=y_new, **plot_kwargs)

    if order is None:
        # Default order.
        order = np.unique(an_set.texts())
    ylabels = order
    yticks = np.arange(1, len(order) + 1) / len(order)
    ax.set_yticks(yticks)
    ax.set_yticklabels(ylabels)

    # Plot missing (labels not in order).
    an_set = an_set.filter_fun(
        fun=lambda lab: lab not in order)

    unique_missing_labels = np.unique(an_set.texts()).tolist()
    if missing_colors is None:
        # Default mapping.
        missing_colors = dict(zip(unique_missing_labels, ('C{}'.format(i) for i in range(len(unique_missing_labels)))))

    for annot in an_set:
        lab = annot.text
        if isinstance(missing_colors, dict):
            col = missing_colors[lab]
        else:
            col = missing_colors
        ax.add_patch(Rectangle((annot.onset, -missing_height), annot.duration, missing_height,
                               color=col, alpha=missing_alpha))

    if add_legend and isinstance(missing_colors, dict):
        # Get handle to existing legend.
        old_legend = ax.get_legend()

        # Create new legend.
        default_legend_kwargs = {'loc': 'center right'}
        if legend_kwargs is not None:
            default_legend_kwargs.update(legend_kwargs)

        ax.legend(handles=[mpatches.Patch(color=col, label=lab, alpha=missing_alpha)
                            for lab, col in missing_colors.items() if lab in unique_missing_labels],
                   **default_legend_kwargs)

        # Re-add the old existing legend.
        if old_legend is not None:
            ax.add_artist(old_legend)

    ax.set_ylim([-missing_height, 1.05])
    ax.set_title('Hypnogram')
    ax.set_ylabel('')

    format_time_axis(time_scale=time_scale, ax=ax)


def plot_hypnogram_quality(
        sleep_labels, onsets, durations=None, order=None,
        quality_labels=None, quality_colors=None, quality_alpha=1, quality_height=0.1,
        mask_color='r', mask_alpha=0.3, mask_skip=None,
        time_scale=None, add_legend=True, legend_kwargs=None, ax=None, **kwargs):
    """
    Helper function to plot the hypnograms from sleep_labels, onsets and durations.
    If durations is not specified, sleep_labels are assumed to follow each other consecutively.

    Args:
        **kwargs (dict): for plt.fill_between(), e.g. color, alpha, etc.
    """
    # Defaults.
    if durations is None:
        durations = np.diff(onsets)
        durations = np.append(durations, durations[-1])

    if order is None:
        unique_labels = np.unique(sleep_labels).tolist()
        if all([lab in HYPNOGRAM_ORDER for lab in unique_labels]):
            order = HYPNOGRAM_ORDER
        else:
            order = unique_labels

    if mask_skip is None:
        mask_skip = ['TS', 'IS']

    if ax is None:
        ax = plt.gca()

    # To annotation set.
    an_set = AnnotationSet().from_lists(
        onsets=onsets,
        durations=durations,
        texts=sleep_labels).merge_successive_texts()

    # Create x and y for plt.fill_between().
    x_height = np.array([(order.index(xi) + 1) / len(order) if xi in order else 0 for xi in an_set.texts()])
    x_new = []
    y_new = []
    for i_annot, annot in enumerate(an_set):
        onset_i = annot.onset
        offset_i = annot.onset + annot.duration - 1e-8
        height_i = x_height[i_annot]

        # Add a point for both the onset and offset.
        x_new.append(onset_i)
        y_new.append(height_i)
        x_new.append(offset_i)
        y_new.append(height_i)

    # Plot.
    plot_kwargs = dict(dict(color='#E1EED8', edgecolor='C7',
                            alpha=1.0), **kwargs)
    ax.fill_between(
        x=x_new, y1=y_new, **plot_kwargs)

    # Plot quality labels.
    if quality_labels is not None:
        an_set = AnnotationSet().from_lists(
            onsets=onsets,
            durations=durations,
            texts=quality_labels).merge_successive_texts()

        unique_quality_labels = np.unique(an_set.texts()).tolist()
        if quality_colors is None:
            # Default mapping.
            quality_colors = dict(zip(unique_quality_labels, ('C{}'.format(i) for i in range(len(unique_quality_labels)))))

        for annot in an_set:
            lab = annot.text
            if lab == '':
                # Skip.
                continue

            if isinstance(quality_colors, dict):
                col = quality_colors[lab]
            else:
                col = quality_colors
            ax.add_patch(Rectangle((annot.onset, -quality_height), annot.duration, quality_height,
                                   color=col, alpha=quality_alpha))

            # Add a mask to the plot to mask the bad parts in the hypnogram.
            if mask_alpha > 0 and lab not in mask_skip:
                if mask_color is not None:
                    # Use a specified mask color. Otherwise, use the same color as above.
                    col = mask_color
                ax.add_patch(Rectangle((annot.onset, 0), annot.duration, 1.02,
                                       facecolor=col, alpha=mask_alpha, edgecolor=None)) #edgecolor=to_rgba(col, alpha=mask_alpha)))

        if add_legend and isinstance(quality_colors, dict):
            # Get handle to existing legend.
            old_legend = plt.gca().get_legend()

            # Create new legend.
            default_legend_kwargs = {'loc': 'upper left', 'bbox_to_anchor': [1, 1]}
            if legend_kwargs is not None:
                default_legend_kwargs.update(legend_kwargs)

            ax.legend(handles=[mpatches.Patch(color=col, label=lab, alpha=quality_alpha)
                                for lab, col in quality_colors.items() if lab in unique_quality_labels],
                                **default_legend_kwargs)

            # Re-add the old existing legend.
            if old_legend is not None:
                ax.add_artist(old_legend)

    # Figure make-up.
    ylabels = order
    yticks = np.arange(1, len(order) + 1) / len(order)
    ax.set_yticks(yticks)
    ax.set_yticklabels(ylabels)
    ax.set_ylim([-quality_height, 1.05])
    ax.set_title('Hypnogram')
    ax.set_ylabel('')
    format_time_axis(time_scale=time_scale, ax=ax)


def to_1_class(label, raise_error=True):
    """
    Convert 4-class sleep label to 1-class sleep label.

    Args:
        label (str): 4-class sleep label.
        raise_error (bool): if True, raises an error if the label cannot be converted.
            If False, returns the original label if conversion failed.

    Returns:
        new_label (str): 1-class label.
    """
    if 'QS' in label:
        new_label = 'Sleep'
    elif label in ['ASI', 'LVI']:
        new_label = 'Sleep'
    else:
        if not raise_error:
            new_label = label
        else:
            raise ValueError('Cannot convert label {} to 1 class sleep.'.format(label))
    return new_label


def to_2_class(label, raise_error=True):
    """
    Convert 4-class sleep label to 2-class sleep label.

    Args:
        label (str): 4-class sleep label.
        raise_error (bool): if True, raises an error if the label cannot be converted.
            If False, returns the original label if conversion failed.

    Returns:
        new_label (str): 2-class label.
    """
    if 'QS' in label:
        new_label = 'QS'
    elif label in ['ASI', 'LVI']:
        new_label = 'AS'
    else:
        if not raise_error:
            new_label = label
        else:
            raise ValueError('Cannot convert label {} to 2 class sleep.'.format(label))
    return new_label
