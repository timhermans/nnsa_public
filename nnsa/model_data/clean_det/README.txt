Predict whether the EEG is clean (clean=1, artefact=0).

Versions used to create the models:
python=3.8.8
numpy=1.19.5
scikit-learn=0.24.1
tensorflow==2.4.1