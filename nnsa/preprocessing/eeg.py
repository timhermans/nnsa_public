"""
EEG specific preprocessing routines.

Author: Tim Hermans (tim-hermans@hotmail.com).
"""
import numpy as np
import matplotlib.pyplot as plt


def clamp_eeg(x):
    """
    Clamp values outside [-250, 250] to limit the dynamic range.

    References:
        Equation (1) in:
        L. Webb, M. Kauppila, J. A. Roberts, S. Vanhatalo, and N. J. Stevenson,
        “Automated detection of artefacts in neonatal EEG with residual neural networks,”
        Computer Methods and Programs in Biomedicine, vol. 208, p. 106194, Sep. 2021,
        doi: 10.1016/j.cmpb.2021.106194.

    Args:
        x (np.ndarray): array with values.

    Returns:
        y (np.ndarray): array with same shape as `x` but with values
            lower than -205 and higher than 250 clamped.

    Examples:
        # Plot clamping function.
        >>> import matplotlib.pyplot as plt
        >>> x = np.linspace(-1000, 1000, 1000)
        >>> y = clamp_eeg(x)
        >>> plt.plot(x, x, linestyle='--', color='C7') #doctest:+ELLIPSIS
        [<matplotlib.lines.Line2D...
        >>> plt.plot(x, y) #doctest:+ELLIPSIS
        [<matplotlib.lines.Line2D...
    """
    # Copy input array.
    y = x.copy()

    # Replace values outside range with clamped values.
    low_mask = x < -250
    high_mask = x > 250
    y[high_mask] = 250*(np.log(y[high_mask]) - np.log(250) + 1)
    y[low_mask] = -250*(np.log(-y[low_mask]) - np.log(250) + 1)

    return y
