def make_stars(pval):
    s = u"\u2217"  # Unicode for asterisk (has large size and centered in middle).
    if pval < 0.001:
        stars = 3*s
    elif pval < 0.01:
        stars = 2*s
    elif pval < 0.05:
        stars = 1*s
    else:
        stars = ''
    return stars
