"""
Module containing commonly used utility functions.
"""
from .arrays import moving_average
from .code_performance import print_efficiency
from .paths import check_directory_exists, check_filename_exists, check_file_extension
from .plotting import maximize_figure, subplot_rows_columns

__all__ = [
    'print_efficiency',

    'check_directory_exists',
    'check_filename_exists',
    'check_file_extension',

    'maximize_figure',
    'subplot_rows_columns'
	]