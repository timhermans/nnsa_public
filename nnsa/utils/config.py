"""
Handy constants.
"""
import numpy as np

__all__ = [
    'HORIZONTAL_RULE',
    'NUMERIC_TYPES',
]

HORIZONTAL_RULE = '-'*40
NUMERIC_TYPES = (int, float, np.float32, np.float64, np.int32)
