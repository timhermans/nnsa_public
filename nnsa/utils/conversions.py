import numpy as np


def convert_time_scale(time, time_scale):
    """
    Convert a time array in seconds to a specified scale.

    Args:
        time (np.ndarray, float, int): time array in seconds.
        time_scale (str): the time scale to convert to. Choose from 'seconds', 'minutes', 'hours'.

    Returns:
        time (np.ndarray): a copy of the time array, scaled accordingly corresponding to the requested scale.
    """
    if hasattr(time, '__len__'):
        # Copy the array (so we so not mutate the original one).
        time = np.asarray(time).copy().astype(float)
    else:
        time = time

    # Case-insensitive.
    time_scale = time_scale.lower()

    # Convert to requested time scale.
    if time_scale in ['seconds', 'sec', 's', 'second'] or time_scale is None:
        pass
    elif time_scale in ['minutes', 'min', 'm', 'minute']:
        time /= 60
    elif time_scale in ['hours', 'hrs', 'h', 'hour']:
        time /= 3600
    elif time_scale in ['days', 'd', 'day']:
        time /= (3600*24)
    else:
        raise ValueError('Invalid time_scale "{}". Choose from {}.'
                         .format(time_scale, ['seconds', 'minutes', 'hours', 'days']))

    return time


def revert_time_scale(time, time_scale):
    """
    Convert a time array in `time_scale` to seconds.

    Args:
        time (float, int, np.ndarray): time array in arbitrary scale.
        time_scale (str): the time scale. Choose from 'seconds', 'minutes', 'hours'.

    Returns:
        time (np.ndarray): a copy of the time array, rescaled to seconds.
    """
    if hasattr(time, '__len__'):
        # Copy the array (so we so not mutate the original one).
        time = np.asarray(time).copy()
    else:
        time = time*1

    # Case-insensitive.
    time_scale = time_scale.lower()

    # Convert to requested time scale.
    if time_scale in ['seconds', 'sec', 's', 'second'] or time_scale is None:
        pass
    elif time_scale in ['minutes', 'min', 'm', 'minute']:
        time *= 60
    elif time_scale in ['hours', 'hrs', 'h', 'hour']:
        time *= 3600
    elif time_scale in ['days', 'd', 'day']:
        time *= (3600*24)
    else:
        raise ValueError('Invalid time_scale "{}". Choose from {}.'
                         .format(time_scale, ['seconds', 'minutes', 'hours']))

    return time