"""
Deprecated file. All is moved to nnsa.keras.data.
"""

import warnings
import nnsa.keras.utils as ku


def setup_tf(*args, **kwargs):
    msg = '\nUse function in nnsa.keras.data instead of nnsa.data.keras'
    warnings.warn(msg)
    return ku.setup_tf(*args, **kwargs)


def enable_monte_carlo_dropout(*args, **kwargs):
    msg = '\nUse function in nnsa.keras.data instead of nnsa.data.keras'
    warnings.warn(msg)
    return ku.enable_monte_carlo_dropout(*args, **kwargs)
