"""A setuptools based setup module.
See:
https://python-packaging.readthedocs.io/en/latest/minimal.html
https://packaging.python.org/guides/distributing-packages-using-setuptools/
https://github.com/pypa/sampleproject
"""
from setuptools import setup, find_packages
from os import path
# io.open is needed for projects that support Python 2.7
# It ensures open() defaults to text mode with universal newlines,
# and accepts an argument to specify the text encoding
# Python 3 only projects can skip this import
from io import open

VERSION = '1.0.0'

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.
setup(
    name='nnsa',  # Required
    version=VERSION,  # Required
    description='NeoNatal Signal Analysis',  # Optional
    author='Tim Hermans',  # Optional
    author_email='tim-hermans@hotmail.com',  # Optional
    keywords='neonatal signal processing eeg edf',  # Optional
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),  # Required
    python_requires='>=3.8',
    include_package_data=True,  # To include data mentioned in MANIFEST in the installed package folder.

    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    install_requires=[
        # Core.
        'tensorflow>=2.4.1',  # Deep learning.
        'joblib>=1.0.1',   # Saving and loading.
        'scikit-learn>=1.1.1',  # Machine learning.
        'h5py>=2.9',  # Saving and reading HDF5 files for storage of large numpy arrays.
        'hmmlearn>=0.2.8',  # Hidden-Markov Modeling.
        'tables',  # Saving and loading pandas DataFrames to HDF5.
        'matplotlib',  # Plotting.
        'numpy',  # Array processing.
        'scipy',  # Scientific and technical computing.
        'pyprind',  # Progress bar.
        'pandas',  # Tabular data manipulation.
        'seaborn',  # Advanced visualization.
        'xlrd',  # Pandas reading Excel files.
        'openpyxl',  # Pandas writing Excel files.
        'networkx',  # Graphs.
        'statsmodels',  # Statistical models and testing.
        'pycwt',  # Continuous wavelet analysis.
        'numba',  # Faster numpy code.
        'lifelines',  # Kaplan-Meier survival-curves.
        'psutil',  # Get memory usage information.
        'termcolor',  # Printing in color to the terminal.
        ],
    project_urls={  # Optional
        'Source': 'https://gitlab.com/timhermans/nnsa_public',
    },
)
